{* Письмо пользователю для восстановления пароля *}
{* Канонический адрес страницы *}
{$canonical="/user/password_remind" scope=parent}
{if $email_sent}
<!-- CONTAINER -->
<div class="container inforow">
	<div class="col-sm-12">
		<h1>Вам отправлено письмо</h1>
		<p>На {$email|escape} отправлено письмо для восстановления пароля. </p>
	</div>
</div>
<!-- /.container -->
{else}
<!-- HEADCONTENT -->
<div class="headcontent">
	<div class="container">
		<div class="col-sm-12">
			<h1>Напоминание пароля</h1>
		</div>
	</div>
</div>
<!-- /.headcontent -->  
<!-- CONTAINER -->
<div class="container inforow">
	<div class="col-md-6 col-sm-8">
		{if $error}
		<div class="message_error">
			{if $error == 'user_not_found'}<div class="alert alert-warning fade in">Пользователь не найден</div>
			{else}{$error}{/if}
		</div>
		{/if}
		<form class="login-form" method="post">
			<div class="form-group">
				<label for="email">Введите email, который вы указывали при регистрации</label>
				<input type="text" name="email" data-format="email" data-notice="Введите email" value="{$email|escape}"  maxlength="255"/>
			</div>
			<input type="submit" class="btn btn-primary" value="Вспомнить" />
		</form>
		{/if}
	</div>
</div>
<!-- /.container -->