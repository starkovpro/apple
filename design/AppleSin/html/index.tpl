﻿<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1">
		<base href="{$config->root_url}/"/>
		<title>{$meta_title|escape}</title>
		{* Метатеги *}
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta name="description" content="{$meta_description|escape}" />
		<meta name="keywords" content="{$meta_keywords|escape}" />
		
		<!-- Custom Browsers Color Start -->
		<!-- Chrome, Firefox OS and Opera -->
		<meta name="theme-color" content="#000">
		<!-- Windows Phone -->
		<meta name="msapplication-navbutton-color" content="#000">
		<!-- iOS Safari -->
		<meta name="apple-mobile-web-app-status-bar-style" content="#000">
		<!-- Custom Browsers Color End -->
		
		<meta property="og:type" content="article" />
		<meta property="og:title" content="{$meta_title|escape}" />
		<meta property="og:description" content="{$meta_description|escape}" />
		<meta property="og:url" content="{$config->root_url}{$canonical}" />
		<meta property="og:image" content="http://applesin.kz/design/AppleSin/images/applesin_logo.png" />
		
		{* JQuery *}
		<script src="js/jquery/jquery.js"  type="text/javascript"></script>
		{* Капча *}
		<script src='https://www.google.com/recaptcha/api.js'></script>
		{* Аяксовая корзина *}
		<script src="design/{$settings->theme}/js/jquery-ui.min.js"></script>
		<script src="design/{$settings->theme}/js/ajax_cart.js"></script>
		{* js-проверка форм *}
		<script src="js/baloon/js/baloon.js" type="text/javascript"></script>
		<link   href="js/baloon/css/baloon.css" rel="stylesheet" type="text/css" />
		
		{* Канонический адрес страницы *}
		{if isset($canonical)}<link rel="canonical" href="{$config->root_url}{$canonical}"/>{/if}
		<link href="design/{$settings->theme|escape}/images/favicon.png" rel="shortcut icon" type="image/x-icon">
		<link href="design/{$settings->theme|escape}/css/preloader.css" rel="stylesheet">
		<link href="design/{$settings->theme|escape}/css/bootstrap.min.css" rel="stylesheet">
		<link href="design/{$settings->theme|escape}/css/font-awesome.min.css" rel="stylesheet">
		<link href="design/{$settings->theme|escape}/css/animate.css" rel="stylesheet">
		<link href="design/{$settings->theme|escape}/css/style-red.css" rel="stylesheet">
		
	</head>
	<body class="vertical-menu">

		<div class="preloader"></div>

		<div class="vside vside-dark active ">
			<button class="v-toggle ico-sidebar-l"></button>
		</div>
		<header class="header header-dark navbar-fixed-top ">
			<div class="vheader-height"><!--был класс vheader-height-->
				<nav class="navbar">
					<a class="navbar-brand hidden-xs" href="#"><img src="design/{$settings->theme|escape}/images/logo.png" title="Перейти на главную страницу" alt="AppleSin logo"/></a>
					<ul class="nav navbar-nav">
						{foreach $pages as $p}
						{* Выводим только страницы из первого меню *}
						{if $p->menu_id == 1}
						<li {if $page && $page->id == $p->id}class="selected"{/if}>
							<a data-page="{$p->id}" href="{$p->url}">{$p->name|escape}</a>
						</li>
						{/if}
						{/foreach}
					</ul>
				</nav>
				<nav class="navbar">
					{* Рекурсивная функция вывода дерева категорий *}
					{function name=categories_tree}
					{if $categories}
					<ul class="nav navbar-nav">
						{foreach $categories as $c}
						{* Показываем только видимые категории *}
						{if $c->visible}
						<li>
							{if $c->image}<img src="{$config->categories_images_dir}{$c->image}" alt="{$c->name|escape}">{/if}
							<a {if $category->id == $c->id}class="selected"{/if} href="catalog/{$c->url}" data-category="{$c->id}">{$c->name|escape}</a>
						</li>
						{/if}
						{/foreach}
					</ul>
					{/if}
					{/function}
					{categories_tree categories=$categories}
				</nav>
				<!-- Доп меню  -->
				<div class="navbar vextra sf-js-enabled sf-arrows">
					<ul class="nav navbar-nav">
						<ul class="nav navbar-nav">
							<li><a href="/products">Все товары </a></li>
							<li><a href="/warranty">Гарантия и возврат </a></li>
							<li><a href="/shipping">Доставка </a></li>
							<li><a href="/how-to-pay">Оплата </a></li>
							<li><a href="/requisites">Реквизиты</a></li>
							<li><a href="/contact">Написать нам</a></li>
						</ul>
					</ul>
				</div> 
				<!-- Доп меню  (The End) -->
				<!-- Меню блога  -->
				<!--<div class="navbar vextra sf-js-enabled sf-arrows">
					<ul class="nav navbar-nav">
					{* Выбираем в переменную $last_posts последние записи *}
					{get_posts var=last_posts}
					{if $last_posts}
					{foreach $last_posts as $post}
					{if $post->category==1}
					<ul class="nav navbar-nav">
					<li data-post="{$post->id}"><a href="blog/{$post->url}">{$post->name|escape}</a></li>
					</ul>
					{/if}
					{/foreach}
					{/if}
					</ul>
				</div> -->
				<!-- Меню блога  (The End) -->
				<!-- Соц. сети -->
				<nav class="navbar">
					<a href="http://vk.com/apple_petropavl" target="_blank" title="Наша группа VK"><span class="fa-stack fa-lg"><i class="fa fa-square-o fa-stack-2x"></i><i class="fa fa-vk fa-stack-1x"></i></span></a>
					<a href="http://instagram.com/applesinkz " target="_blank" title="Наш instagram"><span class="fa-stack fa-lg"><i class="fa fa-square-o fa-stack-2x"></i><i class="fa fa-instagram fa-stack-1x"></i></span></a>
				</nav>
			</div>
		</header>
		<!-- /.header -->
		<!-- WRAPPER -->
		<div class="wrapper">
			{$content}
		</div>
		<!-- /.wrapper -->
		<!-- FOOTER -->
		<footer class="footer footer-dark hidden-xs">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<p>&copy;  Applesin 2012-2017 г. Продажа,обслуживание и ремонт Apple iPhone 4, Apple iPhone 4s, Apple iPhone 5, Apple iPhone 5s, Apple iPhone 6, Apple iPhone 6 plus, Apple iPhone 6s, Apple iPhone 6s plus, Apple iPhone 7, Apple iPhone 7 plus, Apple iPhone 8, Apple iPhone 8 plus, Apple iPhone X в Петропавловске за наличный расчет и в кредит. <br/>
						Сайт носит исключительно информационный характер и ни при каких условиях не является публичной офертой.</p>
						
						<a href="/products">Все товары </a> | 
						<a href="/warranty">Гарантия и возврат </a> | 
						<a href="/shipping">Доставка </a> |
						<a href="/how-to-pay">Оплата </a> | 
						<a href="/requisites">Реквизиты</a> | 
						<a href="/contact">Написать нам</a> | 
						<a href="https://applesin.bitrix24.kz/pub/form/8_predlozhenie_sotrudnichestva/j5fvp5/" target="blank">Поставщикам</a>
					</div>
				</div>
			</div>
		</footer>
		<!-- /.footer -->
		<!-- ScrollTop -->
		<a href="#" class="scrolltop hidden"><i></i></a>
		<!-- SCRIPTS -->
		<script src="design/{$settings->theme|escape}/js/jquery-2.1.3.min.js"></script>
		<script src="design/{$settings->theme|escape}/js/bootstrap.min.js"></script>
		<script src="design/{$settings->theme|escape}/js/plugins.js"></script>
		<script src="design/{$settings->theme|escape}/js/custom.js"></script>
		{literal}
		<!-- Yandex.Metrika counter --> 
		<script type="text/javascript"> (function (d, w, c) { (w[c] = w[c] || []).push(function() { try { w.yaCounter22404034 = new Ya.Metrika({ id:22404034, clickmap:true, trackLinks:true, accurateTrackBounce:true }); } catch(e) { } }); var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function () { n.parentNode.insertBefore(s, n); }; s.type = "text/javascript"; s.async = true; s.src = "https://mc.yandex.ru/metrika/watch.js"; if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); } })(document, window, "yandex_metrika_callbacks"); </script> <noscript><div><img src="https://mc.yandex.ru/watch/22404034" style="position:absolute; left:-9999px;" alt="" /></div></noscript> 
		<!-- /Yandex.Metrika counter -->
		<!-- Bitrix chat -->
		<script data-skip-moving="true">
			(function(w,d,u,b){
				s=d.createElement('script');r=(Date.now()/1000|0);s.async=1;s.src=u+'?'+r;
				h=d.getElementsByTagName('script')[0];h.parentNode.insertBefore(s,h);
			})(window,document,'https://cdn.bitrix24.kz/b2272691/crm/site_button/loader_4_c19gum.js');
		</script>
		<!-- /Bitrix chat -->
		
		
		{/literal}		
	</body>
</html>