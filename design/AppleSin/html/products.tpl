{* Список товаров *}
{* Канонический адрес страницы *}
{if $category && $brand}
{$canonical="/catalog/{$category->url}/{$brand->url}" scope=parent}
{elseif $category}
{$canonical="/catalog/{$category->url}" scope=parent}
{elseif $brand}
{$canonical="/brands/{$brand->url}" scope=parent}
{elseif $keyword}
{$canonical="/products?keyword={$keyword|escape}" scope=parent}
{else}
{$canonical="/products" scope=parent}
{/if}
<div class="visible-xs">
<div class="mobilehead inforow">
<a href="#"><img src="design/{$settings->theme|escape}/images/logo.png" title="Перейти на главную страницу" alt="AppleSin logo"/></a>
</div>
</div>
<!-- HEADCONTENT -->
<div class="headcontent">
	<div class="container">
		<div class="col-md-8">	
			<h1>{* Заголовок страницы *}
				{if $keyword}
				<h1>Поиск {$keyword|escape}</h1>
				{elseif $page}
				<h1>{$page->name|escape}</h1>
				{else}
				<h1>{$category->name|escape} {$brand->name|escape} {$keyword|escape}</h1>
			{/if}</h1>
			<ol class="breadcrumb">
				<a href="/">Главная</a>
				{if $category}
				{foreach from=$category->path item=cat}
				/ <a href="catalog/{$cat->url}">{$cat->name|escape}</a>
				{/foreach}  
				{if $brand} 
				/ <a href="catalog/{$cat->url}/{$brand->url}">{$brand->name|escape}</a>
				{/if}
				{elseif $brand}
				/ <a href="brands/{$brand->url}">{$brand->name|escape}</a>
				{elseif $keyword}
				/ Поиск
				{/if}
			</ol>
		</div>
		<div class="col-md-4 text-right">
			<!-- Вход пользователя -->
			{if $user}
			<span id="username">
				<a href="user">{$user->name}</a>{if $group->discount>0},
				ваша скидка &mdash; {$group->discount}%{/if}
			</span> / 
			<a id="logout" href="user/logout">выйти</a>
			{else}
			<a id="register" href="user/register">Регистрация</a>
			<a id="login" href="user/login">Вход</a>
			{/if}
			<!-- Вход пользователя (The End)-->
			<p class="pull-right">
				<div id="cart_informer">{include file='cart_informer.tpl'}</div>
			</p>		
			<!-- Выбор валюты -->
			{* Выбор валюты только если их больше одной *}
			{if $currencies|count>1}
			<nav class="nav-currency pull-right">
				{foreach from=$currencies item=c}
				{if $c->enabled} 
				<a href='{url currency_id=$c->id}' class="{if $c->id==$currency->id}active{/if}">{$c->name|escape}</a>
				{/if}
				{/foreach}
			</nav>
			{/if}
			<!-- Выбор валюты (The End) -->	
		</div>
	</div>
	<a data-toggle="collapse" href="#hc-collapse" class="hc-toggle collapsed"></a>
	<div id="hc-collapse" class="hc-panel panel-collapse collapse bg-warning">
		<div class="container">
			<div class="col-sm-12">
				<p>{* Описание страницы (если задана) *}
					{if $category->description}
					{$category->description}
					{else}
					Нет описания :( 
					{/if}
				</p>
			</div>
		</div>
	</div>
</div>
<!-- /.headcontent -->
<!--Каталог товаров-->
<hr class="sm-margin no-border"/>
{if $products}
<!-- PRODUCTS -->
<div class="container products">
	<div class="col-md-3 col-sm-4 col-xs-5 catalog-bar visible-xs">
		<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
			<div class="panel panel-default">
				<div class="panel-heading" role="tab" id="headingOne">
					<h4 class="panel-title">
						<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
							Каталог
						</a>
					</h4>
				</div>
				<div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
					<div class="panel-body">
						{* Рекурсивная функция вывода дерева категорий *}
						{function name=categories_treeinproductpage}
						{if $categories}
						<ul  class="category-list">
							{foreach $categories as $c}
							{* Показываем только видимые категории *}
							{if $c->visible}
							<li>
								<a {if $category->id == $c->id}class="selected"{/if} href="catalog/{$c->url}" data-category="{$c->id}">{$c->name|escape}</a>
								{categories_treeinproductpage categories=$c->subcategories}
							</li>
							{/if}
							{/foreach}
						</ul>
						{/if}
						{/function}
						{categories_treeinproductpage categories=$categories}
					</div>
				</div>
			</div>

			{if $features}
			<div class="panel panel-default">
				<div class="panel-heading" role="tab" id="headingThree">
					<h4 class="panel-title">
						<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
							Фильтр
						</a>
					</h4>
				</div>
				<div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
					<div class="panel-body">
						<ul class="category-list">
							{foreach $features as $f}
							{$f->name}:
							<li><a href="{url params=[$f->id=>null, page=>null]}" {if !$smarty.get.$f@key}class="selected"{/if}>Все</a></li>
							{foreach $f->options as $o}
							<li><a href="{url params=[$f->id=>$o->value, page=>null]}" {if $smarty.get.$f@key == $o->value}class="selected"{/if}>{$o->value|escape}</a></li>
							{/foreach}
							{/foreach}
						</ul>
					</div>
				</div>
			</div>
			{/if}
		</div>
	</div>
	<div class="col-md-3 col-sm-4 col-xs-5 catalog-bar hidden-xs">
		<!-- .widget -->
		<div class="widget" id="search">
			<form action="products" class="search-form">
				<div class="form-group">
					<input class="input_search" type="text" name="keyword" value="{$keyword|escape}" placeholder="Поиск товара"/>
				</div>
				<button type="submit"  value=""><i class="fa fa-search"></i></button>
			</form>
		</div>
		<!-- /.widget -->
		<hr/>
		<!-- widget -->
		<div class="widget">
			<h4>Каталог</h4>
			{* Рекурсивная функция вывода дерева категорий *}
			{function name=categories_treeinproductpage}
			{if $categories}
			<ul  class="category-list">
				{foreach $categories as $c}
				{* Показываем только видимые категории *}
				{if $c->visible}
				<li>
					{if $c->image}<img src="{$config->categories_images_dir}{$c->image}" alt="{$c->name|escape}">{/if}
					<a {if $category->id == $c->id}class="selected"{/if} href="catalog/{$c->url}" data-category="{$c->id}">{$c->name|escape}</a>
					{categories_treeinproductpage categories=$c->subcategories}
				</li>
				{/if}
				{/foreach}
			</ul>
			{/if}
			{/function}
			{categories_treeinproductpage categories=$categories}
		</div>
		<!-- /.widget -->
		<hr/>

		{* Фильтр по свойствам *}
		{if $features}
		<!-- widget -->
		<div class="widget">
			<ul class="category-list">
				{foreach $features as $f}
				<h4>{$f->name}:</h4>
				<li><a href="{url params=[$f->id=>null, page=>null]}" {if !$smarty.get.$f@key}class="selected"{/if}>Все</a></li>
				{foreach $f->options as $o}
				<li><a href="{url params=[$f->id=>$o->value, page=>null]}" {if $smarty.get.$f@key == $o->value}class="selected"{/if}>{$o->value|escape}</a></li>
				{/foreach}
				{/foreach}
			</ul>
		</div>
		<!-- /.widget -->
		<hr class="no-border"/>
		{/if}
	</div>
	<!-- /.catalog-bar -->
	<div class="sort col-md-3 col-sm-4 col-md-offset-6 col-sm-offset-4">
		<select id="select-sorting" onchange="getval(this);">
			<option value="1">Сначала новые</option>
			<option value="2">Цена (сначала дешевые)</option>
			<option value="3">Цена (сначала дорогие)</option>
			<option value="4">По имени (от а до я)</option>
			<option value="5">По имени (от я до а)</option>
		</select>
	</div>
	<div class="col-md-9 col-sm-8 col-xs-7 product-list text-center md-padding-bottom">
		{foreach $products as $product}
		<div class="product">
			{if $product->image}
			<div  class="product-img">
			<a data-product="{$product->id}" href="products/{$product->url}"><img src="{$product->image->filename|resize:200:200}" alt="{$product->name|escape}" title="{$product->name|escape}"/></a>
			</div>
			{else}
			<div  class="product-img">
				<a data-product="{$product->id}" href="products/{$product->url}"><img height="200" src="design/{$settings->theme|escape}/images/nophoto.png" alt="Нет фото" title="Нет фото"/></a>
			</div>
			{/if}
		<h6 class="product-name"><a data-product="{$product->id}" href="products/{$product->url}">{$product->name|escape}</a></h6>
			{if $product->variants|count > 0}
			<form class="variants" action="/cart">
				{foreach $product->variants as $v}
				{if $v->compare_price > 0}<span class="compare_price">{$v->compare_price|convert}</span>{/if}
				<span class="price">{$v->price|convert} <span class="currency">{$currency->sign|escape}</span></span>
				{/foreach}
				<!--<input type="submit" class="button" value="в корзину" data-result-text="добавлено"/>-->
			</form>
			{else}
			Нет в наличии
			{/if}
		</div>
		{/foreach}
		{include file='pagination.tpl'}
	</div>
	{/if}
</div>
<!-- /.product-list -->
<!-- СОРТИРОВКИ -->
<div class="hide">
	<a href="{url sort=position page=null}" id="sort1">по умолчанию</a>
	<a href="{url sort=price_asc page=null}" id="sort2">цена</a>
	<a href="{url sort=price_desc page=null}" id="sort3">цена</a>
	<a href="{url sort=name_asc page=null}" id="sort4">название</a>
	<a href="{url sort=name_desc page=null}" id="sort5">название</a>
</div>
{if $sort=='price_asc'}{literal}<script>$("#select-sorting").val(2);</script>{/literal}{/if}
{if $sort=='price_desc'}{literal}<script>$("#select-sorting").val(3);</script>{/literal}{/if}
{if $sort=='name_asc'}{literal}<script>$("#select-sorting").val(4);</script>{/literal}{/if}
{if $sort=='name_desc'}{literal}<script>$("#select-sorting").val(5);</script>{/literal}{/if}
{literal}
<script>
	function getval(sel) {
		if (sel.value == 1) {
			document.getElementById('sort1').click();
		}
		if (sel.value == 2) {
			document.getElementById('sort2').click();
		}
		if (sel.value == 3) {
			document.getElementById('sort3').click();
		}
		if (sel.value == 4) {
			document.getElementById('sort4').click();
		}
		if (sel.value == 5) {
			document.getElementById('sort5').click();
		}
	}
</script>
{/literal}