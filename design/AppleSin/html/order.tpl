{* Страница заказа *}
{$meta_title = "Ваш заказ №`$order->id`" scope=parent}
<!-- HEADCONTENT -->
<div class="headcontent">
	<div class="container">
		<div class="col-md-12 text-center">
			<h1>Спасибо! Ваш заказ №{$order->id} 
				{if $order->status == 0}принят{/if}
				{if $order->status == 1}в обработке{elseif $order->status == 2}выполнен{/if}
				{if $order->paid == 1}, оплачен{else}{/if}
			</h1>
		</div>
	</div>
</div>
<!-- /.headcontent -->
<!-- CONTAINER -->
<div class="container cart no-padding-bottom">
	<div class="col-sm-12">
		<div class="table-responsive">
			<table class="table text-center">
				<thead>
                    <tr>
                        <th></th>
                        <th class="text-left">Товар и цена</th>
                        <th class="text-center">Количество</th>
                        <th class="text-center">Итого</th>
					</tr>
				</thead>
				<tbody>
					{foreach $purchases as $purchase}
                    <tr>
                        <td class="product-thumbnail text-left">
							{$image = $purchase->product->images|first}
							{if $image}
							<a href="products/{$purchase->product->url}"><img src="{$image->filename|resize:100:100}" alt="{$product->name|escape}"></a>
							{/if}
						</td>
                        <td class="product-name text-left">
                            <a href="/products/{$purchase->product->url}">{$purchase->product_name|escape}</a>
							{$purchase->variant_name|escape}
							{if $order->paid && $purchase->variant->attachment}
							<a class="download_attachment" href="order/{$order->url}/{$purchase->variant->attachment}">скачать файл</a>
							{/if}
                            <span class="price">
                                <span class="amount">{($purchase->price)|convert}&nbsp;{$currency->sign}</span>
							</span>
						</td>
                        <td class="product-quantity">&times; {$purchase->amount}&nbsp;{$settings->units}</td>
                        <td class="product-subtotal">{($purchase->price*$purchase->amount)|convert}&nbsp;{$currency->sign}</td>
					</tr>
					{/foreach}
					{* Если стоимость доставки входит в сумму заказа *}
					{if !$order->separate_delivery && $order->delivery_price>0}
					<tr>
						<td colspan="3" class="text-right">{$delivery->name|escape}:</td>
						<td>{$order->delivery_price|convert}&nbsp;{$currency->sign}</td>
					</tr>
					{/if}
					{* Если стоимость доставки не входит в сумму заказа *}
					{if $order->separate_delivery}
					<tr>
						<td colspan="3" class="text-right">{$delivery->name|escape}:</td>
						<td>0 KZT</td>
					</tr>
					{/if}
					{* Итого *}
					<tr>
						<td colspan="3" class="text-right">Итого:</td>
						<td>{$order->total_price|convert}&nbsp;{$currency->sign}</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</div>
<!-- /.container -->
<!-- CONTAINER -->
<div class="container no-padding">
	<div class="col-md-6">
		<h3>Контактная информация:</h3>
		<address>
			{if $order->name}
			<p>{$order->name|escape}</p>
			{/if}	
			{if $order->email}
			<p>{$order->email|escape}</p>
			{/if}	
			{if $order->phone}
			<p>т. {$order->phone|escape}</p>
			{/if}	
			{if $order->address}
			<p>{$order->address|escape}</p>
			{/if}	
			{if $order->comment}
			<p>{$order->comment|escape|nl2br}</p>
			{/if}	
		</address>
	</div>
</div>
<!-- /.container -->
<div class="container">
	<div class="col-md-6">
		{if !$order->paid}
	    <h3>Оплата:</h3>
		{* Выбор способа оплаты *}
		{if $payment_methods && !$payment_method && $order->total_price>0}
		<form method="post">
			<h3>Выберите способ оплаты</h3>
			<div class="panel-group payment-wrap" id="deliveries">
				{foreach $payment_methods as $payment_method}
								<div class="panel clearfix">
					<div class="radio">
					<label>
						<input type=radio name=payment_method_id value='{$payment_method->id}' {if $payment_method@first}checked{/if} id=payment_{$payment_method->id}>
					<strong>{$payment_method->name}, к оплате {$order->total_price|convert:$payment_method->currency_id}&nbsp;{$all_currencies[$payment_method->currency_id]->sign}</strong>
						<label>
					</div>			
					<div  id="{$delivery->id}">
						{$payment_method->description}
					</div>
				</div>
				{/foreach}
			</div>
			<input type='submit' class="btn btn-default" value='Закончить заказ'>
		</form>
		{* Выбраный способ оплаты *}
		{elseif $payment_method}
		<p>Способ оплаты &mdash; {$payment_method->name}
			<form method=post>
				<input type=submit name='reset_payment_method'  class="btn-link" value='Выбрать другой способ оплаты'>
			</form>	
		</p>
		<p>
			{$payment_method->description}
		</p>
		<h2>
			К оплате {$order->total_price|convert:$payment_method->currency_id}&nbsp;{$all_currencies[$payment_method->currency_id]->sign}
		</h2>
		{* Форма оплаты, генерируется модулем оплаты *}
		{checkout_form order_id=$order->id module=$payment_method->module}
		{/if}
		{else}
		<h3>Заказ оплачен, ждите доставку!</h3>
		{/if}
	</div>
</div>