{* Страница товара *}
{* Канонический адрес страницы *}
{$canonical="/products/{$product->url}" scope=parent}
<div class="visible-xs">
	<div class="mobilehead inforow">
		<a href="#"><img src="design/{$settings->theme|escape}/images/logo.png" title="Перейти на главную страницу" alt="AppleSin logo"/></a>
	</div>
</div>
<!-- HEADCONTENT -->
<div class="headcontent">
	<div class="container">
		<div class="col-md-8">
			<h1>{* Заголовок страницы *}
				{if $keyword}
				<h1>Поиск {$keyword|escape}</h1>
				{elseif $page}
				<h1>{$page->name|escape}</h1>
				{else}
				<h1>{$category->name|escape} {$brand->name|escape} {$keyword|escape}</h1>
				{/if}
				<ol class="breadcrumb">
					<a href="/">Главная</a>
					{if $category}
					{foreach from=$category->path item=cat}
					/ <a href="catalog/{$cat->url}">{$cat->name|escape}</a>
					{/foreach}
					{if $brand}
					/ <a href="catalog/{$cat->url}/{$brand->url}">{$brand->name|escape}</a>
					{/if}
					{elseif $brand}
					/ <a href="brands/{$brand->url}">{$brand->name|escape}</a>
					{elseif $keyword}
					/ Поиск
					{/if}
					/{$product->name|escape}
				</ol>
			</div>
			<div class="col-md-4 text-right">
				<!-- Вход пользователя -->
				{if $user}
				<span id="username">
					<a href="user">{$user->name}</a>{if $group->discount>0},
					ваша скидка &mdash; {$group->discount}%{/if}
				</span> / 
				<a id="logout" href="user/logout">выйти</a>
				{else}
				<a id="register" href="user/register">Регистрация</a> / 
				<a id="login" href="user/login">Вход</a>
				{/if}
				<!-- Вход пользователя (The End)-->
				<p class="pull-right">
					<div id="cart_informer">{include file='cart_informer.tpl'}</div>
				</p>
				<!-- Выбор валюты -->
				{* Выбор валюты только если их больше одной *}
				{if $currencies|count>1}
				<nav class="nav-currency pull-right">
					{foreach from=$currencies item=c}
					{if $c->enabled}
					<a href='{url currency_id=$c->id}' class="{if $c->id==$currency->id}active{/if}">{$c->name|escape}</a>
					{/if}
					{/foreach}
				</nav>
				{/if}
				<!-- Выбор валюты (The End) -->
			</div>
		</div>
	</div>
	<!-- /.headcontent -->
	<!-- CONTAINER -->
	<div class="container type-product inforow" itemscope itemtype="http://schema.org/Product">
		<div class="col-sm-6 text-center magnific-wrap">
			<div class="img-medium slider oneslider">
				{if $product->image}
				<a href="{$product->image->filename|resize:800:600:w}" title="{$product->name|escape}" class="magnific"><img src="{$product->image->filename|resize:540:600:w}" class="img-responsive" alt="{$product->name|escape}"  title="{$product->name|escape}" itemprop="image"></a>
				{else}
				<img src="design/{$settings->theme|escape}/images/nophoto-lg.png" class="img-responsive" alt="Нет фото" title="Нет фото">
				{/if}
			</div>
		</div>
		<div class="col-sm-6">
			<h3  itemprop="name">{$product->name|escape} </h3>
			<span class="price"  itemprop="offers" itemscope itemtype="http://schema.org/Offer">
				{foreach $product->variants as $v}
				{if $v->compare_price > 0}<span class="compare_price">{$v->compare_price|convert}</span>{/if}
				<span class="amount"  itemprop="price">{$v->price|convert} <span class="currency"  itemprop="priceCurrency">{$currency->sign|escape}</span></span>
				{/foreach}
			</span>
			{if $product->pre_order < 1}
			{if $v->actual_date}<small>Цена и остаток актуальны на {$v->actual_date}</small>{/if}
				{/if}
				{if $product->body}
				<!-- Описание товара -->
				<p id="readmore" itemprop="description">
					{$product->body}
				</p>
				{if $product->variants|count > 0}
				<hr class="no-border"/>
				{/if}
				{/if}
				
				{if $product->variants|count > 0}
				<div class="product">
					<div class="description">
						<!-- Выбор варианта товара -->
						<form class="variants form-inline" action="/cart">
							<table class="hidden">
								{foreach $product->variants as $v}
								<tr class="variant">
									<td>
										<input id="product_{$v->id}" name="variant" value="{$v->id}" type="radio" class="variant_radiobutton" {if $product->variant->id==$v->id}checked{/if} {if $product->variants|count<2}style="display:none;"{/if}/>
									</td>
									<td>
										{if $v->name}<label class="variant_name" for="product_{$v->id}">{$v->name}</label>{/if}
									</td>
									<td>
										{if $v->compare_price > 0}<span class="compare_price">{$v->compare_price|convert}</span>{/if}
										<span class="price">{$v->price|convert} <span class="currency">{$currency->sign|escape}</span></span>
									</td>
								</tr>
								{/foreach}
							</table>
							<div class="form-group"><input id="buyButton" type="submit" class="button btn btn-primary" value="в корзину" data-result-text="добавлено"/></div>
							<div class="form-group"><div class="ks-widget" data-template="button" data-merchant-sku="{$v->sku}" data-merchant-code="PROFIT" data-city="591010000"></div></div>
						</form>
						<!-- Выбор варианта товара (The End) -->
					</div>
					<!-- Описание товара (The End)-->
				</div>
				{else}
				<hr class="xs-margin no-border"/>
				<div class="alert alert-default" role="alert">Нет в наличии</div>
				{/if}
				
				<table class="table cart-total">
					{if $v->sku}
					
					<tr>
						<th>Артикул:</th>
						<td>{$v->sku}</td>
					</tr>
					{/if}
					{if $product->features}
					{foreach $product->features as $f}
					<tr>
						<th>{$f->name}</th>
						<td class="text-muted">{$f->value}</td>
					</tr>
					{/foreach}		
					{/if}
				</table>
				<div class="visible-xs">
					<ul class="list-unstyled">
						<li><a href="/warranty">Гарантия и возврат </a></li>
						<li><a href="/shipping">Доставка </a></li>
						<li><a href="/how-to-pay">Оплата </a></li>
					</ul>
				</div>
				
				
				
				{if $v->stock > 0}
				<p  class="text-success"><strong><i class="fa fa-check"></i> Наличие в магазинах:</strong></p>
				<div class="panel-group" id="accordion-2">
					{if $v->m1 > 0}
					<div class="panel panel-simple">
						<div class="panel-heading">
							<h4 class="panel-title">
							<a class="collapsed" data-toggle="collapse" data-parent="#accordion-2" href="#collapseFirst"><i class="fa fa-location-arrow fa-fw"></i>AppleSin на Букетова</a>
						</h4>
					</div>
					<div id="collapseFirst" class="panel-collapse collapse">
						<div class="panel-body">
							<dl class="dl-horizontal"><dt>Режим работы:</dt><dd><p><time>Ежедневно, с 10:00 до 20:00</time></p></dd><dt>Адрес:</dt><dd><address><span>Букетова, 57</span><br /> <span > 150000</span> <span>Петропавловск</span></address></dd><dt>Телефоны:</dt><dd><p><span>+7(707)037-09-65</span><br /> <span>+7(702)887-24-44</span></p></dd></dl>
						</div>
					</div>
				</div>
				{/if}
				{if $v->m4 > 0}
				<div class="panel panel-simple">
					<div class="panel-heading">
						<h4 class="panel-title">
							<a class="collapsed" data-toggle="collapse" data-parent="#accordion-2" href="#collapseSecond"><i class="fa fa-location-arrow fa-fw"></i>AppleSin в Рахмете</a>
						</h4>
					</div>
					<div id="collapseSecond" class="panel-collapse collapse">
						<div class="panel-body">
							<dl class="dl-horizontal"><dt>Режим работы:</dt><dd><p><time>Ежедневно, с 10:00 до 21:00</time></p></dd><dt>Адрес:</dt><dd><div itemprop="location">ТЦ "Рахмет"</div><address><span>К.Сутюшева, 58б</span> <span>Петропавловск</span></address></dd><dt>Телефоны:</dt><dd><p><span>+7(707)515-18-88</span><br /> <span>+7(708)607-08-88</span></p></dd></dl>
						</div>
					</div>
				</div>
				{/if}
				{if $v->m3 > 0}
				<div class="panel panel-simple">
					<div class="panel-heading">
						<h4 class="panel-title">
							<a class="collapsed" data-toggle="collapse" data-parent="#accordion-2" href="#collapseThird"><i class="fa fa-location-arrow fa-fw"></i>AppleSin в Пирамиде</a>
						</h4>
					</div>
					<div id="collapseThird" class="panel-collapse collapse">
						<div class="panel-body">
							<dl class="dl-horizontal"><dt>Режим работы:</dt><dd><p><time>Ежедневно, с 10:00 до 20:00</time></p></dd><dt>Адрес:</dt><dd><div itemprop="location">ТД "Пирамида"</div><address><span>Батыр Баяна, 11</span> <span>Петропавловск</span></address></dd><dt>Телефоны:</dt><dd><p><span>+7(776)976-88-88</span><br /> <span>+7(747)370-09-87</span></p></dd></dl>
						</div>
					</div>
				</div>
				{/if}
				{if $v->m2 > 0}
				<div class="panel panel-simple">
					<div class="panel-heading">
						<h4 class="panel-title">
							<a class="collapsed" data-toggle="collapse" data-parent="#accordion-2" href="#collapseFour"><i class="fa fa-location-arrow fa-fw"></i>Мини AppleSin в Минимаркете</a>
						</h4>
					</div>
					<div id="collapseFour" class="panel-collapse collapse">
						<div class="panel-body">
							<dl class="dl-horizontal"><dt>Режим работы:</dt><dd><p><time>Ежедневно, с 10:00 до 20:00</time></p></dd><dt>Адрес:</dt><dd><div itemprop="location">маг. "Минимаркет"</div><address><span>ул. Абая, 41</span> <span>Петропавловск</span></address></dd><dt>Телефоны:</dt><dd><p><span>+7(747)151-84-44</span><br /> <span>+7(705)794-54-44</span></p></dd></dl>
						</div>
					</div>
				</div>
				{/if}
				{if $v->Alm > 0}
				<div class="panel panel-simple">
					<div class="panel-heading">
						<h4 class="panel-title">
							<a class="collapsed" data-toggle="collapse" data-parent="#accordion-2" href="#collapseFour"><i class="fa fa-location-arrow fa-fw"></i>Склад Алматы</a>
						</h4>
					</div>
					<div id="collapseFour" class="panel-collapse collapse">
						<div class="panel-body">
							<p>Бесплатная доставка в Петропавловск в течении 2-8 рабочих дней.</p>
						</div>
					</div>
				</div>
				{/if}

			</div>
			{/if}
		</div>
	</div>
	<!-- /.container -->
	<!-- SLIDER -->
	<div class="container slider product-slider text-center">
		<header class="page-header">
			<h3>Соседние товары</h3>
		</header>
		<ul  data-auto="true" data-fx="slide" data-interval="3000">
			{foreach $related_products as $related_product}
			<li class="product">
				<div class="product-img">
					{if $related_product->image}
					<a href="products/{$related_product->url}">
						<img width="200" height="200" src="{$related_product->image->filename|resize:200:200}" title="{$related_product->name|escape}" alt="{$related_product->name|escape}"/>
					</a>
					{else}
					<img width="200" height="200" src="design/{$settings->theme|escape}/images/nophoto.png" alt="Нет фото">
					{/if}
				</div>
				<h6><a data-product="{$related_product->id}" href="products/{$related_product->url}">{$related_product->name|escape}</a></h6>
				<span class="price">
					{if $related_product->variants|count > 0}
					{foreach $related_product->variants as $v}
					{if $v->compare_price > 0}<span class="compare_price">{$v->compare_price|convert}</span>{/if}
					<span class="amount">{$v->price|convert} <span class="currency">{$currency->sign|escape}</span></span>
					{/foreach}
					{else}
					Нет в наличии
					{/if}
				</span>
			</li>
			{/foreach}
		</ul>
		<nav class="nav-pages"></nav>
	</div>
	<!-- /.slider -->
	<!-- COMMENTS -->
	<div class="container">
		<div class="col-md-12 comments" id="comments">
			<h3>Комментарии</h3>
			<div class="comment clearfix">
				{if $comments}
				<!-- Список с комментариями -->
				{foreach $comments as $comment}
				<div class="comment clearfix">
					<div class="comment-line">
						<p>
							{$comment->name|escape},
							{$comment->date|date}, {$comment->date|time}
							{if !$comment->approved}<span class="label label-default">ожидает модерации</span>{/if}
							<a  name="comment_{$comment->id}" class="internal"></a>
						</p>
					</div>
					<p>{$comment->text|escape|nl2br}</p>
				</div>
				{/foreach}
				{else}
				<p>Пока нет комментариев</p>
				{/if}
			</div>
		</div>
		<div class="col-md-9 comments" id="comments">
			<!-- Add Comment -->
			<div class="add-comment" id="addcomment">
				<a role="button" data-toggle="collapse" href="#collapseComment" aria-expanded="false" aria-controls="collapseComment"><h3>Написать комментарий</h3></a>
				{if $error}
				<div class="alert alert-warning">
					{if $error=='captcha'}
					Подтвердите что вы не робот
					{elseif $error=='empty_name'}
					Введите имя
					{elseif $error=='empty_comment'}
					Введите комментарий
					{/if}
				</div>
				{/if}
				<div class="collapse" id="collapseComment">
					<form class="comment_form" method="post">
						<div class="form-wrap">
							<div class="form-group">
								<input class="input_name" type="text" id="comment_name" name="name" value="{$comment_name}" data-format=".+" data-notice="Введите имя" placeholder="Ваше имя"/>
							</div>
							<div class="form-group">
								<textarea class="comment_textarea" id="comment_text" name="text" data-format=".+" data-notice="Введите комментарий" placeholder="Комментарий">{$comment_text}</textarea>
							</div>
						</div>
						<div class="g-recaptcha" data-sitekey="{$settings->site_code|escape}" data-size="compact"></div>
						<input class="btn btn btn-light" type="submit" name="comment" value="Отправить" />
					</form>
				</div>
			</div>
			<!-- /.add-comment -->
		</div>
	</div>
	<!-- /.comments -->
	<!-- Kaspi-->
	{literal}
	<script>(function(d, s, id) {
		var js, kjs;
		if (d.getElementById(id)) return;
		js = d.createElement(s); js.id = id;
		js.src = 'https://kaspi.kz/kaspibutton/widget/ks-wi_ext.js';
		kjs = document.getElementsByTagName(s)[0]
		kjs.parentNode.insertBefore(js, kjs);
	}(document, 'script', 'KS-Widget'));</script>
	{/literal}
	<!-- ReadMore -->
	{if $product->body}<script src="design/{$settings->theme}/js/readmore.min.js"></script>
	<script>
		$('p').readmore({
			speed: 1000,
			maxHeight: 200
		});
	</script>
{/if}