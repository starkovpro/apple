{* Страница с формой обратной связи *}
{* Канонический адрес страницы *}
{$canonical="/{$page->url}" scope=parent}
<div class="visible-xs">
<div class="mobilehead inforow">
<a href="#"><img src="design/{$settings->theme|escape}/images/logo.png" title="Перейти на главную страницу" alt="AppleSin logo"/></a>
</div>
</div>
<div class="headcontent">
	<div class="container">
		<div class="col-sm-12">
			<h1>Обратная связь</h1>
		</div>
	</div>
</div>
<!-- CONTAINER -->
<div class="container inforow">
	<div class="col-md-6 col-sm-8">
		{$page->body}
		{if $message_sent}
		{$name|escape}, ваше сообщение было успешно отправлено! Спасибо за обращение в службу поддержки клиентов магазина «AppleSin». Ваше обращение будет рассмотрено в ближайшее время.
		{else}
		<form class="conact-form" method="post">
			{if $error}
			<div class="alert alert-warning">
				{if $error=='captcha'}
				Подтвердите что вы не робот
				{elseif $error=='empty_name'}
				Введите имя
				{elseif $error=='empty_email'}
				Введите email
				{elseif $error=='empty_text'}
				Введите сообщение
				{/if}
			</div>
			{/if}
			<div class="form-group">
				<label>Имя</label>
				<input data-format=".+" data-notice="Введите имя" value="{$name|escape}" name="name" maxlength="255" type="text"/>
			</div>
			<div class="form-group">
				<label>Email</label>
				<input data-format="email" data-notice="Введите email" value="{$email|escape}" name="email" maxlength="255" type="text"/>
			</div>
			<div class="form-group">
			<p>Если хотите чтобы мы перезвонили, укажите в сообщении свой контактный телефон.</p>
				<label>Сообщение</label>
				<textarea data-format=".+" data-notice="Введите сообщение" value="{$message|escape}" name="message">{$message|escape}</textarea>
			</div>
			<div class="g-recaptcha" data-sitekey="{$settings->site_code|escape}"  data-size="compact"></div>
			<input class="btn btn-light" type="submit" name="feedback" value="Отправить" />
		</form>
		{/if}
	</div>
</div>
<!-- /.container -->