{* Страница отдельной записи блога *}
{* Канонический адрес страницы *}
{$canonical="/blog/{$post->url}" scope=parent}
<div class="visible-xs">
<div class="mobilehead inforow">
<a href="#"><img src="design/{$settings->theme|escape}/images/logo.png" title="Перейти на главную страницу" alt="AppleSin logo"/></a>
</div>
</div>
<!-- HEADCONTENT -->
<div class="headcontent">
	<div class="container">
		<div class="col-md-8">
			<!-- Заголовок /-->
			<h1>{$post->name|escape}</h1>
				<ol class="breadcrumb">
				<li><a href="/">Главная</a></li>
				<li><a href="blog">Блог</a></li>
				<li>{$post->name|escape}</li>
			</ol>
		</div>
		<div class="col-md-4 text-right">
				<!-- Вход пользователя -->
				{if $user}
				<span id="username">
					<a href="user">{$user->name}</a>{if $group->discount>0},
					ваша скидка &mdash; {$group->discount}%{/if}
				</span> / 
				<a id="logout" href="user/logout">выйти</a>
				{else}
				<a id="register" href="user/register">Регистрация</a> / 
				<a id="login" href="user/login">Вход</a>
				{/if}
				<!-- Вход пользователя (The End)-->
		</div>
	</div>
</div>
<!-- /.headcontent -->
<!-- Заголовок /-->
<div class="container">
	<div class="post format-standart">
		<div class="col-xs-12 entry">
			<header class="entry-header">
				<div class="meta">
					<small><i class="fa fa-calendar"></i>{$post->date|date}</small>
				</div>
			</header>
			<div class="entry-content">
				{if $post->image }<img src="{$config->root_url}/{$config->blog_images_dir}{$post->image}"  class="img-responsive" alt="{$post->name|escape}" title="{$post->name|escape}" />{/if}
				<p>{$post->text}</p>
			</div>
			</div>
		<!-- Соседние записи /-->
		<div class="col-xs-12 hidden">
			<nav class="nav-single clearfix">
				{if $prev_post}
				<div class="nav-previous text-left">
					<a href="blog/{$prev_post->url}">
						<small>Предыдущая запись</small>
						<span>{$prev_post->name}</span>
					</a>
				</div>
				{/if}
				{if $next_post}
				<div class="nav-next text-right">
					<a href="blog/{$next_post->url}">
						<small>Следующая запись</small>
						<span>{$next_post->name}</span>
					</a>
				</div>
			</nav>
			{/if}
		</div>
		<!-- /.post -->
	</div>
	<hr class="sm-margin"/>
	<!-- COMMENTS -->
	<div class="col-md-12 comments" id="comments">
		<h3>Комментарии</h3>
		<div class="comment clearfix">
			{if $comments}
			<!-- Список с комментариями -->
			{foreach $comments as $comment}
			<div class="comment clearfix">
				<div class="comment-line">
					<p>
						{$comment->name|escape},
						{$comment->date|date}, {$comment->date|time}
						{if !$comment->approved}<span class="label label-default">ожидает модерации</span>{/if}
						<a  name="comment_{$comment->id}" class="internal"></a>
					</p>
				</div>
				<p>{$comment->text|escape|nl2br}</p>
			</div>
			{/foreach}
			{else}
			<p>Пока нет комментариев</p>
			{/if}
		</div>
	</div>
	<div class="col-md-8 comments sm-margin" id="comments">
		<!-- Add Comment -->
		<div class="add-comment" id="addcomment">
			<h3>Написать комментарий</h3>
			{if $error}
			<div class="alert alert-warning">
				{if $error=='captcha'}
				Подтвердите что вы не робот
				{elseif $error=='empty_name'}
				Введите имя
				{elseif $error=='empty_comment'}
				Введите комментарий
				{/if}
			</div>
			{/if}
			<form class="comment_form" method="post">
				<div class="form-wrap">
					<div class="form-group">
						<input class="input_name" type="text" id="comment_name" name="name" value="{$comment_name}" data-format=".+" data-notice="Введите имя" placeholder="Ваше имя"/>
					</div>
					<div class="form-group">
						<textarea class="comment_textarea" id="comment_text" name="text" data-format=".+" data-notice="Введите комментарий" placeholder="Комментарий">{$comment_text}</textarea>
					</div>
				</div>
				<div class="g-recaptcha" data-sitekey="{$settings->site_code|escape}" data-size="compact"></div>
				<input class="btn btn-light" type="submit" name="comment" value="Отправить" />
			</form>
		</div>
		<!-- /.add-comment -->
	</div>
</div>
<script src="/js/baloon/js/default.js" language="JavaScript" type="text/javascript"></script>
<script src="/js/baloon/js/validate.js" language="JavaScript" type="text/javascript"></script>
<script src="/js/baloon/js/baloon.js" language="JavaScript" type="text/javascript"></script>
<link href="/js/baloon/css/baloon.css" rel="stylesheet" type="text/css" />