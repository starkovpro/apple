{* Шаблон корзины *}
{$meta_title = "Корзина" scope=parent}
<div class="headcontent">
	<div class="container">
		<div class="col-sm-12 text-center">
			<h1>
				{if $cart->purchases}В корзине {$cart->total_products} {$cart->total_products|plural:'товар':'товаров':'товара'}
				{else}Корзина пуста{/if}
			</h1>
			<ol class="breadcrumb">
				<a href="/">Главная</a>
				/ Корзина
			</ol>
		</div>
	</div>
</div>
<!-- CONTAINER -->
{if $cart->purchases}
<form method="post" name="cart" class="">
	<div class="container cart">
		<div class="col-sm-12">
			<div class="table-responsive">
				{* Список покупок *}
				<table id="purchases" class="table text-center">
					<thead>
						<tr>
							<th></th>
							<th class="text-left">Товар и цена</th>
							<th class="text-center">Количество</th>
							<th class="text-center">Итого</th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						{foreach from=$cart->purchases item=purchase}
						{* Изображение товара *}
						<tr>
							<td class="product-thumbnail text-left">
								{$image = $purchase->product->images|first}
								{if $image}
								<a href="products/{$purchase->product->url}"><img src="{$image->filename|resize:100:100}" alt="{$product->name|escape}"></a>
								{/if}
							</td>
							{* Название товара *}
							<td class="product-name text-left">
								<a href="products/{$purchase->product->url}">{$purchase->product->name|escape}</a>
								{$purchase->variant->name|escape}
								<span class="price">
									<span class="amount">{($purchase->variant->price)|convert} {$currency->sign}</span>
								</span>
							</td>
							{* Количество допилить *}
							<td class="amount">
								<select name="amounts[{$purchase->variant->id}]" onchange="document.cart.submit();">
									{section name=amounts start=1 loop=$purchase->variant->stock+1 step=1}
									<option value="{$smarty.section.amounts.index}" {if $purchase->amount==$smarty.section.amounts.index}selected{/if}>{$smarty.section.amounts.index} {$settings->units}</option>
									{/section}
								</select>
							</td>
							<td class="product-subtotal">{($purchase->variant->price*$purchase->amount)|convert}&nbsp;{$currency->sign}</td>
							<td class=" text-center">
								<a href="cart/remove/{$purchase->variant->id}"><img src="design/{$settings->theme|escape}/images/close.png" title="Удалить из корзины" alt="Удалить из корзины"></a>
							</td>
						</tr>
						{/foreach}
					</tbody>
				</table>
				<div class="col-sm-4 col-lg-offset-8">
					<table class="table cart-total">
						<tr class="total">
							<th>Итого:</th>
							<td class="text-primary">{$cart->total_price|convert}&nbsp;{$currency->sign}</td>
						</tr>
					</table>
				</div>
			</div>
			{* Доставка *}
		</div>
	</div>
	{if $deliveries}
	<!-- CONTAINER -->
	<div class="container no-padding-top">
		<div class="col-md-12">
			<h2>Выберите способ доставки:</h2>
			<div class="panel-group payment-wrap" id="payment">
				{foreach $deliveries as $delivery}
				<div class="panel clearfix">
					<div class="radio">
						<label>
							<input type="radio" name="delivery_id" value="{$delivery->id}" {if $delivery_id==$delivery->id}checked{elseif $delivery@first}checked{/if} id="deliveries_{$delivery->id}">
							<strong>{$delivery->name}
								{if $cart->total_price < $delivery->free_from && $delivery->price>0}
									({$delivery->price|convert}&nbsp;{$currency->sign})
									{elseif $cart->total_price >= $delivery->free_from}
									(бесплатно)
								{/if}</strong>
							</label>
						</div>
						<div id="{$delivery->id}">
							<p>{$delivery->description}</p>
						</div>
					</div>
					{/foreach}
				</div>
				{* Выбор способа оплаты *}
				{if $payment_methods && !$payment_method}
				<h2>Выберите способ оплаты</h2>
				
							<div class="panel-group payment-wrap" id="deliveries">
				{foreach $payment_methods as $payment_method}
				<div class="panel clearfix">
					<div class="radio">
						<label>
							<input type=radio name=payment_method_id value='{$payment_method->id}' {if $payment_method@first}checked{/if} id=payment_{$payment_method->id}>
							<strong>{$payment_method->name}{*, к оплате {$cart->total_price|convert:$payment_method->currency_id}&nbsp;{$all_currencies[$payment_method->currency_id]->sign}*}</strong>
							</label>
						</div>
						<div id="{$delivery->id}">
							<p>{$payment_method->description}</p>
						</div>
					</div>
					{/foreach}
				</div>
				{/if}
			</div>
		</div>
		{/if}
		<!-- /.container -->
		<!-- CONTAINER -->
		<div class="container no-padding-top">
			<div class="col-md-6">
				<h2>Адрес получателя:</h2>
				
				<div class="form cart_form">
					{if $error}
					<div class="alert alert-warning">
						{if $error == 'empty_name'}Введите имя{/if}
						{if $error == 'empty_email'}Введите email{/if}
						{if $error == 'captcha'}Подтвердите что вы не робот{/if}
					</div>
					{/if}
					<div class="form-group">
						<input name="name" type="text" value="{$name|escape}" data-format=".+" data-notice="Введите имя" placeholder="Ваше имя, фамилия"/>
					</div>
					<div class="form-group">
						<input name="email" type="text" value="{$email|escape}" data-format="email" data-notice="Введите email" placeholder="Ваш email"/>
					</div>
					<div class="form-group">
						<input name="phone" type="text" value="{$phone|escape}" placeholder="Телефон"/>
					</div>
					<div class="form-group">
						<input name="address" type="text" value="{$address|escape}" placeholder="Адрес доставки. Город, улица, дом, квартира, индекс."/>
					</div>
					<div class="form-group">
						<textarea name="comment" id="order_comment" placeholder="Комментарий к&nbsp;заказу">{$comment|escape}</textarea>
					</div>
					<div class="form-group">
						<div class="g-recaptcha" data-sitekey="{$settings->site_code|escape}" data-size="compact"></div>
					</div>
					<input type="submit" name="checkout" class="btn btn-default btn-extra btn-lg" value="Оформить заказ">
				</div>
			</div>
		</div>
	</form>
	{else}
    <!-- CONTAINER -->
    <div class="container text-center">
        <h4>В корзине нет товаров</h4>
        <a href="catalog/iphone" class="btn btn-primary">Перейти в магазин</a>
		{* Рекомендуемые товары *}
{get_featured_products var=featured_products}
{if $featured_products}
<!-- SLIDER -->
<div class="container slider product-slider text-center">
	<header class="page-header">
		<h3>Мы рекомендуем посмотреть:</h3>
	</header>
	<div class="col-md-12">
		<ul data-auto="true" data-fx="slide" data-interval="3000">
			{foreach $featured_products as $product}
			<li class="product">
				<div class="product-img">
					{if $product->image}
					<a href="products/{$related_product->url}">
						<img width="200" src="{$product->image->filename|resize:200:200}" alt="{$product->name|escape}" title="{$product->name|escape}"/>
					</a>
					{else}
					<img width="200" src="design/{$settings->theme|escape}/images/nophoto.png" alt="Нет фото">
					{/if}
				</div>
				<h6><a data-product="{$product->id}" href="products/{$product->url}">{$product->name|escape}</a></h6>
				<span class="price">
					{if $product->variants|count > 0}
					{foreach $product->variants as $v}
					{if $v->compare_price > 0}<span class="compare_price">{$v->compare_price|convert}</span>{/if}
					<span class="amount">{$v->price|convert} <span class="currency">{$currency->sign|escape}</span></span>
					{/foreach}
					{else}
					Нет в наличии
					{/if}
				</span>
			</li>
			{/foreach}
		</ul>
	</div>
	<nav class="nav-pages"></nav>
</div>
<!-- /.slider -->
{/if}
    </div>
    <!-- /.container -->
	{/if}
</div>
<!-- /.container -->