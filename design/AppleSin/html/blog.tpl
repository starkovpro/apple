{* Список записей блога *}
{* Канонический адрес страницы *}
{$canonical="/blog" scope=parent}
<div class="visible-xs">
<div class="mobilehead inforow">
<a href="#"><img src="design/{$settings->theme|escape}/images/logo.png" title="Перейти на главную страницу" alt="AppleSin logo"/></a>
</div>
</div>
<div class="headcontent">
	<div class="container">
		<div class="col-md-8">
			<h1>{$page->name}{$page->body}</h1>
			<ol class="breadcrumb">
				<li><a href="/">Главная</a></li>
				<li>Блог</li>
			</ol>
		</div>
		<div class="col-md-4 text-right">
				<!-- Вход пользователя -->
				{if $user}
				<span id="username">
					<a href="user">{$user->name}</a>{if $group->discount>0},
					ваша скидка &mdash; {$group->discount}%{/if}
				</span> / 
				<a id="logout" href="user/logout">выйти</a>
				{else}
				<a id="register" href="user/register">Регистрация</a> / 
				<a id="login" href="user/login">Вход</a>
				{/if}
				<!-- Вход пользователя (The End)-->
		</div>
	</div>
</div>
{include file='pagination.tpl'}
<!-- POST: Standart -->
{foreach $posts as $post}
{if $post->category == 2}
<div class="container post format-standart">
	<div class="col-xs-12 entry">
		<header class="entry-header">
			<h2><a data-post="{$post->id}" href="blog/{$post->url}">{$post->name|escape}</a></h2>
			<div class="meta">
				<small><i class="fa fa-calendar"></i>{$post->date|date}</small>
			</div>
		</header>
		<div class="entry-content">
			{if $post->image }<img src="{$config->root_url}/{$config->blog_images_dir}{$post->image}" class="img-responsive" alt="{$post->name|escape}" title="{$post->name|escape}"/>{/if}
			<p>{$post->annotation}</p>
			<a href="blog/{$post->url}" class="btn btn-primary">Читать далее</a>
		</div>
	</div>
	<!-- /.post -->
</div>
{/if}
{/foreach}
{include file='pagination.tpl'}

