{* Шаблон текстовой страницы *}
{* Канонический адрес страницы *}
{$canonical="/{$page->url}" scope=parent}
<div class="visible-xs">
	<div class="mobilehead inforow">
		<a href="#"><img src="design/{$settings->theme|escape}/images/logo.png" title="Перейти на главную страницу" alt="AppleSin logo"/></a>
	</div>
</div>
<div class="headcontent">
	<div class="container">
		<div class="col-md-8">
			<!-- Заголовок страницы -->
			<h1 data-page="{$page->id}">{$page->header|escape}</h1>
		</div>
		<div class="col-md-4 text-right">
			<!-- Вход пользователя -->
			{if $user}
			<span id="username">
				<a href="user">{$user->name}</a>{if $group->discount>0},
				ваша скидка &mdash; {$group->discount}%{/if}
			</span> / 
			<a id="logout" href="user/logout">выйти</a>
			{else}
			<a id="register" href="user/register">Регистрация</a> / 
			<a id="login" href="user/login">Вход</a>
			{/if}
			<!-- Вход пользователя (The End)-->
		</div>
	</div>
</div>
<!-- /.headcontent -->
{if $smarty.server.REQUEST_URI == "/predzakaz-iphone-7-iphone-7-plus"}
<div class="container">
	<div class="row">
		<div class="col-sm-3 hidden-middle"> 
			<p> 
				<h4>iPhone 7 4.7 дюймовый дисплей</h4>
			<h5 id="price" class="text-warning">Цена: 310990 тг.</h5>
			</p>
			<form id="preorderform1">
				<input type="hidden" name="model" value="iPhone 7" />
				<div class="form-group">
					<label>Выберите цвет:</label>
					<select id="color1" name="color" class="form-control" onchange="document.getElementById('preview1').src = 'design/{$settings->theme|escape}/images/7/' + this.value">
						<option value="iphone7-jetblack-select.png">Глянцевый чёрный*</option>
						<option value="iphone7-black-select.png">Матовый чёрный</option>
						<option value="iphone7-silver.png">Серебристый</option>
						<option value="iphone7-gold-select.png">Золотой</option>
						<option value="iphone7-rosegold-select.png">Розовое золото</option>
					</select>
				</div>
				<div class="form-group">
					<label>Выберите объем памяти:</label>
					<select id="memory1" name="memory" class="form-control" onchange="document.getElementById('price').innerHTML = 'Цена: '+ this.value">
						<option value="310990 тг.">32GB</option>
						<option value="359990 тг.">128GB</option>
						<option value="409990 тг.">256GB</option>
					</select>
				</div>
				<div id="ph1" class="form-group">
					<label>Ваш номер телефона:</label>
					<input type="tel" id="phone1" name="phone" class="form-control" placeholder="Номер телефона">
				</div>
				<div class="form-group">
					<input class="btn btn-default btn-block" type="button" value="Заказать" id="preorder1" name="preorder1" >
				</div>
				<div id="succesalert1" class="alert alert-success" role="alert">Заказ успешно отправлен, мы скоро перезвоним Вам.</div>
			</form>
		</div>
		<div class="hidden-xs col-sm-3 middle hidden-middle text-center"><img id="preview1" class="iphone7" src="design/{$settings->theme|escape}/images/7/iphone7-jetblack-select.png" height="305"></div>
		<div class="hidden-xs col-sm-3 middle hidden-middle text-center"><img id="preview2" src="design/{$settings->theme|escape}/images/7/iphone7-plus-jetblack-select.png" height="352"></div>
		<div class="col-sm-3  hidden-middle"> 
		<hr class="visible-xs"/>
			<p><h4>iPhone 7 Plus 5.5 дюймовый дисплей</h4>
			<h5 id="price2" class="text-warning">Цена: 369990 тг.</h5>
			</p>
			<form id="preorderform2">
				<input type="hidden" name="model" value="iPhone 7 Plus" />
				<div class="form-group">
					<label>Выберите цвет:</label>
					<select id="color2" name="color" class="form-control" onchange="document.getElementById('preview2').src = 'design/{$settings->theme|escape}/images/7/' + this.value">
						<option value="iphone7-plus-jetblack-select.png">Глянцевый чёрный*</option>
						<option value="iphone7-plus-black-select.png">Матовый чёрный</option>
						<option value="iphone7-plus-silver-select.png">Серебристый</option>
						<option value="iphone7-plus-gold-select.png">Золотой</option>
						<option value="iphone7-plus-rosegold-select.png">Розовое золото</option>
					</select>
				</div>
				<div class="form-group">
					<label>Выберите объем памяти:</label>
					<select id="memory2" name="memory"  class="form-control" onchange="document.getElementById('price2').innerHTML = 'Цена: '+ this.value">
						<option value="369990 тг.">32GB</option>
						<option value="419990 тг.">128GB</option>
						<option value="465990 тг.">256GB</option>
					</select>
				</div>
				<div id="ph2" class="form-group">
					<label>Ваш номер телефона:</label>
					<input type="tel" class="form-control" id="phone2" name="phone" placeholder="Номер телефона">
				</div>
				<div class="form-group">
					<input class="btn btn-default btn-block" type="button" value="Заказать" id="preorder2" name="preorder2">
				</div>
				<div id="succesalert2" class="alert alert-success" role="alert">Заказ успешно отправлен, мы скоро перезвоним Вам.</div>
			</form>
		</div>
		<div class="col-sm-12 text-center"> 
			<hr/>
			<p>* Цвет глянцевый черный доступен только в версиях на 128GB и 256GB. <br>Не получается заказать? Нужна консультация? Звоните нам +7(707)037-09-65</p>
		</div>
	</div>
</div>
{literal}
<script type="text/javascript">
	
	$( "#memory1" ).change(function() {
  alert( "Handler for .change() called." );
});
	
	jQuery(document).ready(function($)
	{
		$( "#succesalert1" ).hide();
		$( "#succesalert2" ).hide();
		$('#preorder1').click(function(e)
		{
			$( "#succesalert1" ).slideUp();
			var val = $("#phone1").val();
			val = val.replace(/\D+/g,"");    
			if(val.length > 5)
			{
				$("#ph1").removeClass("has-error");
				e.preventDefault();
				var error = false;                        
				if(error == false)
				{				
					$.post("http://"+window.location.hostname+"/magic.php"
					, $("#preorderform1").serialize()
					,function(result)
					{
						if(result=='sent') 
						{
							$( "#succesalert1" ).slideDown();
						} 
						else 
						{
							alert("Ошибка");    
						}
					});
				};
			}
			else
			{
				$("#ph1").addClass("has-error");
			}
		});
		
		$('#preorder2').click(function(e)
		{
			$( "#succesalert2" ).slideUp();
			var val = $("#phone2").val();
			val = val.replace(/\D+/g,"");    
			if(val.length > 5)
			{
				$("#ph2").removeClass("has-error");
				e.preventDefault();
				var error = false;                        
				if(error == false)
				{				
					$.post("http://"+window.location.hostname+"/magic.php"
					, $("#preorderform2").serialize()
					,function(result)
					{
						if(result=='sent') 
						{
							$( "#succesalert2" ).slideDown();
						} 
						else 
						{
							alert("Ошибка");    
						}
					});
				};
			}
			else
			{
				$("#ph2").addClass("has-error");
			}
		});
		
		
		
		
	}); 
</script>
{/literal}
{/if}
{if $smarty.server.REQUEST_URI == "/about"}
<!-- CONTAINER -->
<div class="container inforow skills">
	<div class="row">
		<div class="col-sm-8 col-sm-offset-2 text-center">
			<h2>Чем мы занимаемся</h2>
			<p class="text-muted">Продажа и обслуживание Apple iPhone. У нас огромный выбор аксессуаров и большой перечень предоставляемых услуг.</p>
			<hr />
		</div>
	</div>
	<div class="row">
		<div class="col-md-4 col-sm-6 middle hidden-middle">
			<div class="col-sm-3 text-center">
				<div class="icon icon-sm painted" data-icon="Q"></div>
			</div>
			<div class="col-sm-9">
				<h4>Продажа iPhone</h4>
				<p>На каждый купленный iPhone действует фирменная гарантия 1 год.</p>
			</div>
			<hr class="no-border sm-margin"/>
			<div class="col-sm-3 text-center">
				<div class="icon icon-sm painted" data-icon="L"></div>
			</div>
			<div class="col-sm-9">
				<h4>Продажа аксессуаров для iPhone</h4>
				<p>Чехлы, кабели, зарядки, защитные стекла и пленки и многое другое</p>
			</div>
		</div>
		<div class="col-sm-4 hidden-xs hidden-sm">
			<img src="design/{$settings->theme|escape}/images/iphone6s.png" alt="iPhone 6s" title="iPhone 6s" data-animate="zoomIn" class="animate">
		</div>
		<div class="col-md-4 col-sm-6 middle hidden-middle">
			<div class="col-sm-3 text-center">
				<div class="icon icon-sm painted" data-icon="#"></div>
			</div>
			<div class="col-sm-9">
				<h4>Обслуживание iPhone, настройка</h4>
				<p>Apple ID, прошивка, настройка. Мы решим любую проблему с вашим iPhone.</p>
			</div>
			<hr class="no-border sm-margin"/>
			<div class="col-sm-3 text-center">
				<div class="icon icon-sm painted" data-icon="$"></div>
			</div>
			<div class="col-sm-9">
				<h4>Ремонт iPhone</h4>
				<p>Замена дисплеев, кнопок, шлейфов и многое другое. Даем гарантию на выполненный ремонт! </p>
			</div>
		</div>
	</div>
</div>
<!-- /.container -->
{/if}
{if $smarty.server.REQUEST_URI == "/repair"}
<!-- статус ремонта -->
<div class="jumbotron no-height overlay bg-repair-section-parallax" data-stellar-background-ratio=".5">
	<div class="container middle text-center">
		<div class="col-md-10 col-md-offset-1">
			<h1>Узнайте статус <mark>ремонта</mark></h1>
			<p>Введите номер квитанции полученной у нашего мастера. За дополнительной информацией обращайтесь в сервис-центр по телефонам.</p>
			<div class="col-md-10 col-md-offset-1"> 
				<div id="feedback-form">
					<form class="form-inline" role="form" id='feedback' >
						<div class="form-group"><input type="text" class="input-lg" id="kvitok" name="kvitok" placeholder="№ Квитанции"></div>
						<div class="form-group"><button type="button" class="btn btn-default" id="send_message3">Узнать</button></div>
					</form>
					<br/>
					<div class="alert alert-success" id="repairStatusSuccess"><p class="white">Статус вашего ремонта:</p></div>
					<div class="alert alert-warning" id="repairStatusEmpty"><p class="white">Введите номер квитанции!</p></div>
					<div class="alert alert-warning" id="repairStatusBad"><p class="white">Номер квитанции не найден или недействителен. Проверьте правильность заполения поля.</p></div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- /статус ремонта -->
{literal}
<!-- Статус ремонта -->				
<script type="text/javascript">
	$(document).ready(function(){
		$(".alert").css("display", "none");
	});
	$("#send_message3").click(function(){
		$('#repairStatusSuccess').slideUp();
		$('#repairStatusEmpty').slideUp();
		$('#repairStatusBad').slideUp();
		var kvitok = $('#kvitok').val();
		if(kvitok.length == 0){
			$('#repairStatusEmpty').slideDown();
			return;
		}
		$.ajax({
			type: "GET",
			url: "php/repair.php",
			data: "kvitok=" + kvitok,
			success: function(msg){
				if(msg=="")
				{
					$('#repairStatusBad').slideDown();
					return;
				}
				else
				{
					$('#repairStatusSuccess').html(msg)
					$('#repairStatusSuccess').slideDown();
					return;
				}
			}
		});
	});
</script>
<!-- Сортировка таблицы, фильтр -->
<script>
	$( "#all" ).click(function() {
		$("tr").show();
	});
	$( "#iPhone4" ).click(function() {
		$("tr").show();
		$("tr:not(.iPhone4)").hide();
	});
	$( "#iPhone4s" ).click(function() {
		$("tr").show();
		$("tr:not(.iPhone4s)").hide();
	});
	$( "#iPhone5" ).click(function() {
		$("tr").show();
		$("tr:not(.iPhone5)").hide();
	});
	$( "#iPhone5s" ).click(function() {
		$("tr").show();
		$("tr:not(.iPhone5s)").hide();
	});
	$( "#iPhone5c" ).click(function() {
		$("tr").show();
		$("tr:not(.iPhone5c)").hide();
	});
	$( "#iPhone6" ).click(function() {
		$("tr").show();
		$("tr:not(.iPhone6)").hide();
	});
	$( "#iPhone6plus" ).click(function() {
		$("tr").show();
		$("tr:not(.iPhone6plus)").fadeOut();
	});
</script>
<!-- Предзаказ -->
{/literal}
{/if}
<!-- Тело страницы --> 
{$page->body}
{if $smarty.server.REQUEST_URI == "/repair"}
<!-- прайс на ремонт -->
{/if}
{if $smarty.server.REQUEST_URI == "/contacts"}
<script src="http://maps.googleapis.com/maps/api/js?v=3.exp&#038;sensor=false&#038;ver=1"></script>
<!-- MAP -->
<div class="google-map ">
	<div class="google-map-container google-map-big" data-longitude="69.138866" data-latitude="54.865368" data-zoom="16"></div>
	<a href="#google-map-popup" class="a-map" data-toggle="modal"><i class="fa fa-search"></i></a>
</div>
<!-- /.google-map -->
<!-- Modal -->
<div class="modal map-modal" id="google-map-popup">
	<a href="#" class="map-close" data-dismiss="modal"><i></i></a>
	<div class="google-map-popup"></div>
</div>
<!-- /.modal -->
<!-- /.modal -->
{/if}
{if $smarty.server.REQUEST_URI == "/news"}
{* Выбираем в переменную $last_posts последние записи *}
{get_posts var=last_posts}
{if $last_posts}
{foreach $last_posts as $post}
{if $post->category==1}
<div class="container post format-standart">
	<div class="col-xs-12 entry">
		<header class="entry-header">
			<h2><a data-post="{$post->id}" href="blog/{$post->url}">{$post->name|escape}</a></h2>
			<div class="meta">
				<small><i class="fa fa-calendar"></i>{$post->date|date}</small>
			</div>
		</header>
		<div class="entry-content">
			{if $post->image }<img src="{$config->root_url}/{$config->blog_images_dir}{$post->image}" class="img-responsive" alt="{$post->name|escape}" title="{$post->name|escape}"/>{/if}
			<p>{$post->annotation}</p>
			<a href="blog/{$post->url}" class="btn btn-primary">Читать далее</a>
		</div>
	</div>
	<!-- /.post -->
</div> 
{/if}
{/foreach}
{/if}  
{/if}