{* Страница регистрации *}
{* Канонический адрес страницы *}
{$canonical="/user/register" scope=parent}
{$meta_title = "Регистрация" scope=parent}
<div class="visible-xs">
<div class="mobilehead inforow">
<a href="#"><img src="design/{$settings->theme|escape}/images/logo.png" title="Перейти на главную страницу" alt="AppleSin logo"/></a>
</div>
</div>
<!-- HEADCONTENT -->
<div class="headcontent">
	<div class="container">
		<div class="col-md-8">
			<h1>Регистрация</h1>
		</div>
		<div class="col-md-4">
			<!-- Ulogin -->
			<script src="http://ulogin.ru/js/ulogin.js"></script>
			<div id="uLogin" data-ulogin="display=panel;fields=first_name,last_name,email,city;providers=vkontakte,odnoklassniki,mailru,instagram;hidden=other;redirect_uri=http://applesin.com.kz/user/login"></div>
		</div>
	</div>
</div>
<!-- /.headcontent -->  
<!-- CONTAINER -->
<div class="container inforow">
	<div class="col-md-6 col-sm-8">
		{if $error}
		<div class="alert alert-warning">
			{if $error == 'empty_name'}Введите имя
			{elseif $error == 'empty_email'}Введите email
			{elseif $error == 'empty_password'}Введите пароль
			{elseif $error == 'user_exists'}Пользователь с таким email уже зарегистрирован
			{elseif $error == 'captcha'}Подтвердите что вы не робот
			{else}{$error}{/if}
		</div>
		{/if}
		<p class="text-muted">Уже зарегистрированны? <a href="user/login">Войдите</a></p>
		<form class="login-form"  method="post">
			<div class="form-group">
				<label for="name">Имя</label>
				<input type="text" name="name" data-format=".+" data-notice="Введите имя" value="{$name|escape}" maxlength="255" />
			</div>
			<div class="form-group">
				<label for="email">Email</label>
				<input type="text" name="email" data-format="email" data-notice="Введите email" value="{$email|escape}" maxlength="255" />
			</div>		
			<div class="form-group">
				<label for="phone">Телефон</label>
				<input type="text" name="phone" data-format=".+" data-notice="Введите телефон" value="{$phone|escape}" maxlength="20" />
			</div>
			<div class="form-group">
				<label for="password">Пароль</label>
				<input type="password" name="password" data-format=".+" data-notice="Введите пароль" value="" />
			</div>
			<div class="g-recaptcha" data-sitekey="{$settings->site_code|escape}"  data-size="compact"></div>
			<div class="form-group">
				<input type="submit" class="btn btn-primary" name="register" value="Зарегистрироваться">
			</div>
		</form>
	</div>
</div>