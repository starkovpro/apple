{* Шаблон страницы зарегистрированного пользователя *}
<div class="visible-xs">
<div class="mobilehead inforow">
<a href="#"><img src="design/{$settings->theme|escape}/images/logo.png" title="Перейти на главную страницу" alt="AppleSin logo"/></a>
</div>
</div>
<!-- HEADCONTENT -->
<div class="headcontent">
	<div class="container">
		<div class="col-sm-12">
			<h1>Личный кабинет - {$user->name|escape}</h1>
		</div>
	</div>
</div>
<!-- /.headcontent -->  
<div class="container inforow">
	<div class="col-md-6 col-sm-8">
		{if $error}
		<div class="message_error">
			{if $error == 'empty_name'}Введите имя
			{elseif $error == 'empty_email'}Введите email
			{elseif $error == 'empty_password'}Введите пароль
			{elseif $error == 'user_exists'}Пользователь с таким email уже зарегистрирован
			{else}{$error}{/if}
		</div>
		{/if}
		<!-- <a href="http://applesin.kz/gd.php?ID={$user->id|escape:'url'}&FIO={$name|escape:'url'}&Phone={$phone|escape:'url'}" target="_blank">Дисконтная карта</a> -->
		<img src="http://applesin.kz/gd.php?ID={$user->id|escape:'url'}&FIO={$name|escape:'url'}&Phone={$phone|escape:'url'}" />
		<form method="post">
			<div class="form-group">
				<label>Ваше имя:</label>
				<input data-format=".+" data-notice="Введите имя" value="{$name|escape}" name="name" maxlength="255" type="text"/>
			</div>
			<div class="form-group">
				<label>Ваш Email:</label>
				<input data-format="email" data-notice="Введите email" value="{$email|escape}" name="email" maxlength="255" type="text"/>
			</div>				
			<div class="form-group">
				<label>Ваш телефон:</label>
				<input data-format=".+" data-notice="Введите телефон" value="{$phone|escape}" name="phone" maxlength="20" type="text"/>
			</div>	
			<input id="password" value="" name="password" type="password" style="display:none;"/>
			<input class="btn btn-primary" type="submit" class="button" value="Сохранить"><p><a href='#' onclick="$('#password').show();return false;">Изменить пароль</a></p>
		</form>
		{php}

{/php}
<h2>Ваши заказы:</h2>
		{if $orders}
		<p></p>	
		<ul class="flat" id="orders_history">
			{foreach name=orders item=order from=$orders}
			<li>
				{$order->date|date} <a href='order/{$order->url}'>Заказ №{$order->id}</a>
				{if $order->paid == 1}оплачен,{/if} 
				{if $order->status == 0}ждет обработки{elseif $order->status == 1}в обработке{elseif $order->status == 2}выполнен{/if}
			</li>
			{/foreach}
		</ul>
		{else}
		Вы еще ничего не покупали :( 
		{/if}
	</div>
</div>
<!-- /.container -->