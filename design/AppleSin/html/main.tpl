{* Главная страница магазина *}
{* Для того чтобы обернуть центральный блок в шаблон, отличный от index.tpl *}
{* Укажите нужный шаблон строкой ниже. Это работает и для других модулей *}
{$wrapper = 'index.tpl' scope=parent}
{* Канонический адрес страницы *}
{$canonical="" scope=parent}
{* Заголовок страницы *}
<div class="visible-xs">
	<div class="mobilehead inforow">
		<a href="#"><img src="design/{$settings->theme|escape}/images/logo.png" title="Перейти на главную страницу" alt="AppleSin logo"/></a>
	</div>
</div>

<!-- CONTAINER -->
<div class="container">
	<div class="row">
	<div class="col-md-12">
	
	</div>
	</div>
	<div class="row">
		<div class="col-sm-8 col-sm-offset-2 text-center">
			<h2>Новости и акции</h2>
		</div>
	</div>
	<div class="row">
		{* Выбираем в переменную $last_posts последние записи *}
		{get_posts var=last_posts}
		{if $last_posts}
		{foreach $last_posts as $post}
		{if $post->category==1}
		<div class="col-md-6 col-xs-12 post format-image">
			<h3>
				<a href="blog/{$post->url}">{$post->name|escape}</a>
			</h3>
			{if $post->image }<a href="blog/{$post->url}"><img src="{$config->root_url}/{$config->blog_images_dir}{$post->image}" alt="{$post->name|escape}" title="{$post->name|escape}"/></a>{/if}
			<p>{$post->annotation}</p>
		</div>
		{/if}
		{/foreach}
		{/if}
	</div>
</div>

<!-- /.container -->

<!-- CONTAINER subcat-->

<div class="container">
	<header class="page-header text-center">
		<h3>Аксессуары по категориям</h3>
	</header>
	
			<div class="row">
				{* Рекурсивная функция вывода дерева категорий *}
				{function name=categories_treeinproductpage}
				{if $categories}
				{foreach $categories as $c1}
				{if $c1->visible}
				{foreach $c1->subcategories as $c2}
				{if $c2->visible}
				<div class="col-sm-6 col-md-4 subcat" onclick="location.href = 'catalog/{$c2->url}';">
					{if $c2->image}<img src="{$config->categories_images_dir}{$c2->image}" alt="{$c2->name|escape}" >{/if}
					<a href="catalog/{$c2->url}" data-category="{$c2->id}">{$c2->name}</a>
				</div>
				{/if}
				{/foreach}
				{/if}
				{/foreach}                  
				{/if}
				{/function}
				{categories_treeinproductpage categories=$categories}                  
			</div>

</div>

<!-- /.container -->
{* Рекомендуемые товары *}
{get_featured_products var=featured_products}
{if $featured_products}
<!-- SLIDER -->
<div class="container slider product-slider text-center">
	<header class="page-header">
	<h3>Мы рекомендуем посмотреть</h3>
	</header>
	<div class="col-md-12">
		<ul data-auto="true" data-fx="slide" data-interval="3000">
			{foreach $featured_products as $product}
			<li class="product">
				<div class="product-img">
					{if $product->image}
					<a href="products/{$product->url}">
						<img width="200" src="{$product->image->filename|resize:200:200}" alt="{$product->name|escape}" title="{$product->name|escape}"/>
					</a>
					{else}
					<img width="200" src="design/{$settings->theme|escape}/images/nophoto.png" alt="Нет фото">
					{/if}
				</div>
				<h6><a data-product="{$product->id}" href="products/{$product->url}">{$product->name|escape}</a></h6>
				<span class="price">
					{if $product->variants|count > 0}
					{foreach $product->variants as $v}
					{if $v->compare_price > 0}<span class="compare_price">{$v->compare_price|convert}</span>{/if}
					<span class="amount">{$v->price|convert} <span class="currency">{$currency->sign|escape}</span></span>
					{/foreach}
					{else}
					Нет в наличии
					{/if}
				</span>
			</li>
			{/foreach}
		</ul>
	</div>
	<nav class="nav-pages"></nav>
</div>
<!-- /.slider -->
{/if}
{* Акционные товары *}
{get_discounted_products var=discounted_products limit=9}
{if $discounted_products}
<!-- SLIDER -->
<div class="container slider product-slider text-center">
	<header class="page-header">
		<h3>Товары со скидкой:</h3>
	</header>
	<div class="col-md-12">
		<ul  data-auto="true" data-fx="slide" data-interval="3000">
			{foreach $discounted_products as $product}
			<li class="product">
				<div class="product-img">
					{if $product->image}
					<a href="products/{$product->url}">
						<img width="200" src="{$product->image->filename|resize:200:200}" alt="{$product->name|escape}" title="{$product->name|escape}"/>
					</a>
					{else}
					<img width="200" src="design/{$settings->theme|escape}/images/nophoto.png" alt="Нет фото">
					{/if}
				</div>
				<h6><a data-product="{$product->id}" href="products/{$product->url}">{$product->name|escape}</a></h6>
				<span class="price">
					{if $product->variants|count > 0}
					{foreach $product->variants as $v}
					{if $v->compare_price > 0}<span class="compare_price">{$v->compare_price|convert}</span>{/if}
					<span class="amount">{$v->price|convert} <span class="currency">{$currency->sign|escape}</span></span>
					{/foreach}
					{else}
					Нет в наличии
					{/if}
				</span>
			</li>
			{/foreach}
		</ul>
	</div>
	<nav class="nav-pages"></nav>
</div>
<!-- /.slider -->
{/if}
<!-- CONTAINER -->
<div class="container inforow">
	<div class="col-sm-4">
		<h2 class="painted">Отзывы и предложения</h2>
	</div>
	<div class="col-sm-8">
		<p>Мы всегда рады общению с нашими клиентами. Если хотите поблагодарить нас или, наоборот, обратить внимание на недостатки, напишите нам.  Так же вы можете написать нам предложение, замечание по работе, отзыв или любой другой вопрос. </p>
		<a class="btn btn-primary" href="contact">Написать нам</a>
	</div>
</div>
<!-- /.container -->
<div class="bg-primary hidden-xs">
	<!-- SLIDER -->
	<div class="container slider oneslider inforow">
		<ul>
			<li>
				<div class="col-sm-3 col-sm-offset-1">
					<img src="design/{$settings->theme|escape}/images/clients/nophoto.png" class="img-circle" alt="Нет фото" title="Нет фото">
				</div>
				<div class="col-sm-7 middle hidden-middle">
					<blockquote>
						<h3>“Отличное сервисное обслуживание молодцы, перед тем как пойти к вам ходил в другой сервис по ремонту телефона сказали что мой телефон не подлежит ремонту типа плата не годна, знакомые подсказали сходить AppleSin и урааа оказалось просто экран накрылся!!! Спасибо вам!!! ”</h3>
					</blockquote>
					<h5>Талгат<small>наш постоянный клиент</small></h5>
				</div>
			</li>
			<li>
				<div class="col-sm-3 col-sm-offset-1">
					<img src="design/{$settings->theme|escape}/images/clients/2.png" class="img-circle" alt="Садовский Алексей" title="Садовский Алексей">
				</div>
				<div class="col-sm-7 middle hidden-middle">
					<blockquote>
						<h3>“Ребята молодцы, выбор аксессуаров просто огромный! С продавцами приятно и удобно работать. Так держать, AppleSin!”</h3>
					</blockquote>
					<h5>Садовский Алексей<small>26 лет, дизайн-студия "Полиграф Полиграфыч", г. Петропавловск</small></h5>
				</div>
			</li>
			<li>
				<div class="col-sm-3 col-sm-offset-1">
					<img src="design/{$settings->theme|escape}/images/clients/1.png" class="img-circle" alt="Зелянин Константин" title="Зелянин Константин">
				</div>
				<div class="col-sm-7 middle hidden-middle">
					<blockquote>
						<h3>“Давно хотел себе хороший смартфон. Был проездом в Петропавловске, посоветовали заглянуть в AppleSin. Продавец, если не ошибаюсь, Евгений, очень грамотный парень, разрекламировал так, что у меня не оставалось выбора кроме как купить iPhone 6s. Спасибо, AppleSin!”</h3>
					</blockquote>
					<h5>Зелянин Константин<small>24 года, владелец Apple iPhone 6s, г. Омск</small></h5>
				</div>
			</li>
		</ul>
		<div class="arrow prev"><i></i></div>
		<div class="arrow next"><i></i></div>
		<nav class="nav-pages visible-xs"></nav>
	</div>
	<!-- /.slider -->
</div>
<!-- NEWSLETTER -->
<div id="subscribe" class="newsletter bg-sl-fscreen-6 bg-sl-center overlay overlay-light">
	<div class="container inforow middle">
		<div class="col-sm-7 col-sm-offset-1">
			<p>Подпишитесь на новости и акции</p>
		</div>
		<div class="col-sm-2">
			<div id="feedback-form">			
				<a href="http://eepurl.com/cb87YD" target="_blank" class="btn btn-default btn-lg">Подписаться</a>			
			</div>
		</div>
	</div>
</div>
<!-- /.newsletter -->	