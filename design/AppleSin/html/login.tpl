{* Страница входа пользователя *}

{* Канонический адрес страницы *}
{$canonical="/user/login" scope=parent}

{$meta_title = "Вход" scope=parent}
<div class="visible-xs">
<div class="mobilehead inforow">
<a href="#"><img src="design/{$settings->theme|escape}/images/logo.png" title="Перейти на главную страницу" alt="AppleSin logo"/></a>
</div>
</div>
<!-- HEADCONTENT -->
<div class="headcontent">
	<div class="container">
		<div class="col-sm-12">
			<h1>Вход</h1>
		</div>
	</div>
</div>
<!-- /.headcontent -->  
<!-- CONTAINER -->
<div class="container inforow">
	<div class="col-md-6 col-sm-8">
		<p class="text-muted">Еще не зарегистрированны? <a href="user/register">Зарегистрируйтесть сейчас</a></p>
		{if $error}
		<div class="alert alert-warning fade in">
			{if $error == 'login_incorrect'}Неверный логин или пароль
			{elseif $error == 'user_disabled'}Ваш аккаунт еще не активирован.
			{else}{$error}{/if}
		</div>
		{/if}
		<form class="login-form"  method="post">
			<div class="form-group">
				<label for="username">Логин</label>
				<input type="text" name="email" data-format="email" data-notice="Введите email" value="{$email|escape}" maxlength="255" />
			</div>
			<div class="form-group">
				<label for="password">Пароль</label>
				<input type="password" name="password" data-format=".+" data-notice="Введите пароль" value="" />
			</div>
			<input class="btn btn-primary" type="submit" name="login" value="Войти">
		</form>
		<p><a href="user/password_remind" class="text-primary">Забыли пароль?</a></p>
	</div>
</div>
<!-- /.container -->