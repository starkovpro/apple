<?php
//var_dump(gd_info()); //client_card_source.png

$img = imagecreatefrompng('client_card_source.png');
$font_file_arial = './ArialMT.ttf';
$font_file_ocr = './OCRAEXT.TTF';

$text1 = str_pad($_GET["ID"], 4, "0", STR_PAD_LEFT);//*$_GET["ID"];//"0005";
$text2 = $_GET["FIO"];//"Иванов Иван22";
$text3 = $_GET["Phone"];//"+77076845445";

//$bbox1 = imagettfbbox(19, 0, $font_file_ocr, $text1);
//$bbox2 = imagettfbbox(14, 0, $font_file_arial, $text2);
//$bbox3 = imagettfbbox(10, 0, $font_file_arial, $text3);

//var_dump($bbox1);
//var_dump($bbox2);
//var_dump($bbox3);


$color1 = imagecolorallocate($img, 255, 227, 0);
$color2 = imagecolorallocate($img, 255, 255, 255);
$color3 = imagecolorallocate($img, 255, 255, 255);

imagettftext($img, 60, 0, 100, 436, $color1, $font_file_ocr, $text1);
imagettftext($img, 60, 0, 100, 697, $color2, $font_file_arial, $text2);
imagettftext($img, 35, 0, 352, 777, $color3, $font_file_arial, $text3);

header('Content-type: image/png');
imagepng($img);
imagedestroy($img);
?>