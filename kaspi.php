<?php
//header("Content-type: text/xml; charset=UTF-8");
ob_start();
require_once('api/Simpla.php');
$simpla = new Simpla();

$LastModified = gmdate('D, d M Y H:i:s T', time());
header('Last-Modified: '. $LastModified);

print (pack('CCC', 0xef, 0xbb, 0xbf));

print "<?xml version='1.0' encoding='utf-8'?>

<kaspi_catalog date='string'

              xmlns='kaspiShopping'

          xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance'

 xsi:schemaLocation='kaspiShopping http://kaspi.kz/kaspishopping.xsd'>

    <company>PROFIT</company>

    <merchantid>PROFIT</merchantid>

	<offers>
	";
// Валюты
$currencies = $simpla->money->get_currencies(array('enabled'=>1));
$main_currency = reset($currencies);


// Категории
$categories = $simpla->categories->get_categories();

	// Товары
//$simpla->db->query("SET SQL_BIG_SELECTS=1");
// Товары
$simpla->db->query("SELECT v.m1, v.m2, v.m3, v.m4, v.price, v.id AS variant_id, p.name AS product_name, v.name AS variant_name, v.position AS variant_position, v.sku AS variant_sku, p.id AS product_id, p.url, p.annotation, pc.category_id, i.filename AS image, b.name AS brand
					FROM __variants v LEFT JOIN __products p ON v.product_id=p.id
					LEFT JOIN s_brands b ON b.id = p.brand_id
					LEFT JOIN __products_categories pc ON p.id = pc.product_id AND pc.position=(SELECT MIN(position) FROM __products_categories WHERE product_id=p.id LIMIT 1)	
					LEFT JOIN __images i ON p.id = i.product_id AND i.position=(SELECT MIN(position) FROM __images WHERE product_id=p.id LIMIT 1)	
					WHERE p.visible AND p.to_kaspi AND (v.stock >0 OR v.stock is NULL) GROUP BY v.id ORDER BY p.id, v.position ");

$currency_code = reset($currencies)->code;

$prev_product_id = null;
while($p = $simpla->db->result())
{
$prev_product_id = $p->product_id;

$price = round($simpla->money->convert($p->price, $main_currency->id, false),2);
$m1 = $p->m1;
$m2 = $p->m2;
$m3 = $p->m3;
$m4 = $p->m4;
			print"<offer sku='".$p->variant_sku."'>
			";

            print "<model>".htmlspecialchars($p->product_name).($p->variant_name?' '.htmlspecialchars($p->variant_name):'')."</model>
			";

            print "<brand>".$p->brand."</brand>
			";

            print "	<availabilities>
			";

            if ($m1>0){
			print"	<availability available='yes' storeId='PP1'/>
				";}
				else{
				print"<availability available='no' storeId='PP1'/>
				";	
				}
				;
            if ($m2>0){print"<availability available='yes' storeId='PP2'/>
				";}
								else{
				print"<availability available='no' storeId='PP2'/>
				";	
				};
            if ($m3>0){print"<availability available='yes' storeId='PP3'/>
				";}
								else{
				print"<availability available='no' storeId='PP3'/>
				";	
				};
            if ($m4>0){print"<availability available='yes' storeId='PP4'/>
				";}
								else{
				print"<availability available='no' storeId='PP4'/>
				";	
				};

		print "</availabilities>
			";

            print "<price>".$price."</price>
			";

        print "</offer>
			";
		
}
print "
	</offers>

</kaspi_catalog>";
$string = ob_get_clean();
header("Content-type: text/xml; charset=UTF-8");
header(sprintf("Content-Length: %s", strlen($string)));
echo $string;
?>




