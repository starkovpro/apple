<?
/*
����� ��� �������������� � API ���������
*/
class vkapi {
  private $app_id;
  private $secret;
  private $access_token;
  private $v;
  private $server_token;

  
  function __construct ($app_id , $app_secret , $v) {
    $this-> app_id = $app_id;
    $this-> secret = $app_secret;
    $this-> v = $v;
  }
  
  function authcode ($code , $rurl) {
    $ch = curl_init('https://oauth.vk.com/access_token?client_id=' . $this-> app_id . '&client_secret=' . $this-> secret .'&code=' . $code . '&redirect_uri=' . $rurl);
    curl_setopt($ch, CURLOPT_POST, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    $r=curl_exec($ch);
    $un=json_decode($r, true);
    if ($un['access_token']!=NULL)
      $this-> access_token = $un['access_token'];
    return $un; 
  }
  
  function authloginpass ($login , $pass) {
    $ch = curl_init('https://oauth.vk.com/token?grant_type=password&client_id=' . $this-> app_id . '&client_secret=' . $this-> secret .'&username=' . $login . '&password=' . $pass);
    curl_setopt($ch, CURLOPT_POST, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    $r=curl_exec($ch);
    $un=json_decode($r, true);
    if ($un['access_token']!=NULL)
      $this-> access_token = $un['access_token'];
    return $un;
  }
  
  function authserver() {
    $ch = curl_init('https://oauth.vk.com/access_token?client_id=' . $this-> app_id . '&client_secret=' . $this-> secret . '&v=' . $this-> v .'&grant_type=client_credentials');
    curl_setopt($ch, CURLOPT_POST, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    $r=curl_exec($ch);
    $un=json_decode($r, true);
    if ($un['access_token'] != NULL){ 
      $this -> server_token = $un['access_token'];
    }
    return $un;
    
      
  }
  
  function api ($method , $param = array() , $type = "json") {
    $param['access_token'] = $this-> access_token;
    $param['v'] = $this-> v;
    if ($type == "xml")
      $method .= ".xml"; 
    $ch = curl_init('https://api.vk.com/method/'.$method);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch , CURLOPT_POSTFIELDS , $param);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    $r=curl_exec($ch);
    if ($type == "array") {
      $un=json_decode($r, true);
      return $un;
    }
    return $r;
  }
  
  function serverapi ($method , $param = array() , $type = "json") {
    $param['access_token'] = $this-> server_token;
    $param['client_secret'] = $this-> secret;
    $param['v'] = $this-> v;
    if ($type == "xml")
      $method .= ".xml"; 
    $ch = curl_init('https://api.vk.com/method/'.$method);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch , CURLOPT_POSTFIELDS , $param);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    $r=curl_exec($ch);
    if ($type == "array") {
      $un=json_decode($r, true);
      return $un;
    }
    return $r;
  }
  
  
  function settoken ($access_token , $server = false) {
    if (!$server)
      $this-> access_token = $access_token;
    else
      $this-> server_token = $access_token;
  }
  
  function gettoken ($server = false) {
    if (!$server)
      return $this-> access_token;
    else
      return $this-> server_token;
  }
  
}
