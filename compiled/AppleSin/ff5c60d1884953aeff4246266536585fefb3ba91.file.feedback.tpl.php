<?php /* Smarty version Smarty-3.1.18, created on 2016-04-10 06:38:09
         compiled from "/var/www/vhosts/v-2612.webspace/www/applesin.com.kz/design/AppleSin/html/feedback.tpl" */ ?>
<?php /*%%SmartyHeaderCode:118204361556e486d3544629-81443489%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'ff5c60d1884953aeff4246266536585fefb3ba91' => 
    array (
      0 => '/var/www/vhosts/v-2612.webspace/www/applesin.com.kz/design/AppleSin/html/feedback.tpl',
      1 => 1460231141,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '118204361556e486d3544629-81443489',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.18',
  'unifunc' => 'content_56e486d35b1278_62138421',
  'variables' => 
  array (
    'page' => 0,
    'settings' => 0,
    'message_sent' => 0,
    'name' => 0,
    'error' => 0,
    'email' => 0,
    'message' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_56e486d35b1278_62138421')) {function content_56e486d35b1278_62138421($_smarty_tpl) {?>

<?php $_smarty_tpl->tpl_vars['canonical'] = new Smarty_variable("/".((string)$_smarty_tpl->tpl_vars['page']->value->url), null, 1);
if ($_smarty_tpl->parent != null) $_smarty_tpl->parent->tpl_vars['canonical'] = clone $_smarty_tpl->tpl_vars['canonical'];?>
<div class="visible-xs">
<div class="mobilehead inforow">
<a href="#"><img src="design/<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['settings']->value->theme, ENT_QUOTES, 'UTF-8', true);?>
/images/logo.png" title="Перейти на главную страницу" alt="AppleSin logo"/></a>
</div>
</div>
<div class="headcontent">
	<div class="container">
		<div class="col-sm-12">
			<h1>Обратная связь</h1>
		</div>
	</div>
</div>
<!-- CONTAINER -->
<div class="container inforow">
	<div class="col-md-6 col-sm-8">
		<?php echo $_smarty_tpl->tpl_vars['page']->value->body;?>

		<?php if ($_smarty_tpl->tpl_vars['message_sent']->value) {?>
		<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['name']->value, ENT_QUOTES, 'UTF-8', true);?>
, ваше сообщение было успешно отправлено! Спасибо за обращение в службу поддержки клиентов магазина «AppleSin». Ваше обращение будет рассмотрено в ближайшее время.
		<?php } else { ?>
		<form class="conact-form" method="post">
			<?php if ($_smarty_tpl->tpl_vars['error']->value) {?>
			<div class="alert alert-warning">
				<?php if ($_smarty_tpl->tpl_vars['error']->value=='captcha') {?>
				Подтвердите что вы не робот
				<?php } elseif ($_smarty_tpl->tpl_vars['error']->value=='empty_name') {?>
				Введите имя
				<?php } elseif ($_smarty_tpl->tpl_vars['error']->value=='empty_email') {?>
				Введите email
				<?php } elseif ($_smarty_tpl->tpl_vars['error']->value=='empty_text') {?>
				Введите сообщение
				<?php }?>
			</div>
			<?php }?>
			<div class="form-group">
				<label>Имя</label>
				<input data-format=".+" data-notice="Введите имя" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['name']->value, ENT_QUOTES, 'UTF-8', true);?>
" name="name" maxlength="255" type="text"/>
			</div>
			<div class="form-group">
				<label>Email</label>
				<input data-format="email" data-notice="Введите email" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['email']->value, ENT_QUOTES, 'UTF-8', true);?>
" name="email" maxlength="255" type="text"/>
			</div>
			<div class="form-group">
			<p>Если хотите чтобы мы перезвонили, укажите в сообщении свой контактный телефон.</p>
				<label>Сообщение</label>
				<textarea data-format=".+" data-notice="Введите сообщение" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['message']->value, ENT_QUOTES, 'UTF-8', true);?>
" name="message"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['message']->value, ENT_QUOTES, 'UTF-8', true);?>
</textarea>
			</div>
			<div class="g-recaptcha" data-sitekey="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['settings']->value->site_code, ENT_QUOTES, 'UTF-8', true);?>
"  data-size="compact"></div>
			<input class="btn btn-light" type="submit" name="feedback" value="Отправить" />
		</form>
		<?php }?>
	</div>
</div>
<!-- /.container --><?php }} ?>
