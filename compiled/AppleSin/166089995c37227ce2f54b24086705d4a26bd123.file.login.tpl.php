<?php /* Smarty version Smarty-3.1.18, created on 2016-04-10 16:23:58
         compiled from "/var/www/vhosts/v-2612.webspace/www/applesin.com.kz/design/AppleSin/html/login.tpl" */ ?>
<?php /*%%SmartyHeaderCode:126594059456e5ce08555b93-40871289%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '166089995c37227ce2f54b24086705d4a26bd123' => 
    array (
      0 => '/var/www/vhosts/v-2612.webspace/www/applesin.com.kz/design/AppleSin/html/login.tpl',
      1 => 1460231150,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '126594059456e5ce08555b93-40871289',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.18',
  'unifunc' => 'content_56e5ce085d53b3_05832836',
  'variables' => 
  array (
    'settings' => 0,
    'error' => 0,
    'email' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_56e5ce085d53b3_05832836')) {function content_56e5ce085d53b3_05832836($_smarty_tpl) {?>


<?php $_smarty_tpl->tpl_vars['canonical'] = new Smarty_variable("/user/login", null, 1);
if ($_smarty_tpl->parent != null) $_smarty_tpl->parent->tpl_vars['canonical'] = clone $_smarty_tpl->tpl_vars['canonical'];?>

<?php $_smarty_tpl->tpl_vars['meta_title'] = new Smarty_variable("Вход", null, 1);
if ($_smarty_tpl->parent != null) $_smarty_tpl->parent->tpl_vars['meta_title'] = clone $_smarty_tpl->tpl_vars['meta_title'];?>
<div class="visible-xs">
<div class="mobilehead inforow">
<a href="#"><img src="design/<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['settings']->value->theme, ENT_QUOTES, 'UTF-8', true);?>
/images/logo.png" title="Перейти на главную страницу" alt="AppleSin logo"/></a>
</div>
</div>
<!-- HEADCONTENT -->
<div class="headcontent">
	<div class="container">
		<div class="col-sm-12">
			<h1>Вход</h1>
		</div>
	</div>
</div>
<!-- /.headcontent -->  
<!-- CONTAINER -->
<div class="container inforow">
	<div class="col-md-6 col-sm-8">
		<p class="text-muted">Еще не зарегистрированны? <a href="user/register">Зарегистрируйтесть сейчас</a></p>
		<?php if ($_smarty_tpl->tpl_vars['error']->value) {?>
		<div class="alert alert-warning fade in">
			<?php if ($_smarty_tpl->tpl_vars['error']->value=='login_incorrect') {?>Неверный логин или пароль
			<?php } elseif ($_smarty_tpl->tpl_vars['error']->value=='user_disabled') {?>Ваш аккаунт еще не активирован.
			<?php } else { ?><?php echo $_smarty_tpl->tpl_vars['error']->value;?>
<?php }?>
		</div>
		<?php }?>
		<form class="login-form"  method="post">
			<div class="form-group">
				<label for="username">Логин</label>
				<input type="text" name="email" data-format="email" data-notice="Введите email" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['email']->value, ENT_QUOTES, 'UTF-8', true);?>
" maxlength="255" />
			</div>
			<div class="form-group">
				<label for="password">Пароль</label>
				<input type="password" name="password" data-format=".+" data-notice="Введите пароль" value="" />
			</div>
			<input class="btn btn-primary" type="submit" name="login" value="Войти">
		</form>
		<p><a href="user/password_remind" class="text-primary">Забыли пароль?</a></p>
	</div>
</div>
<!-- /.container --><?php }} ?>
