<?php /* Smarty version Smarty-3.1.18, created on 2017-10-17 14:23:16
         compiled from "/var/www/vhosts/v-2612.webspace/www/applesin.com.kz/design/AppleSin/html/products.tpl" */ ?>
<?php /*%%SmartyHeaderCode:85527917259e5bdf49b5d81-29513610%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '8e6efd5bca517b7637d934f152c3b0683da5d7aa' => 
    array (
      0 => '/var/www/vhosts/v-2612.webspace/www/applesin.com.kz/design/AppleSin/html/products.tpl',
      1 => 1492361902,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '85527917259e5bdf49b5d81-29513610',
  'function' => 
  array (
    'categories_treeinproductpage' => 
    array (
      'parameter' => 
      array (
      ),
      'compiled' => '',
    ),
  ),
  'variables' => 
  array (
    'category' => 0,
    'brand' => 0,
    'keyword' => 0,
    'settings' => 0,
    'page' => 0,
    'cat' => 0,
    'user' => 0,
    'group' => 0,
    'currencies' => 0,
    'c' => 0,
    'currency' => 0,
    'products' => 0,
    'categories' => 0,
    'features' => 0,
    'f' => 0,
    'o' => 0,
    'config' => 0,
    'product' => 0,
    'v' => 0,
    'sort' => 0,
  ),
  'has_nocache_code' => 0,
  'version' => 'Smarty-3.1.18',
  'unifunc' => 'content_59e5bdf5120592_46992529',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_59e5bdf5120592_46992529')) {function content_59e5bdf5120592_46992529($_smarty_tpl) {?>

<?php if ($_smarty_tpl->tpl_vars['category']->value&&$_smarty_tpl->tpl_vars['brand']->value) {?>
<?php $_smarty_tpl->tpl_vars['canonical'] = new Smarty_variable("/catalog/".((string)$_smarty_tpl->tpl_vars['category']->value->url)."/".((string)$_smarty_tpl->tpl_vars['brand']->value->url), null, 1);
if ($_smarty_tpl->parent != null) $_smarty_tpl->parent->tpl_vars['canonical'] = clone $_smarty_tpl->tpl_vars['canonical'];?>
<?php } elseif ($_smarty_tpl->tpl_vars['category']->value) {?>
<?php $_smarty_tpl->tpl_vars['canonical'] = new Smarty_variable("/catalog/".((string)$_smarty_tpl->tpl_vars['category']->value->url), null, 1);
if ($_smarty_tpl->parent != null) $_smarty_tpl->parent->tpl_vars['canonical'] = clone $_smarty_tpl->tpl_vars['canonical'];?>
<?php } elseif ($_smarty_tpl->tpl_vars['brand']->value) {?>
<?php $_smarty_tpl->tpl_vars['canonical'] = new Smarty_variable("/brands/".((string)$_smarty_tpl->tpl_vars['brand']->value->url), null, 1);
if ($_smarty_tpl->parent != null) $_smarty_tpl->parent->tpl_vars['canonical'] = clone $_smarty_tpl->tpl_vars['canonical'];?>
<?php } elseif ($_smarty_tpl->tpl_vars['keyword']->value) {?>
<?php ob_start();?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['keyword']->value, ENT_QUOTES, 'UTF-8', true);?>
<?php $_tmp1=ob_get_clean();?><?php $_smarty_tpl->tpl_vars['canonical'] = new Smarty_variable("/products?keyword=".$_tmp1, null, 1);
if ($_smarty_tpl->parent != null) $_smarty_tpl->parent->tpl_vars['canonical'] = clone $_smarty_tpl->tpl_vars['canonical'];?>
<?php } else { ?>
<?php $_smarty_tpl->tpl_vars['canonical'] = new Smarty_variable("/products", null, 1);
if ($_smarty_tpl->parent != null) $_smarty_tpl->parent->tpl_vars['canonical'] = clone $_smarty_tpl->tpl_vars['canonical'];?>
<?php }?>
<div class="visible-xs">
<div class="mobilehead inforow">
<a href="#"><img src="design/<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['settings']->value->theme, ENT_QUOTES, 'UTF-8', true);?>
/images/logo.png" title="Перейти на главную страницу" alt="AppleSin logo"/></a>
</div>
</div>
<!-- HEADCONTENT -->
<div class="headcontent">
	<div class="container">
		<div class="col-md-8">	
			<h1>
				<?php if ($_smarty_tpl->tpl_vars['keyword']->value) {?>
				<h1>Поиск <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['keyword']->value, ENT_QUOTES, 'UTF-8', true);?>
</h1>
				<?php } elseif ($_smarty_tpl->tpl_vars['page']->value) {?>
				<h1><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['page']->value->name, ENT_QUOTES, 'UTF-8', true);?>
</h1>
				<?php } else { ?>
				<h1><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['category']->value->name, ENT_QUOTES, 'UTF-8', true);?>
 <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['brand']->value->name, ENT_QUOTES, 'UTF-8', true);?>
 <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['keyword']->value, ENT_QUOTES, 'UTF-8', true);?>
</h1>
			<?php }?></h1>
			<ol class="breadcrumb">
				<a href="/">Главная</a>
				<?php if ($_smarty_tpl->tpl_vars['category']->value) {?>
				<?php  $_smarty_tpl->tpl_vars['cat'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['cat']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['category']->value->path; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['cat']->key => $_smarty_tpl->tpl_vars['cat']->value) {
$_smarty_tpl->tpl_vars['cat']->_loop = true;
?>
				/ <a href="catalog/<?php echo $_smarty_tpl->tpl_vars['cat']->value->url;?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['cat']->value->name, ENT_QUOTES, 'UTF-8', true);?>
</a>
				<?php } ?>  
				<?php if ($_smarty_tpl->tpl_vars['brand']->value) {?> 
				/ <a href="catalog/<?php echo $_smarty_tpl->tpl_vars['cat']->value->url;?>
/<?php echo $_smarty_tpl->tpl_vars['brand']->value->url;?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['brand']->value->name, ENT_QUOTES, 'UTF-8', true);?>
</a>
				<?php }?>
				<?php } elseif ($_smarty_tpl->tpl_vars['brand']->value) {?>
				/ <a href="brands/<?php echo $_smarty_tpl->tpl_vars['brand']->value->url;?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['brand']->value->name, ENT_QUOTES, 'UTF-8', true);?>
</a>
				<?php } elseif ($_smarty_tpl->tpl_vars['keyword']->value) {?>
				/ Поиск
				<?php }?>
			</ol>
		</div>
		<div class="col-md-4 text-right">
			<!-- Вход пользователя -->
			<?php if ($_smarty_tpl->tpl_vars['user']->value) {?>
			<span id="username">
				<a href="user"><?php echo $_smarty_tpl->tpl_vars['user']->value->name;?>
</a><?php if ($_smarty_tpl->tpl_vars['group']->value->discount>0) {?>,
				ваша скидка &mdash; <?php echo $_smarty_tpl->tpl_vars['group']->value->discount;?>
%<?php }?>
			</span> / 
			<a id="logout" href="user/logout">выйти</a>
			<?php } else { ?>
			<a id="register" href="user/register">Регистрация</a>
			<a id="login" href="user/login">Вход</a>
			<?php }?>
			<!-- Вход пользователя (The End)-->
			<p class="pull-right">
				<div id="cart_informer"><?php echo $_smarty_tpl->getSubTemplate ('cart_informer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>
</div>
			</p>		
			<!-- Выбор валюты -->
			
			<?php if (count($_smarty_tpl->tpl_vars['currencies']->value)>1) {?>
			<nav class="nav-currency pull-right">
				<?php  $_smarty_tpl->tpl_vars['c'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['c']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['currencies']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['c']->key => $_smarty_tpl->tpl_vars['c']->value) {
$_smarty_tpl->tpl_vars['c']->_loop = true;
?>
				<?php if ($_smarty_tpl->tpl_vars['c']->value->enabled) {?> 
				<a href='<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['url'][0][0]->url_modifier(array('currency_id'=>$_smarty_tpl->tpl_vars['c']->value->id),$_smarty_tpl);?>
' class="<?php if ($_smarty_tpl->tpl_vars['c']->value->id==$_smarty_tpl->tpl_vars['currency']->value->id) {?>active<?php }?>"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['c']->value->name, ENT_QUOTES, 'UTF-8', true);?>
</a>
				<?php }?>
				<?php } ?>
			</nav>
			<?php }?>
			<!-- Выбор валюты (The End) -->	
		</div>
	</div>
	<a data-toggle="collapse" href="#hc-collapse" class="hc-toggle collapsed"></a>
	<div id="hc-collapse" class="hc-panel panel-collapse collapse bg-warning">
		<div class="container">
			<div class="col-sm-12">
				<p>
					<?php if ($_smarty_tpl->tpl_vars['category']->value->description) {?>
					<?php echo $_smarty_tpl->tpl_vars['category']->value->description;?>

					<?php } else { ?>
					Нет описания :( 
					<?php }?>
				</p>
			</div>
		</div>
	</div>
</div>
<!-- /.headcontent -->
<!--Каталог товаров-->
<hr class="sm-margin no-border"/>
<?php if ($_smarty_tpl->tpl_vars['products']->value) {?>
<!-- PRODUCTS -->
<div class="container products">
	<div class="col-md-3 col-sm-4 col-xs-5 catalog-bar visible-xs">
		<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
			<div class="panel panel-default">
				<div class="panel-heading" role="tab" id="headingOne">
					<h4 class="panel-title">
						<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
							Каталог
						</a>
					</h4>
				</div>
				<div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
					<div class="panel-body">
						
						<?php if (!function_exists('smarty_template_function_categories_treeinproductpage')) {
    function smarty_template_function_categories_treeinproductpage($_smarty_tpl,$params) {
    $saved_tpl_vars = $_smarty_tpl->tpl_vars;
    foreach ($_smarty_tpl->smarty->template_functions['categories_treeinproductpage']['parameter'] as $key => $value) {$_smarty_tpl->tpl_vars[$key] = new Smarty_variable($value);};
    foreach ($params as $key => $value) {$_smarty_tpl->tpl_vars[$key] = new Smarty_variable($value);}?>
						<?php if ($_smarty_tpl->tpl_vars['categories']->value) {?>
						<ul  class="category-list">
							<?php  $_smarty_tpl->tpl_vars['c'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['c']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['categories']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['c']->key => $_smarty_tpl->tpl_vars['c']->value) {
$_smarty_tpl->tpl_vars['c']->_loop = true;
?>
							
							<?php if ($_smarty_tpl->tpl_vars['c']->value->visible) {?>
							<li>
								<a <?php if ($_smarty_tpl->tpl_vars['category']->value->id==$_smarty_tpl->tpl_vars['c']->value->id) {?>class="selected"<?php }?> href="catalog/<?php echo $_smarty_tpl->tpl_vars['c']->value->url;?>
" data-category="<?php echo $_smarty_tpl->tpl_vars['c']->value->id;?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['c']->value->name, ENT_QUOTES, 'UTF-8', true);?>
</a>
								<?php smarty_template_function_categories_treeinproductpage($_smarty_tpl,array('categories'=>$_smarty_tpl->tpl_vars['c']->value->subcategories));?>

							</li>
							<?php }?>
							<?php } ?>
						</ul>
						<?php }?>
						<?php $_smarty_tpl->tpl_vars = $saved_tpl_vars;
foreach (Smarty::$global_tpl_vars as $key => $value) if(!isset($_smarty_tpl->tpl_vars[$key])) $_smarty_tpl->tpl_vars[$key] = $value;}}?>

						<?php smarty_template_function_categories_treeinproductpage($_smarty_tpl,array('categories'=>$_smarty_tpl->tpl_vars['categories']->value));?>

					</div>
				</div>
			</div>

			<?php if ($_smarty_tpl->tpl_vars['features']->value) {?>
			<div class="panel panel-default">
				<div class="panel-heading" role="tab" id="headingThree">
					<h4 class="panel-title">
						<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
							Фильтр
						</a>
					</h4>
				</div>
				<div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
					<div class="panel-body">
						<ul class="category-list">
							<?php  $_smarty_tpl->tpl_vars['f'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['f']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['features']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['f']->key => $_smarty_tpl->tpl_vars['f']->value) {
$_smarty_tpl->tpl_vars['f']->_loop = true;
?>
							<?php echo $_smarty_tpl->tpl_vars['f']->value->name;?>
:
							<li><a href="<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['url'][0][0]->url_modifier(array('params'=>array($_smarty_tpl->tpl_vars['f']->value->id=>null,'page'=>null)),$_smarty_tpl);?>
" <?php if (!$_GET[$_smarty_tpl->tpl_vars['f']->value->key]) {?>class="selected"<?php }?>>Все</a></li>
							<?php  $_smarty_tpl->tpl_vars['o'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['o']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['f']->value->options; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['o']->key => $_smarty_tpl->tpl_vars['o']->value) {
$_smarty_tpl->tpl_vars['o']->_loop = true;
?>
							<li><a href="<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['url'][0][0]->url_modifier(array('params'=>array($_smarty_tpl->tpl_vars['f']->value->id=>$_smarty_tpl->tpl_vars['o']->value->value,'page'=>null)),$_smarty_tpl);?>
" <?php if ($_GET[$_smarty_tpl->tpl_vars['f']->value->key]==$_smarty_tpl->tpl_vars['o']->value->value) {?>class="selected"<?php }?>><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['o']->value->value, ENT_QUOTES, 'UTF-8', true);?>
</a></li>
							<?php } ?>
							<?php } ?>
						</ul>
					</div>
				</div>
			</div>
			<?php }?>
		</div>
	</div>
	<div class="col-md-3 col-sm-4 col-xs-5 catalog-bar hidden-xs">
		<!-- .widget -->
		<div class="widget" id="search">
			<form action="products" class="search-form">
				<div class="form-group">
					<input class="input_search" type="text" name="keyword" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['keyword']->value, ENT_QUOTES, 'UTF-8', true);?>
" placeholder="Поиск товара"/>
				</div>
				<button type="submit"  value=""><i class="fa fa-search"></i></button>
			</form>
		</div>
		<!-- /.widget -->
		<hr/>
		<!-- widget -->
		<div class="widget">
			<h4>Каталог</h4>
			
			<?php if (!function_exists('smarty_template_function_categories_treeinproductpage')) {
    function smarty_template_function_categories_treeinproductpage($_smarty_tpl,$params) {
    $saved_tpl_vars = $_smarty_tpl->tpl_vars;
    foreach ($_smarty_tpl->smarty->template_functions['categories_treeinproductpage']['parameter'] as $key => $value) {$_smarty_tpl->tpl_vars[$key] = new Smarty_variable($value);};
    foreach ($params as $key => $value) {$_smarty_tpl->tpl_vars[$key] = new Smarty_variable($value);}?>
			<?php if ($_smarty_tpl->tpl_vars['categories']->value) {?>
			<ul  class="category-list">
				<?php  $_smarty_tpl->tpl_vars['c'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['c']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['categories']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['c']->key => $_smarty_tpl->tpl_vars['c']->value) {
$_smarty_tpl->tpl_vars['c']->_loop = true;
?>
				
				<?php if ($_smarty_tpl->tpl_vars['c']->value->visible) {?>
				<li>
					<?php if ($_smarty_tpl->tpl_vars['c']->value->image) {?><img src="<?php echo $_smarty_tpl->tpl_vars['config']->value->categories_images_dir;?>
<?php echo $_smarty_tpl->tpl_vars['c']->value->image;?>
" alt="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['c']->value->name, ENT_QUOTES, 'UTF-8', true);?>
"><?php }?>
					<a <?php if ($_smarty_tpl->tpl_vars['category']->value->id==$_smarty_tpl->tpl_vars['c']->value->id) {?>class="selected"<?php }?> href="catalog/<?php echo $_smarty_tpl->tpl_vars['c']->value->url;?>
" data-category="<?php echo $_smarty_tpl->tpl_vars['c']->value->id;?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['c']->value->name, ENT_QUOTES, 'UTF-8', true);?>
</a>
					<?php smarty_template_function_categories_treeinproductpage($_smarty_tpl,array('categories'=>$_smarty_tpl->tpl_vars['c']->value->subcategories));?>

				</li>
				<?php }?>
				<?php } ?>
			</ul>
			<?php }?>
			<?php $_smarty_tpl->tpl_vars = $saved_tpl_vars;
foreach (Smarty::$global_tpl_vars as $key => $value) if(!isset($_smarty_tpl->tpl_vars[$key])) $_smarty_tpl->tpl_vars[$key] = $value;}}?>

			<?php smarty_template_function_categories_treeinproductpage($_smarty_tpl,array('categories'=>$_smarty_tpl->tpl_vars['categories']->value));?>

		</div>
		<!-- /.widget -->
		<hr/>

		
		<?php if ($_smarty_tpl->tpl_vars['features']->value) {?>
		<!-- widget -->
		<div class="widget">
			<ul class="category-list">
				<?php  $_smarty_tpl->tpl_vars['f'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['f']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['features']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['f']->key => $_smarty_tpl->tpl_vars['f']->value) {
$_smarty_tpl->tpl_vars['f']->_loop = true;
?>
				<h4><?php echo $_smarty_tpl->tpl_vars['f']->value->name;?>
:</h4>
				<li><a href="<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['url'][0][0]->url_modifier(array('params'=>array($_smarty_tpl->tpl_vars['f']->value->id=>null,'page'=>null)),$_smarty_tpl);?>
" <?php if (!$_GET[$_smarty_tpl->tpl_vars['f']->value->key]) {?>class="selected"<?php }?>>Все</a></li>
				<?php  $_smarty_tpl->tpl_vars['o'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['o']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['f']->value->options; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['o']->key => $_smarty_tpl->tpl_vars['o']->value) {
$_smarty_tpl->tpl_vars['o']->_loop = true;
?>
				<li><a href="<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['url'][0][0]->url_modifier(array('params'=>array($_smarty_tpl->tpl_vars['f']->value->id=>$_smarty_tpl->tpl_vars['o']->value->value,'page'=>null)),$_smarty_tpl);?>
" <?php if ($_GET[$_smarty_tpl->tpl_vars['f']->value->key]==$_smarty_tpl->tpl_vars['o']->value->value) {?>class="selected"<?php }?>><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['o']->value->value, ENT_QUOTES, 'UTF-8', true);?>
</a></li>
				<?php } ?>
				<?php } ?>
			</ul>
		</div>
		<!-- /.widget -->
		<hr class="no-border"/>
		<?php }?>
	</div>
	<!-- /.catalog-bar -->
	<div class="sort col-md-3 col-sm-4 col-md-offset-6 col-sm-offset-4">
		<select id="select-sorting" onchange="getval(this);">
			<option value="1">Сначала новые</option>
			<option value="2">Цена (сначала дешевые)</option>
			<option value="3">Цена (сначала дорогие)</option>
			<option value="4">По имени (от а до я)</option>
			<option value="5">По имени (от я до а)</option>
		</select>
	</div>
	<div class="col-md-9 col-sm-8 col-xs-7 product-list text-center md-padding-bottom">
		<?php  $_smarty_tpl->tpl_vars['product'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['product']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['products']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['product']->key => $_smarty_tpl->tpl_vars['product']->value) {
$_smarty_tpl->tpl_vars['product']->_loop = true;
?>
		<div class="product">
			<?php if ($_smarty_tpl->tpl_vars['product']->value->image) {?>
			<div  class="product-img">
			<a data-product="<?php echo $_smarty_tpl->tpl_vars['product']->value->id;?>
" href="products/<?php echo $_smarty_tpl->tpl_vars['product']->value->url;?>
"><img src="<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['resize'][0][0]->resize_modifier($_smarty_tpl->tpl_vars['product']->value->image->filename,200,200);?>
" alt="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value->name, ENT_QUOTES, 'UTF-8', true);?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value->name, ENT_QUOTES, 'UTF-8', true);?>
"/></a>
			</div>
			<?php } else { ?>
			<div  class="product-img">
				<a data-product="<?php echo $_smarty_tpl->tpl_vars['product']->value->id;?>
" href="products/<?php echo $_smarty_tpl->tpl_vars['product']->value->url;?>
"><img height="200" src="design/<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['settings']->value->theme, ENT_QUOTES, 'UTF-8', true);?>
/images/nophoto.png" alt="Нет фото" title="Нет фото"/></a>
			</div>
			<?php }?>
		<h6 class="product-name"><a data-product="<?php echo $_smarty_tpl->tpl_vars['product']->value->id;?>
" href="products/<?php echo $_smarty_tpl->tpl_vars['product']->value->url;?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value->name, ENT_QUOTES, 'UTF-8', true);?>
</a></h6>
			<?php if (count($_smarty_tpl->tpl_vars['product']->value->variants)>0) {?>
			<form class="variants" action="/cart">
				<?php  $_smarty_tpl->tpl_vars['v'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['v']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['product']->value->variants; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['v']->key => $_smarty_tpl->tpl_vars['v']->value) {
$_smarty_tpl->tpl_vars['v']->_loop = true;
?>
				<?php if ($_smarty_tpl->tpl_vars['v']->value->compare_price>0) {?><span class="compare_price"><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['convert'][0][0]->convert($_smarty_tpl->tpl_vars['v']->value->compare_price);?>
</span><?php }?>
				<span class="price"><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['convert'][0][0]->convert($_smarty_tpl->tpl_vars['v']->value->price);?>
 <span class="currency"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['currency']->value->sign, ENT_QUOTES, 'UTF-8', true);?>
</span></span>
				<?php } ?>
				<!--<input type="submit" class="button" value="в корзину" data-result-text="добавлено"/>-->
			</form>
			<?php } else { ?>
			Нет в наличии
			<?php }?>
		</div>
		<?php } ?>
		<?php echo $_smarty_tpl->getSubTemplate ('pagination.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

	</div>
	<?php }?>
</div>
<!-- /.product-list -->
<!-- СОРТИРОВКИ -->
<div class="hide">
	<a href="<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['url'][0][0]->url_modifier(array('sort'=>'position','page'=>null),$_smarty_tpl);?>
" id="sort1">по умолчанию</a>
	<a href="<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['url'][0][0]->url_modifier(array('sort'=>'price_asc','page'=>null),$_smarty_tpl);?>
" id="sort2">цена</a>
	<a href="<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['url'][0][0]->url_modifier(array('sort'=>'price_desc','page'=>null),$_smarty_tpl);?>
" id="sort3">цена</a>
	<a href="<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['url'][0][0]->url_modifier(array('sort'=>'name_asc','page'=>null),$_smarty_tpl);?>
" id="sort4">название</a>
	<a href="<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['url'][0][0]->url_modifier(array('sort'=>'name_desc','page'=>null),$_smarty_tpl);?>
" id="sort5">название</a>
</div>
<?php if ($_smarty_tpl->tpl_vars['sort']->value=='price_asc') {?><script>$("#select-sorting").val(2);</script><?php }?>
<?php if ($_smarty_tpl->tpl_vars['sort']->value=='price_desc') {?><script>$("#select-sorting").val(3);</script><?php }?>
<?php if ($_smarty_tpl->tpl_vars['sort']->value=='name_asc') {?><script>$("#select-sorting").val(4);</script><?php }?>
<?php if ($_smarty_tpl->tpl_vars['sort']->value=='name_desc') {?><script>$("#select-sorting").val(5);</script><?php }?>

<script>
	function getval(sel) {
		if (sel.value == 1) {
			document.getElementById('sort1').click();
		}
		if (sel.value == 2) {
			document.getElementById('sort2').click();
		}
		if (sel.value == 3) {
			document.getElementById('sort3').click();
		}
		if (sel.value == 4) {
			document.getElementById('sort4').click();
		}
		if (sel.value == 5) {
			document.getElementById('sort5').click();
		}
	}
</script>
<?php }} ?>
