<?php /* Smarty version Smarty-3.1.18, created on 2017-11-04 18:29:40
         compiled from "/var/www/vhosts/v-2612.webspace/www/applesin.com.kz/design/AppleSin/html/order.tpl" */ ?>
<?php /*%%SmartyHeaderCode:53479172259fdb2b4b592a5-57281858%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '45288a8ad24999bc58dbfbfa8221c7b670df75b1' => 
    array (
      0 => '/var/www/vhosts/v-2612.webspace/www/applesin.com.kz/design/AppleSin/html/order.tpl',
      1 => 1457377897,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '53479172259fdb2b4b592a5-57281858',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'order' => 0,
    'purchases' => 0,
    'purchase' => 0,
    'image' => 0,
    'product' => 0,
    'currency' => 0,
    'settings' => 0,
    'delivery' => 0,
    'payment_methods' => 0,
    'payment_method' => 0,
    'all_currencies' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.18',
  'unifunc' => 'content_59fdb2b4c1dfe9_39970467',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_59fdb2b4c1dfe9_39970467')) {function content_59fdb2b4c1dfe9_39970467($_smarty_tpl) {?>
<?php $_smarty_tpl->tpl_vars['meta_title'] = new Smarty_variable("Ваш заказ №".((string)$_smarty_tpl->tpl_vars['order']->value->id), null, 1);
if ($_smarty_tpl->parent != null) $_smarty_tpl->parent->tpl_vars['meta_title'] = clone $_smarty_tpl->tpl_vars['meta_title'];?>
<!-- HEADCONTENT -->
<div class="headcontent">
	<div class="container">
		<div class="col-md-12 text-center">
			<h1>Спасибо! Ваш заказ №<?php echo $_smarty_tpl->tpl_vars['order']->value->id;?>
 
				<?php if ($_smarty_tpl->tpl_vars['order']->value->status==0) {?>принят<?php }?>
				<?php if ($_smarty_tpl->tpl_vars['order']->value->status==1) {?>в обработке<?php } elseif ($_smarty_tpl->tpl_vars['order']->value->status==2) {?>выполнен<?php }?>
				<?php if ($_smarty_tpl->tpl_vars['order']->value->paid==1) {?>, оплачен<?php } else { ?><?php }?>
			</h1>
		</div>
	</div>
</div>
<!-- /.headcontent -->
<!-- CONTAINER -->
<div class="container cart no-padding-bottom">
	<div class="col-sm-12">
		<div class="table-responsive">
			<table class="table text-center">
				<thead>
                    <tr>
                        <th></th>
                        <th class="text-left">Товар и цена</th>
                        <th class="text-center">Количество</th>
                        <th class="text-center">Итого</th>
					</tr>
				</thead>
				<tbody>
					<?php  $_smarty_tpl->tpl_vars['purchase'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['purchase']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['purchases']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['purchase']->key => $_smarty_tpl->tpl_vars['purchase']->value) {
$_smarty_tpl->tpl_vars['purchase']->_loop = true;
?>
                    <tr>
                        <td class="product-thumbnail text-left">
							<?php $_smarty_tpl->tpl_vars['image'] = new Smarty_variable($_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['first'][0][0]->first_modifier($_smarty_tpl->tpl_vars['purchase']->value->product->images), null, 0);?>
							<?php if ($_smarty_tpl->tpl_vars['image']->value) {?>
							<a href="products/<?php echo $_smarty_tpl->tpl_vars['purchase']->value->product->url;?>
"><img src="<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['resize'][0][0]->resize_modifier($_smarty_tpl->tpl_vars['image']->value->filename,100,100);?>
" alt="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value->name, ENT_QUOTES, 'UTF-8', true);?>
"></a>
							<?php }?>
						</td>
                        <td class="product-name text-left">
                            <a href="/products/<?php echo $_smarty_tpl->tpl_vars['purchase']->value->product->url;?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['purchase']->value->product_name, ENT_QUOTES, 'UTF-8', true);?>
</a>
							<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['purchase']->value->variant_name, ENT_QUOTES, 'UTF-8', true);?>

							<?php if ($_smarty_tpl->tpl_vars['order']->value->paid&&$_smarty_tpl->tpl_vars['purchase']->value->variant->attachment) {?>
							<a class="download_attachment" href="order/<?php echo $_smarty_tpl->tpl_vars['order']->value->url;?>
/<?php echo $_smarty_tpl->tpl_vars['purchase']->value->variant->attachment;?>
">скачать файл</a>
							<?php }?>
                            <span class="price">
                                <span class="amount"><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['convert'][0][0]->convert(($_smarty_tpl->tpl_vars['purchase']->value->price));?>
&nbsp;<?php echo $_smarty_tpl->tpl_vars['currency']->value->sign;?>
</span>
							</span>
						</td>
                        <td class="product-quantity">&times; <?php echo $_smarty_tpl->tpl_vars['purchase']->value->amount;?>
&nbsp;<?php echo $_smarty_tpl->tpl_vars['settings']->value->units;?>
</td>
                        <td class="product-subtotal"><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['convert'][0][0]->convert(($_smarty_tpl->tpl_vars['purchase']->value->price*$_smarty_tpl->tpl_vars['purchase']->value->amount));?>
&nbsp;<?php echo $_smarty_tpl->tpl_vars['currency']->value->sign;?>
</td>
					</tr>
					<?php } ?>
					
					<?php if (!$_smarty_tpl->tpl_vars['order']->value->separate_delivery&&$_smarty_tpl->tpl_vars['order']->value->delivery_price>0) {?>
					<tr>
						<td colspan="3" class="text-right"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['delivery']->value->name, ENT_QUOTES, 'UTF-8', true);?>
:</td>
						<td><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['convert'][0][0]->convert($_smarty_tpl->tpl_vars['order']->value->delivery_price);?>
&nbsp;<?php echo $_smarty_tpl->tpl_vars['currency']->value->sign;?>
</td>
					</tr>
					<?php }?>
					
					<?php if ($_smarty_tpl->tpl_vars['order']->value->separate_delivery) {?>
					<tr>
						<td colspan="3" class="text-right"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['delivery']->value->name, ENT_QUOTES, 'UTF-8', true);?>
:</td>
						<td>0 KZT</td>
					</tr>
					<?php }?>
					
					<tr>
						<td colspan="3" class="text-right">Итого:</td>
						<td><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['convert'][0][0]->convert($_smarty_tpl->tpl_vars['order']->value->total_price);?>
&nbsp;<?php echo $_smarty_tpl->tpl_vars['currency']->value->sign;?>
</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</div>
<!-- /.container -->
<!-- CONTAINER -->
<div class="container no-padding">
	<div class="col-md-6">
		<h3>Контактная информация:</h3>
		<address>
			<?php if ($_smarty_tpl->tpl_vars['order']->value->name) {?>
			<p><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['order']->value->name, ENT_QUOTES, 'UTF-8', true);?>
</p>
			<?php }?>	
			<?php if ($_smarty_tpl->tpl_vars['order']->value->email) {?>
			<p><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['order']->value->email, ENT_QUOTES, 'UTF-8', true);?>
</p>
			<?php }?>	
			<?php if ($_smarty_tpl->tpl_vars['order']->value->phone) {?>
			<p>т. <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['order']->value->phone, ENT_QUOTES, 'UTF-8', true);?>
</p>
			<?php }?>	
			<?php if ($_smarty_tpl->tpl_vars['order']->value->address) {?>
			<p><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['order']->value->address, ENT_QUOTES, 'UTF-8', true);?>
</p>
			<?php }?>	
			<?php if ($_smarty_tpl->tpl_vars['order']->value->comment) {?>
			<p><?php echo nl2br(htmlspecialchars($_smarty_tpl->tpl_vars['order']->value->comment, ENT_QUOTES, 'UTF-8', true));?>
</p>
			<?php }?>	
		</address>
	</div>
</div>
<!-- /.container -->
<div class="container">
	<div class="col-md-6">
		<?php if (!$_smarty_tpl->tpl_vars['order']->value->paid) {?>
	    <h3>Оплата:</h3>
		
		<?php if ($_smarty_tpl->tpl_vars['payment_methods']->value&&!$_smarty_tpl->tpl_vars['payment_method']->value&&$_smarty_tpl->tpl_vars['order']->value->total_price>0) {?>
		<form method="post">
			<h3>Выберите способ оплаты</h3>
			<div class="panel-group payment-wrap" id="deliveries">
				<?php  $_smarty_tpl->tpl_vars['payment_method'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['payment_method']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['payment_methods']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['payment_method']->index=-1;
foreach ($_from as $_smarty_tpl->tpl_vars['payment_method']->key => $_smarty_tpl->tpl_vars['payment_method']->value) {
$_smarty_tpl->tpl_vars['payment_method']->_loop = true;
 $_smarty_tpl->tpl_vars['payment_method']->index++;
 $_smarty_tpl->tpl_vars['payment_method']->first = $_smarty_tpl->tpl_vars['payment_method']->index === 0;
?>
								<div class="panel clearfix">
					<div class="radio">
					<label>
						<input type=radio name=payment_method_id value='<?php echo $_smarty_tpl->tpl_vars['payment_method']->value->id;?>
' <?php if ($_smarty_tpl->tpl_vars['payment_method']->first) {?>checked<?php }?> id=payment_<?php echo $_smarty_tpl->tpl_vars['payment_method']->value->id;?>
>
					<strong><?php echo $_smarty_tpl->tpl_vars['payment_method']->value->name;?>
, к оплате <?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['convert'][0][0]->convert($_smarty_tpl->tpl_vars['order']->value->total_price,$_smarty_tpl->tpl_vars['payment_method']->value->currency_id);?>
&nbsp;<?php echo $_smarty_tpl->tpl_vars['all_currencies']->value[$_smarty_tpl->tpl_vars['payment_method']->value->currency_id]->sign;?>
</strong>
						<label>
					</div>			
					<div  id="<?php echo $_smarty_tpl->tpl_vars['delivery']->value->id;?>
">
						<?php echo $_smarty_tpl->tpl_vars['payment_method']->value->description;?>

					</div>
				</div>
				<?php } ?>
			</div>
			<input type='submit' class="btn btn-default" value='Закончить заказ'>
		</form>
		
		<?php } elseif ($_smarty_tpl->tpl_vars['payment_method']->value) {?>
		<p>Способ оплаты &mdash; <?php echo $_smarty_tpl->tpl_vars['payment_method']->value->name;?>

			<form method=post>
				<input type=submit name='reset_payment_method'  class="btn-link" value='Выбрать другой способ оплаты'>
			</form>	
		</p>
		<p>
			<?php echo $_smarty_tpl->tpl_vars['payment_method']->value->description;?>

		</p>
		<h2>
			К оплате <?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['convert'][0][0]->convert($_smarty_tpl->tpl_vars['order']->value->total_price,$_smarty_tpl->tpl_vars['payment_method']->value->currency_id);?>
&nbsp;<?php echo $_smarty_tpl->tpl_vars['all_currencies']->value[$_smarty_tpl->tpl_vars['payment_method']->value->currency_id]->sign;?>

		</h2>
		
		<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['checkout_form'][0][0]->checkout_form(array('order_id'=>$_smarty_tpl->tpl_vars['order']->value->id,'module'=>$_smarty_tpl->tpl_vars['payment_method']->value->module),$_smarty_tpl);?>

		<?php }?>
		<?php } else { ?>
		<h3>Заказ оплачен, ждите доставку!</h3>
		<?php }?>
	</div>
</div><?php }} ?>
