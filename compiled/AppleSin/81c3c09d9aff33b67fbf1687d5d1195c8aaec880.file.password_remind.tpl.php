<?php /* Smarty version Smarty-3.1.18, created on 2018-02-06 23:01:20
         compiled from "/var/www/vhosts/v-2612.webspace/www/applesin.com.kz/design/AppleSin/html/password_remind.tpl" */ ?>
<?php /*%%SmartyHeaderCode:653251175a79df60dfc593-72601007%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '81c3c09d9aff33b67fbf1687d5d1195c8aaec880' => 
    array (
      0 => '/var/www/vhosts/v-2612.webspace/www/applesin.com.kz/design/AppleSin/html/password_remind.tpl',
      1 => 1456605841,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '653251175a79df60dfc593-72601007',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'email_sent' => 0,
    'email' => 0,
    'error' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.18',
  'unifunc' => 'content_5a79df61082be2_08278938',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5a79df61082be2_08278938')) {function content_5a79df61082be2_08278938($_smarty_tpl) {?>

<?php $_smarty_tpl->tpl_vars['canonical'] = new Smarty_variable("/user/password_remind", null, 1);
if ($_smarty_tpl->parent != null) $_smarty_tpl->parent->tpl_vars['canonical'] = clone $_smarty_tpl->tpl_vars['canonical'];?>
<?php if ($_smarty_tpl->tpl_vars['email_sent']->value) {?>
<!-- CONTAINER -->
<div class="container inforow">
	<div class="col-sm-12">
		<h1>Вам отправлено письмо</h1>
		<p>На <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['email']->value, ENT_QUOTES, 'UTF-8', true);?>
 отправлено письмо для восстановления пароля. </p>
	</div>
</div>
<!-- /.container -->
<?php } else { ?>
<!-- HEADCONTENT -->
<div class="headcontent">
	<div class="container">
		<div class="col-sm-12">
			<h1>Напоминание пароля</h1>
		</div>
	</div>
</div>
<!-- /.headcontent -->  
<!-- CONTAINER -->
<div class="container inforow">
	<div class="col-md-6 col-sm-8">
		<?php if ($_smarty_tpl->tpl_vars['error']->value) {?>
		<div class="message_error">
			<?php if ($_smarty_tpl->tpl_vars['error']->value=='user_not_found') {?><div class="alert alert-warning fade in">Пользователь не найден</div>
			<?php } else { ?><?php echo $_smarty_tpl->tpl_vars['error']->value;?>
<?php }?>
		</div>
		<?php }?>
		<form class="login-form" method="post">
			<div class="form-group">
				<label for="email">Введите email, который вы указывали при регистрации</label>
				<input type="text" name="email" data-format="email" data-notice="Введите email" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['email']->value, ENT_QUOTES, 'UTF-8', true);?>
"  maxlength="255"/>
			</div>
			<input type="submit" class="btn btn-primary" value="Вспомнить" />
		</form>
		<?php }?>
	</div>
</div>
<!-- /.container --><?php }} ?>
