<?php /* Smarty version Smarty-3.1.18, created on 2018-08-18 18:46:02
         compiled from "C:\OSPanel\domains\apple.my\design\AppleSin\html\post.tpl" */ ?>
<?php /*%%SmartyHeaderCode:303535b783f3a48ab69-43803609%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '1c2bff21ab29be8a75b66a5bd0319279eb41b9ea' => 
    array (
      0 => 'C:\\OSPanel\\domains\\apple.my\\design\\AppleSin\\html\\post.tpl',
      1 => 1460231172,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '303535b783f3a48ab69-43803609',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'post' => 0,
    'settings' => 0,
    'user' => 0,
    'group' => 0,
    'config' => 0,
    'prev_post' => 0,
    'next_post' => 0,
    'comments' => 0,
    'comment' => 0,
    'error' => 0,
    'comment_name' => 0,
    'comment_text' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.18',
  'unifunc' => 'content_5b783f3a507b89_83808602',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5b783f3a507b89_83808602')) {function content_5b783f3a507b89_83808602($_smarty_tpl) {?>

<?php $_smarty_tpl->tpl_vars['canonical'] = new Smarty_variable("/blog/".((string)$_smarty_tpl->tpl_vars['post']->value->url), null, 1);
if ($_smarty_tpl->parent != null) $_smarty_tpl->parent->tpl_vars['canonical'] = clone $_smarty_tpl->tpl_vars['canonical'];?>
<div class="visible-xs">
<div class="mobilehead inforow">
<a href="#"><img src="design/<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['settings']->value->theme, ENT_QUOTES, 'UTF-8', true);?>
/images/logo.png" title="Перейти на главную страницу" alt="AppleSin logo"/></a>
</div>
</div>
<!-- HEADCONTENT -->
<div class="headcontent">
	<div class="container">
		<div class="col-md-8">
			<!-- Заголовок /-->
			<h1><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['post']->value->name, ENT_QUOTES, 'UTF-8', true);?>
</h1>
				<ol class="breadcrumb">
				<li><a href="/">Главная</a></li>
				<li><a href="blog">Блог</a></li>
				<li><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['post']->value->name, ENT_QUOTES, 'UTF-8', true);?>
</li>
			</ol>
		</div>
		<div class="col-md-4 text-right">
				<!-- Вход пользователя -->
				<?php if ($_smarty_tpl->tpl_vars['user']->value) {?>
				<span id="username">
					<a href="user"><?php echo $_smarty_tpl->tpl_vars['user']->value->name;?>
</a><?php if ($_smarty_tpl->tpl_vars['group']->value->discount>0) {?>,
					ваша скидка &mdash; <?php echo $_smarty_tpl->tpl_vars['group']->value->discount;?>
%<?php }?>
				</span> / 
				<a id="logout" href="user/logout">выйти</a>
				<?php } else { ?>
				<a id="register" href="user/register">Регистрация</a> / 
				<a id="login" href="user/login">Вход</a>
				<?php }?>
				<!-- Вход пользователя (The End)-->
		</div>
	</div>
</div>
<!-- /.headcontent -->
<!-- Заголовок /-->
<div class="container">
	<div class="post format-standart">
		<div class="col-xs-12 entry">
			<header class="entry-header">
				<div class="meta">
					<small><i class="fa fa-calendar"></i><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['date'][0][0]->date_modifier($_smarty_tpl->tpl_vars['post']->value->date);?>
</small>
				</div>
			</header>
			<div class="entry-content">
				<?php if ($_smarty_tpl->tpl_vars['post']->value->image) {?><img src="<?php echo $_smarty_tpl->tpl_vars['config']->value->root_url;?>
/<?php echo $_smarty_tpl->tpl_vars['config']->value->blog_images_dir;?>
<?php echo $_smarty_tpl->tpl_vars['post']->value->image;?>
"  class="img-responsive" alt="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['post']->value->name, ENT_QUOTES, 'UTF-8', true);?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['post']->value->name, ENT_QUOTES, 'UTF-8', true);?>
" /><?php }?>
				<p><?php echo $_smarty_tpl->tpl_vars['post']->value->text;?>
</p>
			</div>
			</div>
		<!-- Соседние записи /-->
		<div class="col-xs-12 hidden">
			<nav class="nav-single clearfix">
				<?php if ($_smarty_tpl->tpl_vars['prev_post']->value) {?>
				<div class="nav-previous text-left">
					<a href="blog/<?php echo $_smarty_tpl->tpl_vars['prev_post']->value->url;?>
">
						<small>Предыдущая запись</small>
						<span><?php echo $_smarty_tpl->tpl_vars['prev_post']->value->name;?>
</span>
					</a>
				</div>
				<?php }?>
				<?php if ($_smarty_tpl->tpl_vars['next_post']->value) {?>
				<div class="nav-next text-right">
					<a href="blog/<?php echo $_smarty_tpl->tpl_vars['next_post']->value->url;?>
">
						<small>Следующая запись</small>
						<span><?php echo $_smarty_tpl->tpl_vars['next_post']->value->name;?>
</span>
					</a>
				</div>
			</nav>
			<?php }?>
		</div>
		<!-- /.post -->
	</div>
	<hr class="sm-margin"/>
	<!-- COMMENTS -->
	<div class="col-md-12 comments" id="comments">
		<h3>Комментарии</h3>
		<div class="comment clearfix">
			<?php if ($_smarty_tpl->tpl_vars['comments']->value) {?>
			<!-- Список с комментариями -->
			<?php  $_smarty_tpl->tpl_vars['comment'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['comment']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['comments']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['comment']->key => $_smarty_tpl->tpl_vars['comment']->value) {
$_smarty_tpl->tpl_vars['comment']->_loop = true;
?>
			<div class="comment clearfix">
				<div class="comment-line">
					<p>
						<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['comment']->value->name, ENT_QUOTES, 'UTF-8', true);?>
,
						<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['date'][0][0]->date_modifier($_smarty_tpl->tpl_vars['comment']->value->date);?>
, <?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['time'][0][0]->time_modifier($_smarty_tpl->tpl_vars['comment']->value->date);?>

						<?php if (!$_smarty_tpl->tpl_vars['comment']->value->approved) {?><span class="label label-default">ожидает модерации</span><?php }?>
						<a  name="comment_<?php echo $_smarty_tpl->tpl_vars['comment']->value->id;?>
" class="internal"></a>
					</p>
				</div>
				<p><?php echo nl2br(htmlspecialchars($_smarty_tpl->tpl_vars['comment']->value->text, ENT_QUOTES, 'UTF-8', true));?>
</p>
			</div>
			<?php } ?>
			<?php } else { ?>
			<p>Пока нет комментариев</p>
			<?php }?>
		</div>
	</div>
	<div class="col-md-8 comments sm-margin" id="comments">
		<!-- Add Comment -->
		<div class="add-comment" id="addcomment">
			<h3>Написать комментарий</h3>
			<?php if ($_smarty_tpl->tpl_vars['error']->value) {?>
			<div class="alert alert-warning">
				<?php if ($_smarty_tpl->tpl_vars['error']->value=='captcha') {?>
				Подтвердите что вы не робот
				<?php } elseif ($_smarty_tpl->tpl_vars['error']->value=='empty_name') {?>
				Введите имя
				<?php } elseif ($_smarty_tpl->tpl_vars['error']->value=='empty_comment') {?>
				Введите комментарий
				<?php }?>
			</div>
			<?php }?>
			<form class="comment_form" method="post">
				<div class="form-wrap">
					<div class="form-group">
						<input class="input_name" type="text" id="comment_name" name="name" value="<?php echo $_smarty_tpl->tpl_vars['comment_name']->value;?>
" data-format=".+" data-notice="Введите имя" placeholder="Ваше имя"/>
					</div>
					<div class="form-group">
						<textarea class="comment_textarea" id="comment_text" name="text" data-format=".+" data-notice="Введите комментарий" placeholder="Комментарий"><?php echo $_smarty_tpl->tpl_vars['comment_text']->value;?>
</textarea>
					</div>
				</div>
				<div class="g-recaptcha" data-sitekey="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['settings']->value->site_code, ENT_QUOTES, 'UTF-8', true);?>
" data-size="compact"></div>
				<input class="btn btn-light" type="submit" name="comment" value="Отправить" />
			</form>
		</div>
		<!-- /.add-comment -->
	</div>
</div>
<script src="/js/baloon/js/default.js" language="JavaScript" type="text/javascript"></script>
<script src="/js/baloon/js/validate.js" language="JavaScript" type="text/javascript"></script>
<script src="/js/baloon/js/baloon.js" language="JavaScript" type="text/javascript"></script>
<link href="/js/baloon/css/baloon.css" rel="stylesheet" type="text/css" /><?php }} ?>
