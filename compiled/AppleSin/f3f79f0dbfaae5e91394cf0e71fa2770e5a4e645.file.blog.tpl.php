<?php /* Smarty version Smarty-3.1.18, created on 2017-10-18 01:38:23
         compiled from "/var/www/vhosts/v-2612.webspace/www/applesin.com.kz/design/AppleSin/html/blog.tpl" */ ?>
<?php /*%%SmartyHeaderCode:94678794059e65c2f86db26-33306730%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'f3f79f0dbfaae5e91394cf0e71fa2770e5a4e645' => 
    array (
      0 => '/var/www/vhosts/v-2612.webspace/www/applesin.com.kz/design/AppleSin/html/blog.tpl',
      1 => 1473529280,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '94678794059e65c2f86db26-33306730',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'settings' => 0,
    'page' => 0,
    'user' => 0,
    'group' => 0,
    'posts' => 0,
    'post' => 0,
    'config' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.18',
  'unifunc' => 'content_59e65c2fb0fc12_65561932',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_59e65c2fb0fc12_65561932')) {function content_59e65c2fb0fc12_65561932($_smarty_tpl) {?>

<?php $_smarty_tpl->tpl_vars['canonical'] = new Smarty_variable("/blog", null, 1);
if ($_smarty_tpl->parent != null) $_smarty_tpl->parent->tpl_vars['canonical'] = clone $_smarty_tpl->tpl_vars['canonical'];?>
<div class="visible-xs">
<div class="mobilehead inforow">
<a href="#"><img src="design/<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['settings']->value->theme, ENT_QUOTES, 'UTF-8', true);?>
/images/logo.png" title="Перейти на главную страницу" alt="AppleSin logo"/></a>
</div>
</div>
<div class="headcontent">
	<div class="container">
		<div class="col-md-8">
			<h1><?php echo $_smarty_tpl->tpl_vars['page']->value->name;?>
<?php echo $_smarty_tpl->tpl_vars['page']->value->body;?>
</h1>
			<ol class="breadcrumb">
				<li><a href="/">Главная</a></li>
				<li>Блог</li>
			</ol>
		</div>
		<div class="col-md-4 text-right">
				<!-- Вход пользователя -->
				<?php if ($_smarty_tpl->tpl_vars['user']->value) {?>
				<span id="username">
					<a href="user"><?php echo $_smarty_tpl->tpl_vars['user']->value->name;?>
</a><?php if ($_smarty_tpl->tpl_vars['group']->value->discount>0) {?>,
					ваша скидка &mdash; <?php echo $_smarty_tpl->tpl_vars['group']->value->discount;?>
%<?php }?>
				</span> / 
				<a id="logout" href="user/logout">выйти</a>
				<?php } else { ?>
				<a id="register" href="user/register">Регистрация</a> / 
				<a id="login" href="user/login">Вход</a>
				<?php }?>
				<!-- Вход пользователя (The End)-->
		</div>
	</div>
</div>
<?php echo $_smarty_tpl->getSubTemplate ('pagination.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

<!-- POST: Standart -->
<?php  $_smarty_tpl->tpl_vars['post'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['post']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['posts']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['post']->key => $_smarty_tpl->tpl_vars['post']->value) {
$_smarty_tpl->tpl_vars['post']->_loop = true;
?>
<?php if ($_smarty_tpl->tpl_vars['post']->value->category==2) {?>
<div class="container post format-standart">
	<div class="col-xs-12 entry">
		<header class="entry-header">
			<h2><a data-post="<?php echo $_smarty_tpl->tpl_vars['post']->value->id;?>
" href="blog/<?php echo $_smarty_tpl->tpl_vars['post']->value->url;?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['post']->value->name, ENT_QUOTES, 'UTF-8', true);?>
</a></h2>
			<div class="meta">
				<small><i class="fa fa-calendar"></i><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['date'][0][0]->date_modifier($_smarty_tpl->tpl_vars['post']->value->date);?>
</small>
			</div>
		</header>
		<div class="entry-content">
			<?php if ($_smarty_tpl->tpl_vars['post']->value->image) {?><img src="<?php echo $_smarty_tpl->tpl_vars['config']->value->root_url;?>
/<?php echo $_smarty_tpl->tpl_vars['config']->value->blog_images_dir;?>
<?php echo $_smarty_tpl->tpl_vars['post']->value->image;?>
" class="img-responsive" alt="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['post']->value->name, ENT_QUOTES, 'UTF-8', true);?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['post']->value->name, ENT_QUOTES, 'UTF-8', true);?>
"/><?php }?>
			<p><?php echo $_smarty_tpl->tpl_vars['post']->value->annotation;?>
</p>
			<a href="blog/<?php echo $_smarty_tpl->tpl_vars['post']->value->url;?>
" class="btn btn-primary">Читать далее</a>
		</div>
	</div>
	<!-- /.post -->
</div>
<?php }?>
<?php } ?>
<?php echo $_smarty_tpl->getSubTemplate ('pagination.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


<?php }} ?>
