<?php /* Smarty version Smarty-3.1.18, created on 2016-05-16 12:42:12
         compiled from "/var/www/vhosts/v-2612.webspace/www/applesin.com.kz/design/AppleSin/html/register.tpl" */ ?>
<?php /*%%SmartyHeaderCode:36553589056e5c6388830d0-23181212%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '812b5ee462cb91a605441e0823ce2a81d18cbc67' => 
    array (
      0 => '/var/www/vhosts/v-2612.webspace/www/applesin.com.kz/design/AppleSin/html/register.tpl',
      1 => 1463380931,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '36553589056e5c6388830d0-23181212',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.18',
  'unifunc' => 'content_56e5c638ad0683_37315790',
  'variables' => 
  array (
    'settings' => 0,
    'error' => 0,
    'name' => 0,
    'email' => 0,
    'phone' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_56e5c638ad0683_37315790')) {function content_56e5c638ad0683_37315790($_smarty_tpl) {?>

<?php $_smarty_tpl->tpl_vars['canonical'] = new Smarty_variable("/user/register", null, 1);
if ($_smarty_tpl->parent != null) $_smarty_tpl->parent->tpl_vars['canonical'] = clone $_smarty_tpl->tpl_vars['canonical'];?>
<?php $_smarty_tpl->tpl_vars['meta_title'] = new Smarty_variable("Регистрация", null, 1);
if ($_smarty_tpl->parent != null) $_smarty_tpl->parent->tpl_vars['meta_title'] = clone $_smarty_tpl->tpl_vars['meta_title'];?>
<div class="visible-xs">
<div class="mobilehead inforow">
<a href="#"><img src="design/<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['settings']->value->theme, ENT_QUOTES, 'UTF-8', true);?>
/images/logo.png" title="Перейти на главную страницу" alt="AppleSin logo"/></a>
</div>
</div>
<!-- HEADCONTENT -->
<div class="headcontent">
	<div class="container">
		<div class="col-md-8">
			<h1>Регистрация</h1>
		</div>
		<div class="col-md-4">
			<!-- Ulogin -->
			<script src="http://ulogin.ru/js/ulogin.js"></script>
			<div id="uLogin" data-ulogin="display=panel;fields=first_name,last_name,email,city;providers=vkontakte,odnoklassniki,mailru,instagram;hidden=other;redirect_uri=http://applesin.com.kz/user/login"></div>
		</div>
	</div>
</div>
<!-- /.headcontent -->  
<!-- CONTAINER -->
<div class="container inforow">
	<div class="col-md-6 col-sm-8">
		<?php if ($_smarty_tpl->tpl_vars['error']->value) {?>
		<div class="alert alert-warning">
			<?php if ($_smarty_tpl->tpl_vars['error']->value=='empty_name') {?>Введите имя
			<?php } elseif ($_smarty_tpl->tpl_vars['error']->value=='empty_email') {?>Введите email
			<?php } elseif ($_smarty_tpl->tpl_vars['error']->value=='empty_password') {?>Введите пароль
			<?php } elseif ($_smarty_tpl->tpl_vars['error']->value=='user_exists') {?>Пользователь с таким email уже зарегистрирован
			<?php } elseif ($_smarty_tpl->tpl_vars['error']->value=='captcha') {?>Подтвердите что вы не робот
			<?php } else { ?><?php echo $_smarty_tpl->tpl_vars['error']->value;?>
<?php }?>
		</div>
		<?php }?>
		<p class="text-muted">Уже зарегистрированны? <a href="user/login">Войдите</a></p>
		<form class="login-form"  method="post">
			<div class="form-group">
				<label for="name">Имя</label>
				<input type="text" name="name" data-format=".+" data-notice="Введите имя" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['name']->value, ENT_QUOTES, 'UTF-8', true);?>
" maxlength="255" />
			</div>
			<div class="form-group">
				<label for="email">Email</label>
				<input type="text" name="email" data-format="email" data-notice="Введите email" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['email']->value, ENT_QUOTES, 'UTF-8', true);?>
" maxlength="255" />
			</div>		
			<div class="form-group">
				<label for="phone">Телефон</label>
				<input type="text" name="phone" data-format=".+" data-notice="Введите телефон" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['phone']->value, ENT_QUOTES, 'UTF-8', true);?>
" maxlength="20" />
			</div>
			<div class="form-group">
				<label for="password">Пароль</label>
				<input type="password" name="password" data-format=".+" data-notice="Введите пароль" value="" />
			</div>
			<div class="g-recaptcha" data-sitekey="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['settings']->value->site_code, ENT_QUOTES, 'UTF-8', true);?>
"  data-size="compact"></div>
			<div class="form-group">
				<input type="submit" class="btn btn-primary" name="register" value="Зарегистрироваться">
			</div>
		</form>
	</div>
</div><?php }} ?>
