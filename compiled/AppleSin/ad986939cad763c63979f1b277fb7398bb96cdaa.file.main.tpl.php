<?php /* Smarty version Smarty-3.1.18, created on 2017-06-07 11:14:27
         compiled from "/var/www/vhosts/v-2612.webspace/www/applesin.com.kz/design/AppleSin/html/main.tpl" */ ?>
<?php /*%%SmartyHeaderCode:32716883756e46af59a9d31-85705419%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'ad986939cad763c63979f1b277fb7398bb96cdaa' => 
    array (
      0 => '/var/www/vhosts/v-2612.webspace/www/applesin.com.kz/design/AppleSin/html/main.tpl',
      1 => 1496812460,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '32716883756e46af59a9d31-85705419',
  'function' => 
  array (
    'categories_treeinproductpage' => 
    array (
      'parameter' => 
      array (
      ),
      'compiled' => '',
    ),
    'categories_treeinproductpage3' => 
    array (
      'parameter' => 
      array (
      ),
      'compiled' => '',
    ),
  ),
  'version' => 'Smarty-3.1.18',
  'unifunc' => 'content_56e46af5af74b6_73801073',
  'variables' => 
  array (
    'settings' => 0,
    'last_posts' => 0,
    'post' => 0,
    'config' => 0,
    'categories' => 0,
    'c1' => 0,
    'c2' => 0,
    'featured_products' => 0,
    'product' => 0,
    'v' => 0,
    'currency' => 0,
    'discounted_products' => 0,
  ),
  'has_nocache_code' => 0,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_56e46af5af74b6_73801073')) {function content_56e46af5af74b6_73801073($_smarty_tpl) {?>


<?php $_smarty_tpl->tpl_vars['wrapper'] = new Smarty_variable('index.tpl', null, 1);
if ($_smarty_tpl->parent != null) $_smarty_tpl->parent->tpl_vars['wrapper'] = clone $_smarty_tpl->tpl_vars['wrapper'];?>

<?php $_smarty_tpl->tpl_vars['canonical'] = new Smarty_variable('', null, 1);
if ($_smarty_tpl->parent != null) $_smarty_tpl->parent->tpl_vars['canonical'] = clone $_smarty_tpl->tpl_vars['canonical'];?>

<div class="visible-xs">
	<div class="mobilehead inforow">
		<a href="#"><img src="design/<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['settings']->value->theme, ENT_QUOTES, 'UTF-8', true);?>
/images/logo.png" title="Перейти на главную страницу" alt="AppleSin logo"/></a>
	</div>
</div>

<!-- CONTAINER -->
<div class="container">
	<div class="row">
		<div class="col-sm-8 col-sm-offset-2 text-center">
			<h2>Новости и акции</h2>
		</div>
	</div>
	<div class="row">
		
		<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['get_posts'][0][0]->get_posts_plugin(array('var'=>'last_posts'),$_smarty_tpl);?>

		<?php if ($_smarty_tpl->tpl_vars['last_posts']->value) {?>
		<?php  $_smarty_tpl->tpl_vars['post'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['post']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['last_posts']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['post']->key => $_smarty_tpl->tpl_vars['post']->value) {
$_smarty_tpl->tpl_vars['post']->_loop = true;
?>
		<?php if ($_smarty_tpl->tpl_vars['post']->value->category==1) {?>
		<div class="col-md-6 col-xs-12 post format-image">
			<h3>
				<a href="blog/<?php echo $_smarty_tpl->tpl_vars['post']->value->url;?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['post']->value->name, ENT_QUOTES, 'UTF-8', true);?>
</a>
			</h3>
			<?php if ($_smarty_tpl->tpl_vars['post']->value->image) {?><a href="blog/<?php echo $_smarty_tpl->tpl_vars['post']->value->url;?>
"><img src="<?php echo $_smarty_tpl->tpl_vars['config']->value->root_url;?>
/<?php echo $_smarty_tpl->tpl_vars['config']->value->blog_images_dir;?>
<?php echo $_smarty_tpl->tpl_vars['post']->value->image;?>
" alt="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['post']->value->name, ENT_QUOTES, 'UTF-8', true);?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['post']->value->name, ENT_QUOTES, 'UTF-8', true);?>
"/></a><?php }?>
			<p><?php echo $_smarty_tpl->tpl_vars['post']->value->annotation;?>
</p>
		</div>
		<?php }?>
		<?php } ?>
		<?php }?>
	</div>
</div>

<!-- /.container -->

<!-- CONTAINER subcat-->

<div class="container">
	<header class="page-header text-center">
		<h3>Аксессуары по категориям</h3>
	</header>
	
			<div class="row">
				
				<?php if (!function_exists('smarty_template_function_categories_treeinproductpage')) {
    function smarty_template_function_categories_treeinproductpage($_smarty_tpl,$params) {
    $saved_tpl_vars = $_smarty_tpl->tpl_vars;
    foreach ($_smarty_tpl->smarty->template_functions['categories_treeinproductpage']['parameter'] as $key => $value) {$_smarty_tpl->tpl_vars[$key] = new Smarty_variable($value);};
    foreach ($params as $key => $value) {$_smarty_tpl->tpl_vars[$key] = new Smarty_variable($value);}?>
				<?php if ($_smarty_tpl->tpl_vars['categories']->value) {?>
				<?php  $_smarty_tpl->tpl_vars['c1'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['c1']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['categories']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['c1']->key => $_smarty_tpl->tpl_vars['c1']->value) {
$_smarty_tpl->tpl_vars['c1']->_loop = true;
?>
				<?php if ($_smarty_tpl->tpl_vars['c1']->value->visible) {?>
				<?php  $_smarty_tpl->tpl_vars['c2'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['c2']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['c1']->value->subcategories; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['c2']->key => $_smarty_tpl->tpl_vars['c2']->value) {
$_smarty_tpl->tpl_vars['c2']->_loop = true;
?>
				<?php if ($_smarty_tpl->tpl_vars['c2']->value->visible) {?>
				<div class="col-sm-6 col-md-4 subcat" onclick="location.href = 'catalog/<?php echo $_smarty_tpl->tpl_vars['c2']->value->url;?>
';">
					<?php if ($_smarty_tpl->tpl_vars['c2']->value->image) {?><img src="<?php echo $_smarty_tpl->tpl_vars['config']->value->categories_images_dir;?>
<?php echo $_smarty_tpl->tpl_vars['c2']->value->image;?>
" alt="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['c2']->value->name, ENT_QUOTES, 'UTF-8', true);?>
" ><?php }?>
					<a href="catalog/<?php echo $_smarty_tpl->tpl_vars['c2']->value->url;?>
" data-category="<?php echo $_smarty_tpl->tpl_vars['c2']->value->id;?>
"><?php echo $_smarty_tpl->tpl_vars['c2']->value->name;?>
</a>
				</div>
				<?php }?>
				<?php } ?>
				<?php }?>
				<?php } ?>                  
				<?php }?>
				<?php $_smarty_tpl->tpl_vars = $saved_tpl_vars;
foreach (Smarty::$global_tpl_vars as $key => $value) if(!isset($_smarty_tpl->tpl_vars[$key])) $_smarty_tpl->tpl_vars[$key] = $value;}}?>

				<?php smarty_template_function_categories_treeinproductpage($_smarty_tpl,array('categories'=>$_smarty_tpl->tpl_vars['categories']->value));?>
                  
			</div>

</div>

<!-- /.container -->

<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['get_featured_products'][0][0]->get_featured_products_plugin(array('var'=>'featured_products'),$_smarty_tpl);?>

<?php if ($_smarty_tpl->tpl_vars['featured_products']->value) {?>
<!-- SLIDER -->
<div class="container slider product-slider text-center">
	<header class="page-header">
	<h3>Мы рекомендуем посмотреть</h3>
	</header>
	<div class="col-md-12">
		<ul data-auto="true" data-fx="slide" data-interval="3000">
			<?php  $_smarty_tpl->tpl_vars['product'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['product']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['featured_products']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['product']->key => $_smarty_tpl->tpl_vars['product']->value) {
$_smarty_tpl->tpl_vars['product']->_loop = true;
?>
			<li class="product">
				<div class="product-img">
					<?php if ($_smarty_tpl->tpl_vars['product']->value->image) {?>
					<a href="products/<?php echo $_smarty_tpl->tpl_vars['product']->value->url;?>
">
						<img width="200" src="<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['resize'][0][0]->resize_modifier($_smarty_tpl->tpl_vars['product']->value->image->filename,200,200);?>
" alt="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value->name, ENT_QUOTES, 'UTF-8', true);?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value->name, ENT_QUOTES, 'UTF-8', true);?>
"/>
					</a>
					<?php } else { ?>
					<img width="200" src="design/<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['settings']->value->theme, ENT_QUOTES, 'UTF-8', true);?>
/images/nophoto.png" alt="Нет фото">
					<?php }?>
				</div>
				<h6><a data-product="<?php echo $_smarty_tpl->tpl_vars['product']->value->id;?>
" href="products/<?php echo $_smarty_tpl->tpl_vars['product']->value->url;?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value->name, ENT_QUOTES, 'UTF-8', true);?>
</a></h6>
				<span class="price">
					<?php if (count($_smarty_tpl->tpl_vars['product']->value->variants)>0) {?>
					<?php  $_smarty_tpl->tpl_vars['v'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['v']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['product']->value->variants; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['v']->key => $_smarty_tpl->tpl_vars['v']->value) {
$_smarty_tpl->tpl_vars['v']->_loop = true;
?>
					<?php if ($_smarty_tpl->tpl_vars['v']->value->compare_price>0) {?><span class="compare_price"><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['convert'][0][0]->convert($_smarty_tpl->tpl_vars['v']->value->compare_price);?>
</span><?php }?>
					<span class="amount"><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['convert'][0][0]->convert($_smarty_tpl->tpl_vars['v']->value->price);?>
 <span class="currency"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['currency']->value->sign, ENT_QUOTES, 'UTF-8', true);?>
</span></span>
					<?php } ?>
					<?php } else { ?>
					Нет в наличии
					<?php }?>
				</span>
			</li>
			<?php } ?>
		</ul>
	</div>
	<nav class="nav-pages"></nav>
</div>
<!-- /.slider -->
<?php }?>

<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['get_discounted_products'][0][0]->get_discounted_products_plugin(array('var'=>'discounted_products','limit'=>9),$_smarty_tpl);?>

<?php if ($_smarty_tpl->tpl_vars['discounted_products']->value) {?>
<!-- SLIDER -->
<div class="container slider product-slider text-center">
	<header class="page-header">
		<h3>Товары со скидкой:</h3>
	</header>
	<div class="col-md-12">
		<ul  data-auto="true" data-fx="slide" data-interval="3000">
			<?php  $_smarty_tpl->tpl_vars['product'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['product']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['discounted_products']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['product']->key => $_smarty_tpl->tpl_vars['product']->value) {
$_smarty_tpl->tpl_vars['product']->_loop = true;
?>
			<li class="product">
				<div class="product-img">
					<?php if ($_smarty_tpl->tpl_vars['product']->value->image) {?>
					<a href="products/<?php echo $_smarty_tpl->tpl_vars['product']->value->url;?>
">
						<img width="200" src="<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['resize'][0][0]->resize_modifier($_smarty_tpl->tpl_vars['product']->value->image->filename,200,200);?>
" alt="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value->name, ENT_QUOTES, 'UTF-8', true);?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value->name, ENT_QUOTES, 'UTF-8', true);?>
"/>
					</a>
					<?php } else { ?>
					<img width="200" src="design/<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['settings']->value->theme, ENT_QUOTES, 'UTF-8', true);?>
/images/nophoto.png" alt="Нет фото">
					<?php }?>
				</div>
				<h6><a data-product="<?php echo $_smarty_tpl->tpl_vars['product']->value->id;?>
" href="products/<?php echo $_smarty_tpl->tpl_vars['product']->value->url;?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value->name, ENT_QUOTES, 'UTF-8', true);?>
</a></h6>
				<span class="price">
					<?php if (count($_smarty_tpl->tpl_vars['product']->value->variants)>0) {?>
					<?php  $_smarty_tpl->tpl_vars['v'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['v']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['product']->value->variants; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['v']->key => $_smarty_tpl->tpl_vars['v']->value) {
$_smarty_tpl->tpl_vars['v']->_loop = true;
?>
					<?php if ($_smarty_tpl->tpl_vars['v']->value->compare_price>0) {?><span class="compare_price"><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['convert'][0][0]->convert($_smarty_tpl->tpl_vars['v']->value->compare_price);?>
</span><?php }?>
					<span class="amount"><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['convert'][0][0]->convert($_smarty_tpl->tpl_vars['v']->value->price);?>
 <span class="currency"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['currency']->value->sign, ENT_QUOTES, 'UTF-8', true);?>
</span></span>
					<?php } ?>
					<?php } else { ?>
					Нет в наличии
					<?php }?>
				</span>
			</li>
			<?php } ?>
		</ul>
	</div>
	<nav class="nav-pages"></nav>
</div>
<!-- /.slider -->
<?php }?>
<!-- CONTAINER -->
<div class="container inforow">
	<div class="col-sm-4">
		<h2 class="painted">Отзывы и предложения</h2>
	</div>
	<div class="col-sm-8">
		<p>Мы всегда рады общению с нашими клиентами. Если хотите поблагодарить нас или, наоборот, обратить внимание на недостатки, напишите нам.  Так же вы можете написать нам предложение, замечание по работе, отзыв или любой другой вопрос. </p>
		<a class="btn btn-primary" href="contact">Написать нам</a>
	</div>
</div>
<!-- /.container -->
<div class="bg-primary hidden-xs">
	<!-- SLIDER -->
	<div class="container slider oneslider inforow">
		<ul>
			<li>
				<div class="col-sm-3 col-sm-offset-1">
					<img src="design/<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['settings']->value->theme, ENT_QUOTES, 'UTF-8', true);?>
/images/clients/nophoto.png" class="img-circle" alt="Нет фото" title="Нет фото">
				</div>
				<div class="col-sm-7 middle hidden-middle">
					<blockquote>
						<h3>“Отличное сервисное обслуживание молодцы, перед тем как пойти к вам ходил в другой сервис по ремонту телефона сказали что мой телефон не подлежит ремонту типа плата не годна, знакомые подсказали сходить AppleSin и урааа оказалось просто экран накрылся!!! Спасибо вам!!! ”</h3>
					</blockquote>
					<h5>Талгат<small>наш постоянный клиент</small></h5>
				</div>
			</li>
			<li>
				<div class="col-sm-3 col-sm-offset-1">
					<img src="design/<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['settings']->value->theme, ENT_QUOTES, 'UTF-8', true);?>
/images/clients/2.png" class="img-circle" alt="Садовский Алексей" title="Садовский Алексей">
				</div>
				<div class="col-sm-7 middle hidden-middle">
					<blockquote>
						<h3>“Ребята молодцы, выбор аксессуаров просто огромный! С продавцами приятно и удобно работать. Так держать, AppleSin!”</h3>
					</blockquote>
					<h5>Садовский Алексей<small>26 лет, дизайн-студия "Полиграф Полиграфыч", г. Петропавловск</small></h5>
				</div>
			</li>
			<li>
				<div class="col-sm-3 col-sm-offset-1">
					<img src="design/<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['settings']->value->theme, ENT_QUOTES, 'UTF-8', true);?>
/images/clients/1.png" class="img-circle" alt="Зелянин Константин" title="Зелянин Константин">
				</div>
				<div class="col-sm-7 middle hidden-middle">
					<blockquote>
						<h3>“Давно хотел себе хороший смартфон. Был проездом в Петропавловске, посоветовали заглянуть в AppleSin. Продавец, если не ошибаюсь, Евгений, очень грамотный парень, разрекламировал так, что у меня не оставалось выбора кроме как купить iPhone 6s. Спасибо, AppleSin!”</h3>
					</blockquote>
					<h5>Зелянин Константин<small>24 года, владелец Apple iPhone 6s, г. Омск</small></h5>
				</div>
			</li>
		</ul>
		<div class="arrow prev"><i></i></div>
		<div class="arrow next"><i></i></div>
		<nav class="nav-pages visible-xs"></nav>
	</div>
	<!-- /.slider -->
</div>
<!-- NEWSLETTER -->
<div id="subscribe" class="newsletter bg-sl-fscreen-6 bg-sl-center overlay overlay-light">
	<div class="container inforow middle">
		<div class="col-sm-7 col-sm-offset-1">
			<p>Подпишитесь на новости и акции</p>
		</div>
		<div class="col-sm-2">
			<div id="feedback-form">			
				<a href="http://eepurl.com/cb87YD" target="_blank" class="btn btn-default btn-lg">Подписаться</a>			
			</div>
		</div>
	</div>
</div>
<!-- /.newsletter -->	<?php }} ?>
