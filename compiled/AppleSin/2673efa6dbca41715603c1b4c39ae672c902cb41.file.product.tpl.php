<?php /* Smarty version Smarty-3.1.18, created on 2018-08-18 14:28:22
         compiled from "C:\OSPanel\domains\apple.my\design\AppleSin\html\product.tpl" */ ?>
<?php /*%%SmartyHeaderCode:21266269145b7802d6ec6ef9-40361035%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '2673efa6dbca41715603c1b4c39ae672c902cb41' => 
    array (
      0 => 'C:\\OSPanel\\domains\\apple.my\\design\\AppleSin\\html\\product.tpl',
      1 => 1508483960,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '21266269145b7802d6ec6ef9-40361035',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'product' => 0,
    'settings' => 0,
    'keyword' => 0,
    'page' => 0,
    'category' => 0,
    'brand' => 0,
    'cat' => 0,
    'user' => 0,
    'group' => 0,
    'currencies' => 0,
    'c' => 0,
    'currency' => 0,
    'v' => 0,
    'f' => 0,
    'related_products' => 0,
    'related_product' => 0,
    'comments' => 0,
    'comment' => 0,
    'error' => 0,
    'comment_name' => 0,
    'comment_text' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.18',
  'unifunc' => 'content_5b7802d7020f12_54085168',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5b7802d7020f12_54085168')) {function content_5b7802d7020f12_54085168($_smarty_tpl) {?>

<?php $_smarty_tpl->tpl_vars['canonical'] = new Smarty_variable("/products/".((string)$_smarty_tpl->tpl_vars['product']->value->url), null, 1);
if ($_smarty_tpl->parent != null) $_smarty_tpl->parent->tpl_vars['canonical'] = clone $_smarty_tpl->tpl_vars['canonical'];?>
<div class="visible-xs">
	<div class="mobilehead inforow">
		<a href="#"><img src="design/<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['settings']->value->theme, ENT_QUOTES, 'UTF-8', true);?>
/images/logo.png" title="Перейти на главную страницу" alt="AppleSin logo"/></a>
	</div>
</div>
<!-- HEADCONTENT -->
<div class="headcontent">
	<div class="container">
		<div class="col-md-8">
			<h1>
				<?php if ($_smarty_tpl->tpl_vars['keyword']->value) {?>
				<h1>Поиск <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['keyword']->value, ENT_QUOTES, 'UTF-8', true);?>
</h1>
				<?php } elseif ($_smarty_tpl->tpl_vars['page']->value) {?>
				<h1><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['page']->value->name, ENT_QUOTES, 'UTF-8', true);?>
</h1>
				<?php } else { ?>
				<h1><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['category']->value->name, ENT_QUOTES, 'UTF-8', true);?>
 <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['brand']->value->name, ENT_QUOTES, 'UTF-8', true);?>
 <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['keyword']->value, ENT_QUOTES, 'UTF-8', true);?>
</h1>
				<?php }?>
				<ol class="breadcrumb">
					<a href="/">Главная</a>
					<?php if ($_smarty_tpl->tpl_vars['category']->value) {?>
					<?php  $_smarty_tpl->tpl_vars['cat'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['cat']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['category']->value->path; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['cat']->key => $_smarty_tpl->tpl_vars['cat']->value) {
$_smarty_tpl->tpl_vars['cat']->_loop = true;
?>
					/ <a href="catalog/<?php echo $_smarty_tpl->tpl_vars['cat']->value->url;?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['cat']->value->name, ENT_QUOTES, 'UTF-8', true);?>
</a>
					<?php } ?>
					<?php if ($_smarty_tpl->tpl_vars['brand']->value) {?>
					/ <a href="catalog/<?php echo $_smarty_tpl->tpl_vars['cat']->value->url;?>
/<?php echo $_smarty_tpl->tpl_vars['brand']->value->url;?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['brand']->value->name, ENT_QUOTES, 'UTF-8', true);?>
</a>
					<?php }?>
					<?php } elseif ($_smarty_tpl->tpl_vars['brand']->value) {?>
					/ <a href="brands/<?php echo $_smarty_tpl->tpl_vars['brand']->value->url;?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['brand']->value->name, ENT_QUOTES, 'UTF-8', true);?>
</a>
					<?php } elseif ($_smarty_tpl->tpl_vars['keyword']->value) {?>
					/ Поиск
					<?php }?>
					/<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value->name, ENT_QUOTES, 'UTF-8', true);?>

				</ol>
			</div>
			<div class="col-md-4 text-right">
				<!-- Вход пользователя -->
				<?php if ($_smarty_tpl->tpl_vars['user']->value) {?>
				<span id="username">
					<a href="user"><?php echo $_smarty_tpl->tpl_vars['user']->value->name;?>
</a><?php if ($_smarty_tpl->tpl_vars['group']->value->discount>0) {?>,
					ваша скидка &mdash; <?php echo $_smarty_tpl->tpl_vars['group']->value->discount;?>
%<?php }?>
				</span> / 
				<a id="logout" href="user/logout">выйти</a>
				<?php } else { ?>
				<a id="register" href="user/register">Регистрация</a> / 
				<a id="login" href="user/login">Вход</a>
				<?php }?>
				<!-- Вход пользователя (The End)-->
				<p class="pull-right">
					<div id="cart_informer"><?php echo $_smarty_tpl->getSubTemplate ('cart_informer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>
</div>
				</p>
				<!-- Выбор валюты -->
				
				<?php if (count($_smarty_tpl->tpl_vars['currencies']->value)>1) {?>
				<nav class="nav-currency pull-right">
					<?php  $_smarty_tpl->tpl_vars['c'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['c']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['currencies']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['c']->key => $_smarty_tpl->tpl_vars['c']->value) {
$_smarty_tpl->tpl_vars['c']->_loop = true;
?>
					<?php if ($_smarty_tpl->tpl_vars['c']->value->enabled) {?>
					<a href='<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['url'][0][0]->url_modifier(array('currency_id'=>$_smarty_tpl->tpl_vars['c']->value->id),$_smarty_tpl);?>
' class="<?php if ($_smarty_tpl->tpl_vars['c']->value->id==$_smarty_tpl->tpl_vars['currency']->value->id) {?>active<?php }?>"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['c']->value->name, ENT_QUOTES, 'UTF-8', true);?>
</a>
					<?php }?>
					<?php } ?>
				</nav>
				<?php }?>
				<!-- Выбор валюты (The End) -->
			</div>
		</div>
	</div>
	<!-- /.headcontent -->
	<!-- CONTAINER -->
	<div class="container type-product inforow" itemscope itemtype="http://schema.org/Product">
		<div class="col-sm-6 text-center magnific-wrap">
			<div class="img-medium slider oneslider">
				<?php if ($_smarty_tpl->tpl_vars['product']->value->image) {?>
				<a href="<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['resize'][0][0]->resize_modifier($_smarty_tpl->tpl_vars['product']->value->image->filename,800,600,'w');?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value->name, ENT_QUOTES, 'UTF-8', true);?>
" class="magnific"><img src="<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['resize'][0][0]->resize_modifier($_smarty_tpl->tpl_vars['product']->value->image->filename,540,600,'w');?>
" class="img-responsive" alt="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value->name, ENT_QUOTES, 'UTF-8', true);?>
"  title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value->name, ENT_QUOTES, 'UTF-8', true);?>
" itemprop="image"></a>
				<?php } else { ?>
				<img src="design/<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['settings']->value->theme, ENT_QUOTES, 'UTF-8', true);?>
/images/nophoto-lg.png" class="img-responsive" alt="Нет фото" title="Нет фото">
				<?php }?>
			</div>
		</div>
		<div class="col-sm-6">
			<h3  itemprop="name"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value->name, ENT_QUOTES, 'UTF-8', true);?>
 </h3>
			<span class="price"  itemprop="offers" itemscope itemtype="http://schema.org/Offer">
				<?php  $_smarty_tpl->tpl_vars['v'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['v']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['product']->value->variants; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['v']->key => $_smarty_tpl->tpl_vars['v']->value) {
$_smarty_tpl->tpl_vars['v']->_loop = true;
?>
				<?php if ($_smarty_tpl->tpl_vars['v']->value->compare_price>0) {?><span class="compare_price"><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['convert'][0][0]->convert($_smarty_tpl->tpl_vars['v']->value->compare_price);?>
</span><?php }?>
				<span class="amount"  itemprop="price"><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['convert'][0][0]->convert($_smarty_tpl->tpl_vars['v']->value->price);?>
 <span class="currency"  itemprop="priceCurrency"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['currency']->value->sign, ENT_QUOTES, 'UTF-8', true);?>
</span></span>
				<?php } ?>
			</span>
			<?php if ($_smarty_tpl->tpl_vars['product']->value->pre_order<1) {?>
			<?php if ($_smarty_tpl->tpl_vars['v']->value->actual_date) {?><small>Цена и остаток актуальны на <?php echo $_smarty_tpl->tpl_vars['v']->value->actual_date;?>
</small><?php }?>
				<?php }?>
				<?php if ($_smarty_tpl->tpl_vars['product']->value->body) {?>
				<!-- Описание товара -->
				<p id="readmore" itemprop="description">
					<?php echo $_smarty_tpl->tpl_vars['product']->value->body;?>

				</p>
				<?php if (count($_smarty_tpl->tpl_vars['product']->value->variants)>0) {?>
				<hr class="no-border"/>
				<?php }?>
				<?php }?>
				
				<?php if (count($_smarty_tpl->tpl_vars['product']->value->variants)>0) {?>
				<div class="product">
					<div class="description">
						<!-- Выбор варианта товара -->
						<form class="variants form-inline" action="/cart">
							<table class="hidden">
								<?php  $_smarty_tpl->tpl_vars['v'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['v']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['product']->value->variants; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['v']->key => $_smarty_tpl->tpl_vars['v']->value) {
$_smarty_tpl->tpl_vars['v']->_loop = true;
?>
								<tr class="variant">
									<td>
										<input id="product_<?php echo $_smarty_tpl->tpl_vars['v']->value->id;?>
" name="variant" value="<?php echo $_smarty_tpl->tpl_vars['v']->value->id;?>
" type="radio" class="variant_radiobutton" <?php if ($_smarty_tpl->tpl_vars['product']->value->variant->id==$_smarty_tpl->tpl_vars['v']->value->id) {?>checked<?php }?> <?php if (count($_smarty_tpl->tpl_vars['product']->value->variants)<2) {?>style="display:none;"<?php }?>/>
									</td>
									<td>
										<?php if ($_smarty_tpl->tpl_vars['v']->value->name) {?><label class="variant_name" for="product_<?php echo $_smarty_tpl->tpl_vars['v']->value->id;?>
"><?php echo $_smarty_tpl->tpl_vars['v']->value->name;?>
</label><?php }?>
									</td>
									<td>
										<?php if ($_smarty_tpl->tpl_vars['v']->value->compare_price>0) {?><span class="compare_price"><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['convert'][0][0]->convert($_smarty_tpl->tpl_vars['v']->value->compare_price);?>
</span><?php }?>
										<span class="price"><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['convert'][0][0]->convert($_smarty_tpl->tpl_vars['v']->value->price);?>
 <span class="currency"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['currency']->value->sign, ENT_QUOTES, 'UTF-8', true);?>
</span></span>
									</td>
								</tr>
								<?php } ?>
							</table>
							<div class="form-group"><input id="buyButton" type="submit" class="button btn btn-primary" value="в корзину" data-result-text="добавлено"/></div>
							<div class="form-group"><div class="ks-widget" data-template="button" data-merchant-sku="<?php echo $_smarty_tpl->tpl_vars['v']->value->sku;?>
" data-merchant-code="PROFIT" data-city="591010000"></div></div>
						</form>
						<!-- Выбор варианта товара (The End) -->
					</div>
					<!-- Описание товара (The End)-->
				</div>
				<?php } else { ?>
				<hr class="xs-margin no-border"/>
				<div class="alert alert-default" role="alert">Нет в наличии</div>
				<?php }?>
				
				<table class="table cart-total">
					<?php if ($_smarty_tpl->tpl_vars['v']->value->sku) {?>
					
					<tr>
						<th>Артикул:</th>
						<td><?php echo $_smarty_tpl->tpl_vars['v']->value->sku;?>
</td>
					</tr>
					<?php }?>
					<?php if ($_smarty_tpl->tpl_vars['product']->value->features) {?>
					<?php  $_smarty_tpl->tpl_vars['f'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['f']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['product']->value->features; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['f']->key => $_smarty_tpl->tpl_vars['f']->value) {
$_smarty_tpl->tpl_vars['f']->_loop = true;
?>
					<tr>
						<th><?php echo $_smarty_tpl->tpl_vars['f']->value->name;?>
</th>
						<td class="text-muted"><?php echo $_smarty_tpl->tpl_vars['f']->value->value;?>
</td>
					</tr>
					<?php } ?>		
					<?php }?>
				</table>
				<div class="visible-xs">
					<ul class="list-unstyled">
						<li><a href="/warranty">Гарантия и возврат </a></li>
						<li><a href="/shipping">Доставка </a></li>
						<li><a href="/how-to-pay">Оплата </a></li>
					</ul>
				</div>
				
				
				
				<?php if ($_smarty_tpl->tpl_vars['v']->value->stock>0) {?>
				<p  class="text-success"><strong><i class="fa fa-check"></i> Наличие в магазинах:</strong></p>
				<div class="panel-group" id="accordion-2">
					<?php if ($_smarty_tpl->tpl_vars['v']->value->m1>0) {?>
					<div class="panel panel-simple">
						<div class="panel-heading">
							<h4 class="panel-title">
							<a class="collapsed" data-toggle="collapse" data-parent="#accordion-2" href="#collapseFirst"><i class="fa fa-location-arrow fa-fw"></i>AppleSin на Букетова</a>
						</h4>
					</div>
					<div id="collapseFirst" class="panel-collapse collapse">
						<div class="panel-body">
							<dl class="dl-horizontal"><dt>Режим работы:</dt><dd><p><time>Ежедневно, с 10:00 до 20:00</time></p></dd><dt>Адрес:</dt><dd><address><span>Букетова, 57</span><br /> <span > 150000</span> <span>Петропавловск</span></address></dd><dt>Телефоны:</dt><dd><p><span>+7(707)037-09-65</span><br /> <span>+7(702)887-24-44</span></p></dd></dl>
						</div>
					</div>
				</div>
				<?php }?>
				<?php if ($_smarty_tpl->tpl_vars['v']->value->m4>0) {?>
				<div class="panel panel-simple">
					<div class="panel-heading">
						<h4 class="panel-title">
							<a class="collapsed" data-toggle="collapse" data-parent="#accordion-2" href="#collapseSecond"><i class="fa fa-location-arrow fa-fw"></i>AppleSin в Рахмете</a>
						</h4>
					</div>
					<div id="collapseSecond" class="panel-collapse collapse">
						<div class="panel-body">
							<dl class="dl-horizontal"><dt>Режим работы:</dt><dd><p><time>Ежедневно, с 10:00 до 21:00</time></p></dd><dt>Адрес:</dt><dd><div itemprop="location">ТЦ "Рахмет"</div><address><span>К.Сутюшева, 58б</span> <span>Петропавловск</span></address></dd><dt>Телефоны:</dt><dd><p><span>+7(707)515-18-88</span><br /> <span>+7(708)607-08-88</span></p></dd></dl>
						</div>
					</div>
				</div>
				<?php }?>
				<?php if ($_smarty_tpl->tpl_vars['v']->value->m3>0) {?>
				<div class="panel panel-simple">
					<div class="panel-heading">
						<h4 class="panel-title">
							<a class="collapsed" data-toggle="collapse" data-parent="#accordion-2" href="#collapseThird"><i class="fa fa-location-arrow fa-fw"></i>AppleSin в Пирамиде</a>
						</h4>
					</div>
					<div id="collapseThird" class="panel-collapse collapse">
						<div class="panel-body">
							<dl class="dl-horizontal"><dt>Режим работы:</dt><dd><p><time>Ежедневно, с 10:00 до 20:00</time></p></dd><dt>Адрес:</dt><dd><div itemprop="location">ТД "Пирамида"</div><address><span>Батыр Баяна, 11</span> <span>Петропавловск</span></address></dd><dt>Телефоны:</dt><dd><p><span>+7(776)976-88-88</span><br /> <span>+7(747)370-09-87</span></p></dd></dl>
						</div>
					</div>
				</div>
				<?php }?>
				<?php if ($_smarty_tpl->tpl_vars['v']->value->m2>0) {?>
				<div class="panel panel-simple">
					<div class="panel-heading">
						<h4 class="panel-title">
							<a class="collapsed" data-toggle="collapse" data-parent="#accordion-2" href="#collapseFour"><i class="fa fa-location-arrow fa-fw"></i>Мини AppleSin в Минимаркете</a>
						</h4>
					</div>
					<div id="collapseFour" class="panel-collapse collapse">
						<div class="panel-body">
							<dl class="dl-horizontal"><dt>Режим работы:</dt><dd><p><time>Ежедневно, с 10:00 до 20:00</time></p></dd><dt>Адрес:</dt><dd><div itemprop="location">маг. "Минимаркет"</div><address><span>ул. Абая, 41</span> <span>Петропавловск</span></address></dd><dt>Телефоны:</dt><dd><p><span>+7(747)151-84-44</span><br /> <span>+7(705)794-54-44</span></p></dd></dl>
						</div>
					</div>
				</div>
				<?php }?>
				<?php if ($_smarty_tpl->tpl_vars['v']->value->Alm>0) {?>
				<div class="panel panel-simple">
					<div class="panel-heading">
						<h4 class="panel-title">
							<a class="collapsed" data-toggle="collapse" data-parent="#accordion-2" href="#collapseFour"><i class="fa fa-location-arrow fa-fw"></i>Склад Алматы</a>
						</h4>
					</div>
					<div id="collapseFour" class="panel-collapse collapse">
						<div class="panel-body">
							<p>Бесплатная доставка в Петропавловск в течении 2-8 рабочих дней.</p>
						</div>
					</div>
				</div>
				<?php }?>

			</div>
			<?php }?>
		</div>
	</div>
	<!-- /.container -->
	<!-- SLIDER -->
	<div class="container slider product-slider text-center">
		<header class="page-header">
			<h3>Соседние товары</h3>
		</header>
		<ul  data-auto="true" data-fx="slide" data-interval="3000">
			<?php  $_smarty_tpl->tpl_vars['related_product'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['related_product']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['related_products']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['related_product']->key => $_smarty_tpl->tpl_vars['related_product']->value) {
$_smarty_tpl->tpl_vars['related_product']->_loop = true;
?>
			<li class="product">
				<div class="product-img">
					<?php if ($_smarty_tpl->tpl_vars['related_product']->value->image) {?>
					<a href="products/<?php echo $_smarty_tpl->tpl_vars['related_product']->value->url;?>
">
						<img width="200" height="200" src="<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['resize'][0][0]->resize_modifier($_smarty_tpl->tpl_vars['related_product']->value->image->filename,200,200);?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['related_product']->value->name, ENT_QUOTES, 'UTF-8', true);?>
" alt="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['related_product']->value->name, ENT_QUOTES, 'UTF-8', true);?>
"/>
					</a>
					<?php } else { ?>
					<img width="200" height="200" src="design/<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['settings']->value->theme, ENT_QUOTES, 'UTF-8', true);?>
/images/nophoto.png" alt="Нет фото">
					<?php }?>
				</div>
				<h6><a data-product="<?php echo $_smarty_tpl->tpl_vars['related_product']->value->id;?>
" href="products/<?php echo $_smarty_tpl->tpl_vars['related_product']->value->url;?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['related_product']->value->name, ENT_QUOTES, 'UTF-8', true);?>
</a></h6>
				<span class="price">
					<?php if (count($_smarty_tpl->tpl_vars['related_product']->value->variants)>0) {?>
					<?php  $_smarty_tpl->tpl_vars['v'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['v']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['related_product']->value->variants; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['v']->key => $_smarty_tpl->tpl_vars['v']->value) {
$_smarty_tpl->tpl_vars['v']->_loop = true;
?>
					<?php if ($_smarty_tpl->tpl_vars['v']->value->compare_price>0) {?><span class="compare_price"><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['convert'][0][0]->convert($_smarty_tpl->tpl_vars['v']->value->compare_price);?>
</span><?php }?>
					<span class="amount"><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['convert'][0][0]->convert($_smarty_tpl->tpl_vars['v']->value->price);?>
 <span class="currency"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['currency']->value->sign, ENT_QUOTES, 'UTF-8', true);?>
</span></span>
					<?php } ?>
					<?php } else { ?>
					Нет в наличии
					<?php }?>
				</span>
			</li>
			<?php } ?>
		</ul>
		<nav class="nav-pages"></nav>
	</div>
	<!-- /.slider -->
	<!-- COMMENTS -->
	<div class="container">
		<div class="col-md-12 comments" id="comments">
			<h3>Комментарии</h3>
			<div class="comment clearfix">
				<?php if ($_smarty_tpl->tpl_vars['comments']->value) {?>
				<!-- Список с комментариями -->
				<?php  $_smarty_tpl->tpl_vars['comment'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['comment']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['comments']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['comment']->key => $_smarty_tpl->tpl_vars['comment']->value) {
$_smarty_tpl->tpl_vars['comment']->_loop = true;
?>
				<div class="comment clearfix">
					<div class="comment-line">
						<p>
							<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['comment']->value->name, ENT_QUOTES, 'UTF-8', true);?>
,
							<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['date'][0][0]->date_modifier($_smarty_tpl->tpl_vars['comment']->value->date);?>
, <?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['time'][0][0]->time_modifier($_smarty_tpl->tpl_vars['comment']->value->date);?>

							<?php if (!$_smarty_tpl->tpl_vars['comment']->value->approved) {?><span class="label label-default">ожидает модерации</span><?php }?>
							<a  name="comment_<?php echo $_smarty_tpl->tpl_vars['comment']->value->id;?>
" class="internal"></a>
						</p>
					</div>
					<p><?php echo nl2br(htmlspecialchars($_smarty_tpl->tpl_vars['comment']->value->text, ENT_QUOTES, 'UTF-8', true));?>
</p>
				</div>
				<?php } ?>
				<?php } else { ?>
				<p>Пока нет комментариев</p>
				<?php }?>
			</div>
		</div>
		<div class="col-md-9 comments" id="comments">
			<!-- Add Comment -->
			<div class="add-comment" id="addcomment">
				<a role="button" data-toggle="collapse" href="#collapseComment" aria-expanded="false" aria-controls="collapseComment"><h3>Написать комментарий</h3></a>
				<?php if ($_smarty_tpl->tpl_vars['error']->value) {?>
				<div class="alert alert-warning">
					<?php if ($_smarty_tpl->tpl_vars['error']->value=='captcha') {?>
					Подтвердите что вы не робот
					<?php } elseif ($_smarty_tpl->tpl_vars['error']->value=='empty_name') {?>
					Введите имя
					<?php } elseif ($_smarty_tpl->tpl_vars['error']->value=='empty_comment') {?>
					Введите комментарий
					<?php }?>
				</div>
				<?php }?>
				<div class="collapse" id="collapseComment">
					<form class="comment_form" method="post">
						<div class="form-wrap">
							<div class="form-group">
								<input class="input_name" type="text" id="comment_name" name="name" value="<?php echo $_smarty_tpl->tpl_vars['comment_name']->value;?>
" data-format=".+" data-notice="Введите имя" placeholder="Ваше имя"/>
							</div>
							<div class="form-group">
								<textarea class="comment_textarea" id="comment_text" name="text" data-format=".+" data-notice="Введите комментарий" placeholder="Комментарий"><?php echo $_smarty_tpl->tpl_vars['comment_text']->value;?>
</textarea>
							</div>
						</div>
						<div class="g-recaptcha" data-sitekey="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['settings']->value->site_code, ENT_QUOTES, 'UTF-8', true);?>
" data-size="compact"></div>
						<input class="btn btn btn-light" type="submit" name="comment" value="Отправить" />
					</form>
				</div>
			</div>
			<!-- /.add-comment -->
		</div>
	</div>
	<!-- /.comments -->
	<!-- Kaspi-->
	
	<script>(function(d, s, id) {
		var js, kjs;
		if (d.getElementById(id)) return;
		js = d.createElement(s); js.id = id;
		js.src = 'https://kaspi.kz/kaspibutton/widget/ks-wi_ext.js';
		kjs = document.getElementsByTagName(s)[0]
		kjs.parentNode.insertBefore(js, kjs);
	}(document, 'script', 'KS-Widget'));</script>
	
	<!-- ReadMore -->
	<?php if ($_smarty_tpl->tpl_vars['product']->value->body) {?><script src="design/<?php echo $_smarty_tpl->tpl_vars['settings']->value->theme;?>
/js/readmore.min.js"></script>
	<script>
		$('p').readmore({
			speed: 1000,
			maxHeight: 200
		});
	</script>
<?php }?><?php }} ?>
