<?PHP

require_once('View.php');

class RegisterView extends View
{
	function fetch()
	{
		$response = null;		 		
		$reCaptcha = new \ReCaptcha\ReCaptcha($this->settings->secret_code);
		$default_status = 1; // Активен ли пользователь сразу после регистрации (0 или 1)
		
		if($this->request->method('post') && $this->request->post('register'))
		{
			$name			= $this->request->post('name');
			$email			= $this->request->post('email');
			$password		= $this->request->post('password');
			 $captcha_code           = $this->request->post('g-recaptcha-response');
			
			$this->design->assign('name', $name);
			$this->design->assign('email', $email);
			// ReCaptcha     
         $response = $reCaptcha->verify($captcha_code, $_SERVER['REMOTE_ADDR']); 
			
			$this->db->query('SELECT count(*) as count FROM __users WHERE email=?', $email);
			$user_exists = $this->db->result('count');

			if($user_exists)
				$this->design->assign('error', 'user_exists');
			elseif(empty($name))
				$this->design->assign('error', 'empty_name');
			elseif(empty($email))
				$this->design->assign('error', 'empty_email');
			elseif(empty($password))
				$this->design->assign('error', 'empty_password');		
			elseif($response->getErrorCodes())
			{
				$this->design->assign('error', 'captcha');
			}
			elseif($user_id = $this->users->add_user(array('name'=>$name, 'email'=>$email, 'password'=>$password, 'enabled'=>$default_status, 'phone'=>$this->request->post('phone'), 'group_id'=> $id !=1, 'last_ip'=>$_SERVER['REMOTE_ADDR'])))
			{
				$_SESSION['user_id'] = $user_id;
				if(!empty($_SESSION['last_visited_page']))
					header('Location: '.$_SESSION['last_visited_page']);				
				else
					header('Location: '.$this->config->root_url);
			}
			else
				$this->design->assign('error', 'unknown error');
	
		}
		return $this->design->fetch('register.tpl');
	}	
}
