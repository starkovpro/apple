<?PHP

/**
 * Simpla CMS
 *
 * @copyright 	2011 Denis Pikusov
 * @link 		http://simp.la
 * @author 		Denis Pikusov
 *
 * Отображение статей на сайте
 * Этот класс использует шаблоны articles.tpl и article.tpl
 *
 */
 
require_once('View.php');

class FeedbackView extends View
{
	function fetch()
	{
		$response = null;		 		
		$reCaptcha = new \ReCaptcha\ReCaptcha($this->settings->secret_code);
		$feedback = new stdClass;
		if($this->request->method('post') && $this->request->post('feedback'))
		{
			$feedback->name         = $this->request->post('name');
			$feedback->email        = $this->request->post('email');
			$feedback->message      = $this->request->post('message');
			$captcha_code           = $this->request->post('g-recaptcha-response');
			
			$this->design->assign('name',  $feedback->name);
			$this->design->assign('email', $feedback->email);
			$this->design->assign('message', $feedback->message);
			// ReCaptcha     
         $response = $reCaptcha->verify($captcha_code, $_SERVER['REMOTE_ADDR']); 
		 
			if(empty($feedback->name))
				$this->design->assign('error', 'empty_name');
			elseif(empty($feedback->email))
				$this->design->assign('error', 'empty_email');
			elseif(empty($feedback->message))
				$this->design->assign('error', 'empty_text');
			elseif($response->getErrorCodes())
			{
				$this->design->assign('error', 'captcha');
			}
			else
			{
				$this->design->assign('message_sent', true);
				
				$feedback->ip = $_SERVER['REMOTE_ADDR'];
				$feedback_id = $this->feedbacks->add_feedback($feedback);
				
				// Отправляем email
				$this->notify->email_feedback_admin($feedback_id);				
				
								
			}
		}

		if($this->page)
		{
			$this->design->assign('meta_title', $this->page->meta_title);
			$this->design->assign('meta_keywords', $this->page->meta_keywords);
			$this->design->assign('meta_description', $this->page->meta_description);
		}

		$body = $this->design->fetch('feedback.tpl');
		
		return $body;
	}
}
